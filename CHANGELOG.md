# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.6] - 2021-12-01
### Security
* Composer security fix

## [1.0.5] - 2021-10-01

* Fix beach loading

## [1.0.4] - 2021-07-02

* Composer security fix (league/flysystem)

## [1.0.3] - 2021-06-15

* Performance fix
* Laravel Mix upgrade
* NPM Audit fix


## [1.0.2] - 2021-05-28

* Composer security fix

## [1.0.1] - 2021-04-27

* Add selectbox on metadata
* Remove extra field on beach
* Improve tests

## [1.0.0] - 2021-03-03

First release of BM.

* System develped as Master Plan documentation
* Manage Assessments
* Manage Audit
* Manage Audit Object
* Add Hazards and Risks
* Recommendations and Comments

[1.0.6]: https://git.nano.rocks/sls/bm/compare/1.0.5...1.0.6
[1.0.5]: https://git.nano.rocks/sls/bm/compare/1.0.4...1.0.5
[1.0.4]: https://git.nano.rocks/sls/bm/compare/1.0.3...1.0.4
[1.0.3]: https://git.nano.rocks/sls/bm/compare/1.0.2...1.0.3
[1.0.2]: https://git.nano.rocks/sls/bm/compare/1.0.1...1.0.2
[1.0.1]: https://git.nano.rocks/sls/bm/compare/1.0.0...1.0.1
[1.0.0]: https://git.nano.rocks/sls/bm/compare/0.0.0...1.0.0
