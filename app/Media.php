<?php

namespace App;

use Carbon\Carbon;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media as BaseMedia;

class Media extends BaseMedia
{

    use SoftDeletes, LogsActivity;
    protected static $logName = 'media';
    protected static $logOnlyDirty = true;
    protected static $logAttributes = ['name'];
    protected static $submitEmptyLogs = false;
    protected $touches = ['model'];

    protected $appends = [
        'url'
    ];

    protected $visible = [
        'id',
        'name',
        'url',
        'size',
        'mime_type',
        'assessment_id',
        'beach_id',
        'audit_id',
        'audit_object_id',
        'object_type_id',
        'user_id',
        'created_at',
        'updated_at',
    ];


    public function getFullUrl(string $conversionName = ''): string
    {
        if ($this->disk == 's3' || $this->disk == 'google') {
            return $this->getTemporaryUrl(Carbon::now()->addMinutes(config('filesystems.expire')), $conversionName);
        } else {
            return parent::getFullUrl($conversionName);
        }
    }

    public function getDescriptionForEvent(string $eventName): string
    {
        return "File <b>".$this->name."</b> has been {$eventName}.";
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function assessment()
    {
        return $this->belongsTo(Assessment::class);
    }

    public function getUrlAttribute($value)
    {
        return $this->getFullUrl();
    }

    public function getIconAttribute($value)
    {
        return $this->icon($value);
    }

    public function icon()
    {
      // List of official MIME Types: http://www.iana.org/assignments/media-types/media-types.xhtml
        $icon_classes = array(
        // Media
        'image'                                                          => 'file-image',
        'audio'                                                          => 'file-audio',
        'video'                                                          => 'file-video',
        // Documents
        'application/pdf'                                                => 'file-pdf',
        'application/msword'                                             => 'file-word',
        'application/vnd.ms-word'                                        => 'file-word',
        'application/vnd.oasis.opendocument.text'                        => 'file-word',
        'application/vnd.openxmlformatsfficedocument.wordprocessingml' => 'file-word',
        'application/vnd.ms-excel'                                       => 'file-excel',
        'application/vnd.openxmlformatsfficedocument.spreadsheetml'    => 'file-excel',
        'application/vnd.oasis.opendocument.spreadsheet'                 => 'file-excel',
        'application/vnd.ms-powerpoint'                                  => 'file-powerpoint',
        'application/vnd.openxmlformatsfficedocument.presentationml'   => 'file-powerpoint',
        'application/vnd.oasis.opendocument.presentation'                => 'file-powerpoint',
        'text/plain'                                                     => 'file-code',
        'text/html'                                                      => 'file-code',
        'application/json'                                               => 'file-code',
        // Archives
        'application/gzip'                                               => 'file-archive',
        'application/zip'                                                => 'file-archive',
        );
        foreach ($icon_classes as $text => $icon) {
            if (strpos($this->mime_type, $text) === 0) {
                return $icon;
            }
        }

        return 'file';
    }
}
