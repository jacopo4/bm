<?php

namespace App\Console;

use App\Console\Commands\ImportBeaches;
use App\Console\Commands\ImportMembers;
use App\Console\Commands\ImportEntitites;
use App\Console\Commands\RunCypressTest;
use App\Console\Commands\SyncDBAndFixtures;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ImportMembers::class,
        ImportEntitites::class,
        ImportBeaches::class,
        RunCypressTest::class,
        SyncDBAndFixtures::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('import:beaches')->weekly();

        $schedule->command('import:entities')->weekly();

        $schedule->command('import:members')->weekly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
