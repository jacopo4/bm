<?php

namespace App\Console\Commands;

use App\Beach;
use App\Entity;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\RemoteModels\AbsampBeach;
use App\Services\SLS\DataWarehouse;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportBeaches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:beaches';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import SLS Beaches from Absamp Database';

    /**
     * The console command log label.
     *
     * @var string
     */
    protected $label = 'ImportBeaches';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info("Start imports of  Beaches");

        $query    = AbsampBeach::with(['coordinates']);

        ProgressBar::setFormatDefinition('custom', '<info>%current% / %max%</info> <fg=red;bg=black>%message%</>');
        $bar = $this->output->createProgressBar($query->count());
        $bar->setFormat('custom');
        $bar->setMessage('Saving Beaches');
        $this->info("Fetching  ({$query->count()})");

        $query->chunk(200, function ($beaches) use ($bar) {
            foreach ($beaches as $beach) {
                $localBeach = Beach::firstOrNew(
                    [
                    'absamp_id' => $beach->beachid,
                    'key' => $beach->beach_key
                    ]
                );

                if ($localBeach->exists) {
                    if ($localBeach->date_last_modified > $localBeach->updated_at) {
                        $bar->setMessage('New Update for  : '.$localBeach->key);
                        $localBeach->title = $beach->beach_name;
                        $localBeach->state = $beach->state;

                        $localBeach->latitude = $beach->coordinates->latitude;
                        $localBeach->longitude = $beach->coordinates->longitude;
                        $localBeach->last_updated = $beach->date_created;

                        $metadata = [];
                        if ($beach->rating) {
                            if ($beach->rating->hazard_rating_lt) {
                                $metadata["hazard_rating"] = intval($beach->rating->hazard_rating_lt);
                            }
                        }
                        if ($beach->location_type_id) {
                            $metadata["beach_type"] = $beach->location_type_id;
                        }
                        $localBeach->metadata = $metadata;

                        $localBeach->save();
                    } else {
                        $bar->setMessage('No changes for Beach  : '.$localBeach->key);
                    }
                } else {
                    $bar->setMessage('Creating New Beach  : '.$localBeach->key);
                    $localBeach->title = $beach->beach_name;
                    $localBeach->state = $beach->state;

                    $localBeach->latitude = $beach->coordinates->latitude;
                    $localBeach->longitude = $beach->coordinates->longitude;
                    $localBeach->last_updated = $beach->date_created;
                    $metadata = [];
                    if ($beach->rating) {
                        if ($beach->rating->hazard_rating_lt) {
                            $metadata["hazard_rating"] = intval($beach->rating->hazard_rating_lt);
                        }
                    }
                    if ($beach->location_type_id) {
                        $metadata["beach_type"] = $beach->location_type_id;
                    }
                    $localBeach->metadata = $metadata;
                    $localBeach->save();
                }


                $bar->advance();
            }
        });

        $bar->finish();
    }
}
