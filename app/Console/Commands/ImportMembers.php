<?php

namespace App\Console\Commands;

use App\User;
use App\Entity;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Services\SLS\DataWarehouse;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportMembers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:members';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import SLS Members from Data Warehouse';

    /**
     * The console command log label.
     *
     * @var string
     */
    protected $label = 'ImportMembers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = 200;
        $this->info("Start imports of Memmbers");
        $this->comment("Geting Fresh token");
        $this->api = new DataWarehouse();
        $this->info("Fetching first ${limit} members");
        $skip =  1;
        while ($skip != null) {
            $results = $this->api->getMembers($skip, $limit);
            try {
                $this->parseItems($results);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
                $this->info("Starting new DataWarehouse instance");
                $this->api = new DataWarehouse();
                $results = $this->api->getMembers($skip, $limit);
                $this->parseItems($results);
            }
            $skip = $results->SkipToken;
            if ($skip != null) {
                $this->info("We got token for next page: ${skip}");
            } else {
                $this->info("Batch Done");
            }
        }
    }

    protected function parseItems($results)
    {

        ProgressBar::setFormatDefinition('custom', '<info>%current% / %max%</info> <fg=red;bg=black>%message%</>');
        //' %current%/%max% -- %message%'

        $bar = $this->output->createProgressBar(count($results->Result));
        // $bar->setFormat('very_verbose');
        $bar->setFormat('custom');
        $bar->setMessage('Saving members');

        $bar->start();
        foreach ($results->Result as $member) {
            $user = User::firstOrNew(
                ['member_id' => $member->MemberID]
            );

            if ($user->exists) {
                // $this->comment("User exists ID:".$user->id);
                if ($user->remote_last_updated_at != $member->LastUpdated) {
                    // check if LastUpdate changes
                    // $this->comment("\t changed update him");
                    $bar->setMessage('New Update for  : '.$member->MemberID);
                    $user->update(
                        [
                            'first_name' => (is_string($member->FirstName)) ? $member->FirstName : 'N/A',
                            'last_name' => (is_string($member->Surname)) ? $member->Surname : 'N/A',
                            'email' => (is_string($member->EmailAddress1)) ? $member->EmailAddress1 : null,
                            'mobile' => (is_string($member->MobilePhone)) ? $member->MobilePhone : null,
                            'remote_last_updated_at' => $member->LastUpdated
                        ]
                    );
                    $this->saveMembership($member, $user);
                } else {
                    // do nothing
                    // $this->comment("\tNo change");
                }
            } else {
                // new dude
                // $this->comment("New dude");
                $bar->setMessage('New Member : '.$member->MemberID);
                $user->fill(
                    [
                        'first_name' => (is_string($member->FirstName)) ? $member->FirstName : 'N/A',
                        'last_name' => (is_string($member->Surname)) ? $member->Surname : 'N/A',
                        'dob' => $member->DOB,
                        'password' => bcrypt(Str::random()),
                        'email' => (is_string($member->EmailAddress1)) ? $member->EmailAddress1 : null,
                        'mobile' => (is_string($member->MobilePhone)) ? $member->MobilePhone : null,
                        'remote_last_updated_at' => $member->LastUpdated,
                        'officer_entities' => []
                    ]
                );
                $user->save();
                $this->saveMembership($member, $user);
            }


            $bar->advance();
        }
        $bar->finish();
        $this->info("Done.");
    }

    protected function saveMembership($member, $user)
    {
        if ($member->Memberships) {
            $entites = [];
            foreach ($member->Memberships as $memberShip) {
                array_push($entites, $memberShip->EntityID);
            }
            $user->entities()->sync($entites);
        }
    }
}
