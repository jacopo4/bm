<?php

namespace App\Console\Commands;

use App\Entity;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Services\SLS\DataWarehouse;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportEntitites extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:entities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import SLS Entitites from Data Warehouse';

    /**
     * The console command log label.
     *
     * @var string
     */
    protected $label = 'ImportEntities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $this->info("Start imports of Entities");
        $this->comment("Geting Fresh token");
        $this->api = new DataWarehouse();
        
        $results = $this->api->getEntities();
        $this->parseItems($results);
    }

    protected function parseItems($results)
    {

        ProgressBar::setFormatDefinition('custom', '<info>%current% / %max%</info> <fg=red;bg=black>%message%</>');
        //' %current%/%max% -- %message%'
        

        $bar = $this->output->createProgressBar(count($results->Result));
        // $bar->setFormat('very_verbose');
        $bar->setFormat('custom');
        $bar->setMessage('Saving entities');

        $bar->start();
        foreach ($results->Result as $remoteEntitiy) {
            $entity = Entity::firstOrNew(
                [
                    'id'        => $remoteEntitiy->EntityID,
                    'entity_id' => $remoteEntitiy->EntityID
                ]
            );

            $bar->setMessage('New entitiy : '.$remoteEntitiy->EntityID);
            $entity->title = (!empty($remoteEntitiy->DisplayName)) ? $remoteEntitiy->DisplayName : $remoteEntitiy->EntityName;
            $entity->save();
            
            $bar->advance();
        }
        $bar->finish();
        $this->info("Done.");
    }
}
