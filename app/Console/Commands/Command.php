<?php

namespace App\Console\Commands;

use Illuminate\Console\Command as LaravelCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Command extends LaravelCommand
{
    protected $label;
    protected $name = 'b-system:pre-fire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function preHandle()
    {
        \Log::info("Starting command: " . $this->label);
        $this->start = microtime(true);
        $returnValue = $this->handle();
        // finished
        \Log::info("Finishing command: " . $this->label . " duration:" . (microtime(true) - $this->start));
        return $returnValue;
    }

    /**
     * Make it log and output
     *
     * @param [type] $string
     * @param [type] $verbosity
     * @return void
     */
    public function info($string, $verbosity = null)
    {
        parent::info($string, $verbosity);
        \Log::info("[$this->label] " . $string);
    }
    /**
     * Make it comment and log
     *
     * @param [type] $string
     * @param [type] $verbosity
     * @return void
     */
    public function comment($string, $verbosity = null)
    {
        parent::comment($string, $verbosity);
        \Log::info("[$this->label] " . $string);
    }
    /**
     * Make it error and log
     *
     * @param [type] $string
     * @param [type] $verbosity
     * @return void
     */
    public function error($string, $verbosity = null)
    {
        parent::error($string, $verbosity);
        \Log::error("[$this->label] " . $string);
    }

    /**
     * Execute the console command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface   $input
     * @param  \Symfony\Component\Console\Output\OutputInterface $output
     * @return mixed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $method = method_exists($this, 'preHandle') ? 'preHandle' : 'handle';

        return (int) $this->laravel->call([$this, $method]);
    }
}
