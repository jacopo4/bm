<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\ObjectType;
use App\ObjectSubtype;
use App\RemoteModels\AbsampAttributes;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportObjectTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:objects-types';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Object types and subtypes from Absamp Database';

    /**
     * The console command log label.
     *
     * @var string
     */
    protected $label = 'ImportObjectTypes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info("Start imports of Objects Types");

        $groups = [
            '1' => true,// Activities
            '6' => true, // Access
            '12' => true, // Equipment
            '13' => true, // Support
            '14' => true, // Facilities
            '16' => true, // Hazards
            '19' => true, // Services
            '28' => true, // Sign
        ];

        $query    = AbsampAttributes::with(['code']);

        ProgressBar::setFormatDefinition('custom', '<info>%current% / %max%</info> <fg=red;bg=black>%message%</>');
        $bar = $this->output->createProgressBar($query->count());
        $bar->setFormat('custom');
        $bar->setMessage('Saving Types and Subtypes');
        $this->info("Fetching  ({$query->count()})");

        $query->chunk(200, function ($subtypes) use ($bar, $groups) {
            foreach ($subtypes as $subtype) {
                $types = ObjectType::firstOrCreate(
                    [
                        'id' => $subtype->acdid,
                        'title' => $subtype->code->description,
                        'explanation' => $subtype->code->explanation,
                        'is_group' => isset($groups[$subtype->acdid]) ? 1 : 0,
                        'created_at' => $subtype->code->date_created,
                        'updated_at' => $subtype->code->date_last_modified
                    ]
                );

                $subtypes = ObjectSubtype::firstOrCreate(
                    [
                        'id' => $subtype->attid,
                        'type_id' => $subtype->acdid,
                        'title' => $subtype->att_text,
                        'order' => $subtype->display_seq,
                        'created_at' => $subtype->date_created,
                        'updated_at' => $subtype->date_last_modified,
                        'metadata_type' => ObjectSubtype::metadataTypeFor($subtype->attid)
                    ]
                );

                $bar->advance();
            }
        });

        $bar->finish();
    }
}
