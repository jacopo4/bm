<?php

namespace App\Console\Commands;

use App\Risk;
use App\User;
use App\Audit;
use App\Comment;
use App\Recommendation;
use App\Assessment;
use App\AuditObject;
use Symfony\Component\Process\Process;

class RunCypressTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cypress:test {browser?} {--height=1080} {--width=1920} {--baseUrl=} {--spec=} {--fast} {--percy}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh database, start testing server and run all cypress tests';

    /**
     * The console command log label.
     *
     * @var string
     */
    protected $label = 'RunCypressTest';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $fast = $this->option('fast');
        $percy = $this->option('percy');
        $browser = $this->argument('browser');
        $height = $this->option('height');
        $width = $this->option('width');
        $baseUrl = $this->option('baseUrl');
        $spec = $this->option('spec');

        if (env('FAST') !== null || $fast) {
            $this->info("Fast mode no running migrations");
        } else {
            $bootstrap = new Process(array(PHP_BINARY, './tests/bootstrap.php'));
            $bootstrap->start();
            while ($bootstrap->isRunning()) {
                echo $bootstrap->getIncrementalOutput();
            }
        }

        // dump database into fixtures
        $this->info("Dumping DB into fixtures");
        $fp = fopen('./cypress/fixtures/users.json', 'w');
        fwrite($fp, User::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/assessments.json', 'w');
        fwrite($fp, Assessment::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/auditObjects.json', 'w');
        fwrite($fp, AuditObject::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/audits.json', 'w');
        fwrite($fp, Audit::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/comments.json', 'w');
        fwrite($fp, Comment::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/recommendations.json', 'w');
        fwrite($fp, Recommendation::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/risks.json', 'w');
        fwrite($fp, Risk::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $env = 'testing';
        $webserver = new Process(array(PHP_BINARY, 'artisan', 'serve', '--port=8081', '--env=' . $env));
        $webserver->disableOutput();
        $this->info("Starting server");
        $webserver->start();

        if (PHP_OS == 'Darwin') {
            sleep(2);
            $browser = 'chrome';
        } else {
            sleep(5);
        }



        // percy exec -- cypress run

        if ($browser) {
            $this->comment("Browser set to ${browser}");
            $cypressCommand = ['npm', 'run', 'cypress', '--', '--browser', $browser];
        } else {
            $cypressCommand = ['npm', 'run', 'cypress', '--'];
        }

        if ($spec) {
            $cypressCommand = array_merge($cypressCommand, ['--spec', "{$spec}"]);
        }

        // if (PHP_OS == 'Linux') {
        //     // record on Linux
        //     $cypressCommand = array_merge($cypressCommand, ['--record']);
        // }

        if ($baseUrl) {
            $cypressCommand = array_merge($cypressCommand, ['--config', "baseUrl={$baseUrl},viewportHeight={$height},viewportWidth={$width}"]);
        } else {
            $cypressCommand = array_merge($cypressCommand, ['--config', "viewportHeight={$height},viewportWidth={$width}"]);
        }


        // percy
        if (env('PERCY_TOKEN') && $percy) {
            $this->comment("Percy detected.");
            array_unshift($cypressCommand, "npx", "percy", "exec", "--");
        }

        $cypress = new Process($cypressCommand);
        $cypress->setIdleTimeout(60);
        $cypress->setTty(Process::isTtySupported());
        $cypress->start();
        while ($cypress->isRunning()) {
            echo $cypress->getIncrementalOutput();
            sleep(1);
        }
        echo $cypress->getIncrementalOutput();
        $killServer = new Process(array('./tests/killserver.sh'));
        $killServer->disableOutput();
        $this->info("Killing server");
        $killServer->run();

        // if cypress fails then return 1
        if ($cypress->getExitCode()) {
            return 1;
        }
        return 0;
    }
}
