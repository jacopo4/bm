<?php

namespace App\Console\Commands;

use Symfony\Component\Process\Process;
use App\User;
use App\Assessment;
use App\AuditObject;
use App\Audit;
use App\Comment;
use App\Recommendation;
use App\Risk;

class SyncDBAndFixtures extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cypress:db {--fast}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump database into fixtures for cypress use --fast to dump database without reseeding';

    /**
     * The console command log label.
     *
     * @var string
     */
    protected $label = 'SyncDBAndFixtures';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $fast = $this->option('fast');

        if (env('FAST') !== null || $fast) {
            $this->info("Fast mode no running migrations");
        } else {
            $bootstrap = new Process(array(PHP_BINARY, './tests/bootstrap.php'));
            $bootstrap->start();
            while ($bootstrap->isRunning()) {
                echo $bootstrap->getIncrementalOutput();
            }
        }

        // dump database into fixtures
        $this->info("Dumping DB into fixtures");
        $fp = fopen('./cypress/fixtures/users.json', 'w');
        fwrite($fp, User::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/assessments.json', 'w');
        fwrite($fp, Assessment::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/auditObjects.json', 'w');
        fwrite($fp, AuditObject::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/audits.json', 'w');
        fwrite($fp, Audit::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/comments.json', 'w');
        fwrite($fp, Comment::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/recommendations.json', 'w');
        fwrite($fp, Recommendation::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);

        $fp = fopen('./cypress/fixtures/risks.json', 'w');
        fwrite($fp, Risk::all()->toJson(JSON_PRETTY_PRINT));
        fclose($fp);
    }
}
