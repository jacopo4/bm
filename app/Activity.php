<?php

namespace App;

use App\Audit;
use App\Beach;
use App\Events\ActivityCreated;
use Spatie\Activitylog\Models\Activity as SpatieActivity;

class Activity extends SpatieActivity
{
    protected $dispatchesEvents = [
        'created' => ActivityCreated::class
    ];

    public function assessment()
    {
        return $this->belongsTo(Assessment::class);
    }
    public function beach()
    {
        return $this->belongsTo(Beach::class);
    }

    public function audit()
    {
        return $this->belongsTo(Audit::class);
    }

    public function auditObject()
    {
        return $this->belongsTo(AuditObject::class);
    }
}
