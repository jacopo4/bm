<?php

namespace App;

use App\Comment;
use App\Recommendation;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class AuditObject extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, LogsActivity;
    protected $table = 'audit_object';
    protected $hidden = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    protected $fillable = [
        // 'id',
        'assessment_id',
        'beach_id',
        'audit_id',
        'user_id',
        'type_id',
        'subtype_id',
        'title',
        'metadata',
        'latitude',
        'longitude'
    ];

    protected $visible = [
        'id',
        'assessment_id',
        'beach_id',
        'audit_id',
        'user_id',
        'type_id',
        'subtype_id',
        'title',
        'metadata',
        'latitude',
        'longitude',
        'hazards',
        'hazardChildren',
        'signs',
        'signChildren',
        'pivot'
    ];

    protected $casts = [
      'metadata' => 'array'
    ];

    protected static $logName = 'AuditObject';
    protected static $logOnlyDirty = true;
    protected static $logFillable = true;
   // protected static $logAttributes = ['*'];
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Audit Object  <strong>".$this->title."</strong> has been {$eventName}";
    }


    public function assessment()
    {
        return $this->belongsTo(Assessment::class);
    }

    public function beach()
    {
        return $this->belongsTo(Beach::class);
    }

    public function audit()
    {
        return $this->belongsTo(Audit::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(ObjectType::class);
    }

    public function subtype()
    {
        return $this->belongsTo(ObjectSubtype::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function recommendations()
    {
        return $this->morphMany(Recommendation::class, 'recommendable');
    }

    public function risks()
    {
        return $this->hasMany(Risk::class);
    }

    public function hazards()
    {
        return $this->belongsToMany(AuditObject::class, 'audit_object_hazard', 'audit_object_id', 'hazard_object_id');
    }

    public function signs()
    {
        return $this->belongsToMany(AuditObject::class, 'audit_object_sign', 'audit_object_id', 'sign_object_id');
    }

    public function hazardChildren()
    {
        return $this->belongsToMany(AuditObject::class, 'audit_object_hazard', 'hazard_object_id', 'audit_object_id');
    }

    public function signChildren()
    {
        return $this->belongsToMany(AuditObject::class, 'audit_object_sign', 'sign_object_id', 'audit_object_id');
    }

    // MEDIA?
    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('photos');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumbs')
            ->fit(Manipulations::FIT_CONTAIN, 150, 150)
            ->nonQueued();
    }

    // public function getPhotosAttribute()
    // {
    //     return $this->getFirstMedia('photos')->getFullUrl();
    // }

    // public function getPhotosThumbAttribute()
    // {
    //     return $this->getFirstMedia('photos')->getFullUrl('thumbs');
    // }
}
