<?php

namespace App\Providers;

use App\Events\ActivityCreated;
use App\Listeners\WriteMetaToActivity;
use Illuminate\Auth\Events\Registered;
use App\Listeners\WriteMetadataToMedia;
use Spatie\MediaLibrary\Events\MediaHasBeenAdded;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ActivityCreated::class   => [
            WriteMetaToActivity::class
        ],
        MediaHasBeenAdded::class => [
            WriteMetadataToMedia::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
