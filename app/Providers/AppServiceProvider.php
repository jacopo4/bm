<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Relation::morphMap([
            'activity' => \App\Activity::class,
            'assessment' => \App\Assessment::class,
            'audit' => \App\Audit::class,
            'auditObject' => \App\AuditObject::class,
            'beach' => \App\Beach::class,
            'comment' => \App\Comment::class,
            'entity' => \App\Entity::class,
            'objectSubtype' => \App\ObjectSubtype::class,
            'objectType' => \App\ObjectType::class,
            'risk' => \App\Risk::class,
            'user' => \App\User::class,
            'media' => \App\Media::class
        ]);
    }
}
