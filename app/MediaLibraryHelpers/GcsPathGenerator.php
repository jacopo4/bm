<?php

namespace App\MediaLibraryHelpers;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\PathGenerator\BasePathGenerator;

class GcsPathGenerator extends BasePathGenerator
{
    /*
     * Get a unique base path for the given media.
     */
    protected function getBasePath(Media $media): string
    {
        # "https://storage.google.com/bm-develop/beach/7482/12/sunset-small.jpg"
        return $media->model_type.'/'.$media->model_id.'/'.$media->getKey();
    }
}
