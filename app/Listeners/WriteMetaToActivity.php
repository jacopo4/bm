<?php

namespace App\Listeners;

use App\Manual;
use App\Events\ActivityCreated;
use Illuminate\Support\Facades\Auth;

class WriteMetaToActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivityCreated  $event
     * @return void
     */
    public function handle(ActivityCreated $event)
    {
        $activity = $event->activity;
        // get the company form logged in user

        if ($activity->subject_type == 'assessment') {
            $activity->assessment_id = $activity->subject_id;
        } elseif ($activity->subject_type == 'beach') {
            $activity->beach_id = $activity->subject_id;
        } elseif ($activity->subject_type == 'audit') {
            $activity->assessment_id = $activity->subject->assessment->id;
            $activity->beach_id = $activity->subject->beach->id;
            $activity->audit_id = $activity->subject_id;
        } elseif ($activity->subject_type == 'objectType') {
            $activity->object_type_id = $activity->subject_id;
        } elseif ($activity->subject_type == 'auditObject') {
            $activity->audit_object_id = $activity->subject_id;
            
            $activity->assessment_id = $activity->subject->assessment->id;
            $activity->beach_id = $activity->subject->beach->id;
            $activity->audit_id = $activity->subject->audit->id;
            $activity->object_type_id = $activity->subject->type_id;
        } elseif ($activity->subject_type == 'risk') {
            $activity->assessment_id = $activity->subject->assessment->id;
            $activity->beach_id = $activity->subject->beach->id;
            $activity->audit_id = $activity->subject_id;
            $activity->audit_object_id = $activity->subject->audit_object_id;
            $activity->object_type_id = $activity->subject->object_type_id;
        }

        $activity->save();
    }
}
