<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\Events\MediaHasBeenAdded;

class WriteMetadataToMedia
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MediaCreated  $event
     * @return void
     */
    public function handle(MediaHasBeenAdded $event)
    {
        $media = $event->media;
        $path = $media->getPath();

        Log::info("File {$path} has been saved for media {$media->id} / {$media->media_type} size: {$media->human_readable_size}");


        // $table->bigInteger('assessment_id')->unsigned()->index();
        // $table->bigInteger('beach_id')->unsigned()->index();
        // $table->bigInteger('audit_id')->unsigned()->index();
        // $table->bigInteger('audit_object_id')->unsigned()->index();
        // $table->bigInteger('object_type_id')->unsigned()->index();
        // $table->bigInteger('user_id')->unsigned()->index();
        
        if ($media->model_type == 'auditObject') {
            $object = $media->model;
            $media->assessment_id = $object->assessment_id;
            $media->beach_id = $object->beach_id;
            $media->audit_id = $object->audit_id;
            $media->audit_object_id = $object->id;
            $media->object_type_id = $object->type_id;
        }

        // if fail get the company from object
        if (Auth::guest()) {
        } else {
            $media->user_id = Auth::user()->id;
        }
        $media->save();
    }
}
