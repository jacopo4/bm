<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Assessment;
use App\User;

class UserInvited extends Notification
{
    use Queueable;

    /**
     * @var Project
     */
    public $assessment;
    /**
     * @var User
     */
    public $user;
    /**
     * @var Role_id
     */
    public $role_id;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Assessment $assessment, User $user, $role_id)
    {
        $this->assessment = $assessment;
        $this->user = $user;
        $this->role_id = $role_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hello, '.$notifiable->first_name.'!')
                    ->subject('You have been invited to an assessment')
                    ->line($this->user->first_name.' has invited you to assessment \''.$this->assessment->title.'\' as '. $this->roleIdToRole($this->role_id))
                    ->action('Visit '.config('app.name'), url('/'));
                    // ->line('Thank you for using our application!');
    }

    public function roleIdToRole($role_id)
    {
        $roles = [
            1 => 'an admin',
            2 => 'an auditor',
            3 => 'a client'
        ];
        return $roles[$role_id];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
