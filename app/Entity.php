<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entity extends Model
{
    //

    use SoftDeletes, LogsActivity;

    protected static $logName = 'Entity';
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;
    //protected static $logAttributes = ['name', 'location'];
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Entity  <strong>".$this->title."</strong> has been {$eventName}";
    }



    protected $fillable = [
        'id',
        'entity_id',
        'title',
        'state',
        'beach_id',
        'longitude',
        'latitude',
    ];

    /**
     * Get the users for the patrol.
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
