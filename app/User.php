<?php

namespace App;

use App\Entity;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, LogsActivity;


    protected static $logName = 'User';
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;
    //protected static $logAttributes = ['name', 'location'];
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "User <strong>".$this->username."</strong> has been {$eventName}";
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $dates = [
        'dob',
        'created_at',
        'updated_at',
        'last_login_at',
        //'remote_last_updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'member_id',
        'password',
        'username',
        'api_token',
        'dob',
        'email',
        'mobile',
        'officer_entities',
        'remote_last_updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'officer_entities' => 'array',
    ];

    public function entities()
    {
        return $this->belongsToMany(Entity::class)->orderBy('title');
    }

    public function assessments()
    {
        return $this->belongsToMany(Assessment::class)->withPivot('role_id')->withTimestamps();
    }

    public function audits()
    {
        return $this->HasMany(Audit::class);
    }

    public function objects()
    {
        return $this->HasMany(AuditObject::class);
    }
}
