<?php

namespace App\Policies;

use App\Media;
use App\User;
use App\AuditObject;
use Illuminate\Auth\Access\HandlesAuthorization;

class MediaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can index.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Media  $media
     * @return mixed
     */
    public function view(User $user, Media $media)
    {
        return $media->assessment->users->contains($user); // if on assessment
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, AuditObject $auditObject)
    {
        return $auditObject->assessment->users->contains($user); // if on assessment
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Media  $media
     * @return mixed
     */
    public function update(User $user, Media $media)
    {
        return $media->user->is($user) || $media->assessment->users()->where('role_id', 1)->get()->contains($user); // if own media or admin
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Media  $media
     * @return mixed
     */
    public function delete(User $user, Media $media)
    {
        return $media->user->is($user) || $media->assessment->users()->where('role_id', 1)->get()->contains($user); // if own media or admin
    }
}
