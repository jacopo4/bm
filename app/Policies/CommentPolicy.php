<?php

namespace App\Policies;

use App\Comment;
use App\User;
use App\AuditObject;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can index.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function view(User $user, Comment $comment)
    {
        return $comment->assessment->users->contains($user); // if on assessment
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, AuditObject $auditObject)
    {
        return $auditObject->assessment->users->contains($user); // if on assessment
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function update(User $user, Comment $comment)
    {
        return $comment->user->is($user) || $comment->assessment->users()->where('role_id', 1)->get()->contains($user); // if own comment or admin
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function delete(User $user, Comment $comment)
    {
        return $comment->user->is($user) || $comment->assessment->users()->where('role_id', 1)->get()->contains($user); // if own comment or admin
    }
}
