<?php

namespace App\Policies;

use App\Assessment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssessmentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can index.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Assessment  $assessment
     * @return mixed
     */
    public function view(User $user, Assessment $assessment)
    {
        return $assessment->users->contains($user); // can only view if on assessment
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true; // anyone can create
    }

    /**
     * Determine whether the user can add members.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function addMember(User $user, Assessment $assessment)
    {
        return $assessment->users()->where('role_id', 1)->get()->contains($user); // admin only
    }

    /**
     * Determine whether the user can remove members.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function removeMember(User $user, Assessment $assessment)
    {
        return $assessment->users()->where('role_id', 1)->get()->contains($user); // admin only
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Assessment  $assessment
     * @return mixed
     */
    public function update(User $user, Assessment $assessment)
    {
        return $assessment->users()->where('role_id', 1)->get()->contains($user); // admin only
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Assessment  $assessment
     * @return mixed
     */
    public function delete(User $user, Assessment $assessment)
    {
        return $assessment->users()->where('role_id', 1)->get()->contains($user); // admin only
    }
}
