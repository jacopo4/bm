<?php

namespace App\Policies;

use App\Risk;
use App\User;
use App\AuditObject;
use Illuminate\Auth\Access\HandlesAuthorization;

class RiskPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can index.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Risk  $risk
     * @return mixed
     */
    public function view(User $user, Risk $risk)
    {
        return $risk->assessment->users->contains($user); // if on assessment
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, AuditObject $auditObject)
    {
        return $auditObject->assessment->users()->whereIn('role_id', [1, 2])->get()->contains($user); // admin or auditor
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Risk  $risk
     * @return mixed
     */
    public function update(User $user, Risk $risk)
    {
        return $risk->assessment->users()->whereIn('role_id', [1, 2])->get()->contains($user); // admin or auditor
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Risk  $risk
     * @return mixed
     */
    public function delete(User $user, Risk $risk)
    {
        return $risk->assessment->users()->whereIn('role_id', [1, 2])->get()->contains($user); // admin or auditor
    }
}
