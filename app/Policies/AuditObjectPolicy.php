<?php

namespace App\Policies;

use App\AuditObject;
use App\User;
use App\Audit;
use Illuminate\Auth\Access\HandlesAuthorization;

class AuditObjectPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can index.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\AuditObject  $auditObject
     * @return mixed
     */
    public function view(User $user, AuditObject $auditObject)
    {
        return $auditObject->assessment->users->contains($user); // if on assessment
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, Audit $audit)
    {
        return $audit->assessment->users()->whereIn('role_id', [1, 2])->get()->contains($user); // admin or auditor
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\AuditObject  $auditObject
     * @return mixed
     */
    public function update(User $user, AuditObject $auditObject)
    {
        return $auditObject->assessment->users()->whereIn('role_id', [1, 2])->get()->contains($user); // admin or auditor
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\AuditObject  $auditObject
     * @return mixed
     */
    public function delete(User $user, AuditObject $auditObject)
    {
        return $auditObject->assessment->users()->where('role_id', 1)->get()->contains($user); // admin only
    }
}
