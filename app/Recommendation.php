<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recommendation extends Model
{
    use LogsActivity, SoftDeletes;

    protected $guarded = [];


    protected static $logName = 'Recommendation';
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;
    //protected static $logAttributes = ['name', 'location'];
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Recommendation  <strong>".$this->name."</strong> has been {$eventName}";
    }

    public function recommendable()
    {
        return $this->morphTo();
    }

    public function assessment()
    {
        return $this->belongsTo(Assessment::class);
    }

    public function beach()
    {
        return $this->belongsTo(Beach::class);
    }

    public function audit()
    {
        return $this->belongsTo(Audit::class);
    }

    public function auditObject()
    {
        return $this->belongsTo(AuditObject::class);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
