<?php namespace App\Services;

use Curl;
use Sabre;
use Tests\MockCurl;
use App\APIActivity;
use Mockery\Exception;
use Tests\MockSoapClient;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: cerw
 * Date: 3/03/15
 * Time: 3:43 PM
 */
class Service
{
    public $soap;

    private $auth;

    public function __construct()
    {
        $this->auth = [
            'DataWarehouse' => [
                'type' => 'rest',
                'host' => config('services.sls.datawarehouse.host'),
                'client' => config('services.sls.datawarehouse.client'),
                'secret' => config('services.sls.datawarehouse.secret'),
                'username' => config('services.sls.datawarehouse.username'),
                'password' => config('services.sls.datawarehouse.password'),
                'url' => 'api/oauth/token'
            ],
            'Members' => [
                'type' => 'rest',
                'host' => config('services.sls.members.host'),
                'client' => config('services.sls.members.client'),
                'secret' => config('services.sls.members.secret'),
                'url' => 'SLSA_MembersOnline/public/oauth/token'
            ],
            'Beachsafe' => [
                'type' => 'rest',
                'host' => config('services.sls.beachsafe.host'),
                'token' => config('services.sls.beachsafe.key')
            ]
        ];
    }
    

    
    /**
     * @param bool $username
     * @param bool $password
     * @return string
     * @throws \ErrorException
     * @internal param null $file
     */
    public function getUserToken($service = true, $username = false, $password = false)
    {

        // get config
        $rest = $this->auth[$service];

        if (env('APP_ENV') == 'testing') {
            $curl = new MockCurl();
        } else {
            $curl = new Curl\Curl();
        }

        $curl->setHeader('Accept', 'application/json');
        $payload = [
            'grant_type' => 'password',
            'client_id' => $rest['client'],
            'client_secret' => $rest['secret'],
            'username' => (!$username) ? $rest['username'] : $username,
            'password' => (!$password) ? $rest['password'] : $password,
        ];


        $curl->post($rest['host'] . '/' . $rest['url'], $payload);
        
        
        // errros
        if ($curl->error && $curl->http_status_code !== 401) {
            \Log::error('[Service Services getUserToken] Curl Error 😥 path: ' . $rest['host'].
                        " Error: " . json_encode($curl));
            return json_encode($curl->error_code);
        }

        // sucess ones
        if ($curl->http_status_code === 200) {
            \Log::debug('[Service Services getUserToken] GOT User token 😀 path: ' . $rest['host']);
            return json_decode($curl->response);
        } elseif ($curl->http_status_code === 401) {
            \Log::debug('[Service Services getUserToken] GOT 401 token 😥 path: ' . $rest['host']);
            return false;
        } else {
            \Log::error('[Service Services getUserToken] Bad response from REST API  ' . json_encode($curl));
            return false;
        }

        return $curl->response;
    }

    /**
     * @param string $service
     * @param string $method
     * @param $path
     * @return string
     * @internal param null $file
     */
    public function callREST($service, $method, $path = false, $token = false, $payload = [])
    {
        $rest = $this->auth[$service];
        if ($service == null) {
            \Log::error("[Services Service callREST] service Parameter is missing. {$service}");
        }

        if (env('APP_ENV') == 'testing') {
            $curl = new MockCurl();
        } else {
            $curl = new Curl\Curl();
        }


        $curl->setHeader('Accept', 'application/json');
        $curl->setUserAgent('Beach Management App - ' . env('APP_ENV'). ' ' . env('APP_VERSION'));
        // types of auth
        if (isset($rest['token'])) {
            $curl->setHeader('Authorization', 'Bearer ' . $rest['token']);
        } elseif ($token) {
            $curl->setHeader('Authorization', 'Bearer ' . $token);
        }

        $curl->{$method}($rest['host'] . '/' . $path, $payload);

        if ($curl->error) {
            \Log::error('[Service Services callREST] Curl Error :( path: ' .$rest['host'].'/'. $path . " code: " . $curl->error_code);
        }

        return $curl->response;
    }

    /**
     * Fetch url using curl
     *
     * @param null $file
     *
     * @return null
     */
    public function getUrl($file = null)
    {
        if ($file == null) {
            \Log::error("[Services Service getURL] File Parameter is missing. {$file}");
        }

        $curl = new Curl\Curl();
        $curl->get($file);

        if ($curl->error) {
            \Log::error('[Service Services getUrl] Curl Error :( file: ' . $file . " code: " . $curl->error_code);
        }

        return $curl->response;
    }
}
