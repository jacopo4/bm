<?php namespace App\Services\SLS;

use App\APIActivity;
use App\Entity;
use App\Services\Service;
use App\Beach;
use App\User;
use App\Patrol;
use App\PatrolPosition;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SLSMember extends Service
{


    /**
     * Authorize User
     *
     * @param bool $username
     * @param bool $password
     *
     * @return array
     */
    public function authenticateUser($username = true, $password = true)
    {
        // for now all calls are done via tunnel not VPN!

        try {
            // token first
            $response    = $this->getUserToken('Members', $username, $password);
            // get user details
            // $response['access_token']
            if ($response !== false && isset($response->access_token)) {
                $user = $this->callREST('Members', 'get', 'SLSA_MembersOnline/public/api/user', $response->access_token);
            } else {
                \Log::debug("[SLSMember authenticateUser] {$username} Failed to get user token");
            }
        } catch (\Exception $e) {
            // \Bugsnag::notifyException($e);
            \Log::error("[SLSMember authenticateUser] {$username} REST failed! " . $e->getMessage());

            return false;
        }

        if (! isset($response)) {
            \Log::error("[SLSMember authenticateUser] {$username} Can not get response! ");

            return false;
        }

        if (isset($user)) {
            \Log::info("[SLSMember authenticateUser] User Authenticated " . $username);

            return json_decode($user);
        }

        return false;
    }
}
