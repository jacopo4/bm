<?php namespace App\Services\SLS;

use App\Beach;
use App\Services\Service;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Sabre;
use Carbon\Carbon;

class Beachsafe extends Service
{

    /**
     * Return List of Beaches from API
     *
     * @param $beachKey
     * @return array
     */
    public function getBeach($beachKey)
    {
        try {
            $beach = $this->callREST("Beachsafe", 'get', 'api/v3/beach/' . $beachKey);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            \Log::error('[Beachsafe getBeach] REST fail! ' . $e->getMessage());
        }

        return json_decode($beach);
    }

    public function formatLocationWeather($beach)
    {
        $weather = [];

        $weather['weather'] = (isset($beach->weather)) ? $beach->weather : '';
        $weather['water_temperature'] = (isset($beach->water_temperature)) ? $beach->water_temperature : 0;

        foreach ($beach->weather_forecasts as $forecast) {
            if (Carbon::parse($forecast->date)->isToday()) {
                $weather['forecast'] = $forecast;
            }
        }

        $directions = [
            "North" => 0,
            "NE" => 2,
            "East" => 3,
            "E" => 90,
            "ENE" => 3,
            "SE" => 4,
            "South" => 180,
            "S" => 5,
            "SW" => 6,
            "SSW" => 6,
            "West" => 7,
            "W" => 270,
            "WNW" => 7,
            "NW" => 8,
            "NNW" => 8
        ];

        $tides = [];
        if (isset($beach->tides)) {
            foreach ($beach->tides as $tide) {
                if (Carbon::parse($tide->date_time_local)->isToday()) {
                    $hour = Carbon::parse($tide->date_time_local)->format("H");
                    //$time = ($tide->date_time_local->format('H') >= 12) ? '1' : '0';
                    $tides[$hour]['time'] = Carbon::parse($tide->date_time_local)->format("g a");
                    $tides[$hour]['hour'] = Carbon::parse($tide->date_time_local)->format("H");
                    $tides[$hour]['height'] = $tide->height;
                    $tides[$hour]['state'] = $tide->state;
                }
            }
        }

        $weather['tides'] = $tides;
        return $weather;
    }
}
