<?php namespace App\Services\SLS;

use App\User;
use App\Beach;
use App\Entity;
use App\Patrol;
use Carbon\Carbon;
use App\APIActivity;
use App\PatrolPosition;
use App\Services\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DataWarehouse extends Service
{

    protected $token;

    public function __construct()
    {
        parent::__construct();
        $response    = $this->getUserToken('DataWarehouse');
        $this->token = $response->access_token;
    }


    /**
     * getMembers
     *
     * @param mixed $offset
     * @param mixed $limit
     * @return void
     */
    public function getMembers($offset = false, $limit = 1000)
    {
        

        try {
            if ($offset) {
                // Somethine sset
                $append = "&SkipToken=".$offset;
            } else {
                $append = "";
            }

            $members = $this->callREST('DataWarehouse', 'get', 'api/members?MaxRecords='.$limit.$append, $this->token);
        } catch (\Exception $e) {
            // \Bugsnag::notifyException($e);
            \Log::error("[DataWarehouse getMembers] REST failed! " . $e->getMessage());

            return false;
        }

        if (isset($members)) {
            \Log::info("[DataWarehouse getMembers] Got Members");

            return json_decode($members);
        }
    }

    public function getEntities()
    {
        

        try {
            $members = $this->callREST('DataWarehouse', 'get', 'api/entities?IncludeLowerLevelOrgs=1', $this->token);
        } catch (\Exception $e) {
            // \Bugsnag::notifyException($e);
            \Log::error("[DataWarehouse getEntities] REST failed! " . $e->getMessage());

            return false;
        }

        if (isset($members)) {
            \Log::info("[DataWarehouse getEntities] Got Entities");

            return json_decode($members);
        }
    }
}
