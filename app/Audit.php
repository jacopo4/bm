<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Audit extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logName = 'Audit';
    protected static $logOnlyDirty = true;
    protected static $logFillable = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Audit  <strong>".$this->title."</strong> has been {$eventName}";
    }

    protected $hidden = [
        'deleted_at',
    ];

    protected $fillable = [
        //'id', // TODO this seem wrong?
        'assessment_id',
        'beach_id',
        'user_id',
        'title',
        'date_visit',
        'summary',
        'status',
        'beach_hazard_rating',
        'beach_type',
        'beach_fvr_development',
        'beach_fvr_population',
        'beach_fvr_frequency',
        'beach_local_population',
        'beach_human_water',
        'beach_human_water_conflict',
        'beach_human_beach',
        'beach_human_beach_conflict'

    ];

    protected $casts = [
        'date_visit' => 'datetime:Y-m-d'
    ];

    
    public function assessment()
    {
        return $this->belongsTo(Assessment::class);
    }

    public function beach()
    {
        return $this->belongsTo(Beach::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function objects()
    {
        return $this->HasMany(AuditObject::class);
    }
}
