<?php

namespace App\Http\Controllers;

use App\User;
use App\Assessment;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ExportController extends Controller
{

    public function preview(Request $request, Assessment $assessment)
    {
        $this->authorize('view', $assessment);

        $beach_local_population = [
            1 => "< 50 residents and/or < 200 non residents (domestic or overseas tourists)" ,
            2 => "50 - 250 residents and/or 21-100 non residents (domestic or overseas tourists)" ,
            3 => "250 - 1000 residents and/or 100-200 non residents (domestic or overseas tourists)" ,
            4 => "1000 - 2500 residents and/or 500-1000 non residents (domestic or overseas tourists)" ,
            5 => "2500+ residents and/or 1000 non residents (domestic or overseas tourists)"
        ];
        $beach_human_beach_conflict = [
            1 => "No conflicts reported" ,
            2 => "Isolated conflicts" ,
            3 => "Regular" ,
            4 => "Persistent" ,
            5 => "Persistent and dangerous"
        ];
        $beach_human_beach = [
            1 => "1-250" ,
            2 => "250-500" ,
            3 => "500-750" ,
            4 => "750-1000" ,
            5 => "1000+"
        ];
        $beach_human_water_conflict = [
            1 => "No conflicts reported" ,
            2 => "Isolated conflicts" ,
            3 => "Regular" ,
            4 => "Persistent" ,
            5 => "Persistent and dangerous"
        ];
        $beach_human_water = [
            1 => "1-25" ,
            2 => "25-50" ,
            3 => "50-75" ,
            4 => "75-100" ,
            5 => "100+"
        ];
        $beach_fvr_development = [
            1 => "Beach Rating 1 and 2" ,
            2 => "Beach Rating 3 and 4" ,
            3 => "Beach Rating 5 and 6" ,
            4 => "Beach Rating 7 and 8" ,
            5 => "Beach Rating 9 and 10"
        ];
        $beach_fvr_frequency = [
            1 => "An annual activity or event in held at the facility" ,
            2 => "An activity event takes place in the facility on a monthly basis" ,
            3 => "An activity event takes place in the facility on a weekly basis" ,
            4 => "An activity event takes place in the facility on a monthly basis" ,
            5 => "The facility is in continuous use for the majority of the day"
        ];
        $beach_fvr_population = [
            1 => "Greater than 500 people at a time" ,
            2 => "100 to 500 people at a time" ,
            3 => "50 to 100 people at a time" ,
            4 => "5 to 50 people at a time" ,
            5 => "Less than 5 people at a time"
        ];
        $likehood = [
            0.5 => "Rare (0.5)",
            1 => "Unlikely (1)",
            3 => "Possible (3)",
            6 => "Likely (6)",
            10 => "Almost certain (10)"
        ];
        $consequence = [
            1 => "Insignificant (1)",
            2 => "Minor (2)",
            5 => "Moderate (5)",
            10 => "Major (10)",
            20 => "Severe (20)"
        ];
        $types = [
           6 => 'Access',
           1 => 'Activities',
           19 => 'Services',
           13 => 'Support',
           16 => 'Hazards',
           12 => 'Equipments',
           14 => 'Facilities',
           28 => 'Signages'
        ];
        $assessment_types = [ 1 => "Standard"];
        $roles = [
            1 => "Admin",
            2 => "Auditor",
            3 => "Client"
        ];

        return view('exports.preview', compact('assessment', 'roles', 'assessment_types', 'types', 'likehood', 'consequence', 'beach_local_population', 'beach_human_beach_conflict', 'beach_human_beach', 'beach_human_water_conflict', 'beach_human_water', 'beach_fvr_development', 'beach_fvr_frequency', 'beach_fvr_population'));
    }
}
