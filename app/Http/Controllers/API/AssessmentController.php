<?php

namespace App\Http\Controllers\API;

use Auth;
use Storage;
use App\User;
use Validator;
use Carbon\Carbon;
use App\Assessment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\UserInvited;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Assessment as AssessmentResource;

class AssessmentController extends Controller
{

    public function create(Request $request)
    {
        $this->authorize('create', Assessment::class);

        $rules     = [
            'title' => ['required', 'string'],
            'area' => ['required', 'string'],
            'start_date' => ['required', 'date', 'after_or_equal:today'],
            'end_date' => ['required', 'date', 'after:start_date', 'before:delivery_date'],
            'delivery_date' => ['required', 'date'],
            'summary' => ['nullable', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }

        $delivery_date = ($request->delivery_date ? Carbon::createFromFormat("Y-m-d", $request->delivery_date) : null);
        $assessment = Assessment::make([
            'title'      => $request->title,
            'type'       => 1,
            'area'       => $request->area,
            'summary'    => $request->summary,
            'status'     => 1,
            'start_date' => Carbon::createFromFormat("Y-m-d", $request->start_date),
            'end_date'   => Carbon::createFromFormat("Y-m-d", $request->end_date),
            'delivery_date'   => $delivery_date
        ]);

        $assessment->save();
        $assessment->users()->attach(Auth::user()->id, ['role_id' => '1']);

        //           $assessment->beaches()->attach($request->beaches);
        //    $hiddenBeaches = $assessment->beaches()->select('id')->get();
        //    $assessment->beaches = $hiddenBeaches;

        return response()->json(['status' => 'ok', 'assessment' => new AssessmentResource($assessment)]);
    }

    public function update(Assessment $assessment, Request $request)
    {
        $this->authorize('update', $assessment);

        $rules     = [
            'title' => ['required', 'string'],
            'area' => ['required', 'string'],
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date'],
            'delivery_date' => ['required', 'date'],
            'summary' => ['nullable','string'],
            'status' => ['required', 'integer', 'in:1,2']
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
              $out = [];
              $out['error'] = true;
              $out['errors'] = $validator->errors();
              return response()->json($out);
        }

        $delivery_date = ($request->delivery_date ? Carbon::createFromFormat("Y-m-d", $request->delivery_date) : null);
        $assessment->title = $request->title;
        $assessment->type = $request->type;
        $assessment->area = $request->area;
        $assessment->summary = $request->summary;
        $assessment->status = $request->status;
        $assessment->start_date = Carbon::createFromFormat("Y-m-d", $request->start_date);
        $assessment->end_date = Carbon::createFromFormat("Y-m-d", $request->end_date);
        $assessment->delivery_date = $delivery_date;
        $assessment->save();

        return response()->json(['status' => 'ok', 'assessment' => $assessment]);
    }

    // public function delete(Assessment $assessment, Request $request)
    // {
    //     $assessment->delete();

    //     return response()->json(['status' => 'ok']);
    // }

    public function fetch(Assessment $assessment, Request $request)
    {
        $this->authorize('view', $assessment);

        $hiddenBeaches = $assessment->beaches()->select('id')->get();

        $assessment->beaches = $hiddenBeaches;
        return response()->json(['status' => 'ok', 'assessment' => $assessment]);
    }

    public function index(Request $request)
    {
        $this->authorize('index', Assessment::class);
        //$assessments = Assessment::with('beaches')->with('users')->with('audits')->with('objects')->get();

        $assessments = Auth::user()->assessments()->with('users')->get();
        // foreach ($assessments as $assessment) {
        //     $assessment->beaches->makeHidden([
        //      'updated_at',
        //      'created_at',
        //      'last_updated'
        //     ]);
        //     $assessment->users->makeHidden([
        //      'updated_at',
        //      'created_at'
        //     ]);
        //     $assessment->audits->makeHidden([
        //      'updated_at',
        //      'created_at'
        //     ]);
        //     $assessment->objects->makeHidden([
        //      'updated_at',
        //      'created_at'
        //     ]);
        // }
        return response()->json(['status' => 'ok', 'assessment' => $assessments]);
    }

    public function addMember(Request $request, Assessment $assessment, User $user)
    {
        $this->authorize('addMember', $assessment);

        $request->validate([
            'role_id' => ['required', 'integer'],
        ]);
        if ($user->assessments()->where('id', $assessment->id)->exists()) {
            return response()->json(['errors' => ['User' => ['User already belongs to assessment']]]);
        }
        $assessment->users()->attach($user->id, ['role_id' => $request->role_id]);
        $assessment->load('users:id,first_name,last_name');

        $user->notify(new UserInvited($assessment, Auth::user(), $request->role_id));

        return response()->json(['status' => 'ok', 'assessment' => $assessment]);
    }

    public function removeMember(Request $request, Assessment $assessment, User $user)
    {
        $this->authorize('removeMember', $assessment);

        $assessment->users()->detach($user->id);
        $assessment->load('users:id,first_name,last_name');
        return response()->json(['status' => 'ok', 'assessment' => $assessment]);
    }
}
