<?php

namespace App\Http\Controllers\API\Auth;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\SLS\SLSMember;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
 
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->SLSMember = new SLSMember;
    }

/**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function apiLogin(Request $request)
    {
        // in non produciton try local auth first
        if (!\App::environment('production')) {
            \Log::info('[LoginController apiLogin] local auth cos not in production');
            try {
                return $this->login($request);
            } catch (\Exception $e) {
                \Log::error('[LoginController apiLogin] Cound not login user localy.');
            }
        }

        $this->validateLogin($request);
        $response = $this->SLSMember->authenticateUser($request->username, $request->password);

        if ($response === false) {
            \Log::info('[LoginController apiLogin] Login failed for user : '.$request->username);
            return $this->sendFailedLoginResponse($request);
        }

        if ($response != false) {
            if (! isset($response->MemberID)) {
                return response()->json(["error" => "Your account is missing member_id"], 406);
            }
            
            $user = User::updateOrCreate(
                ['member_id' => $response->MemberID],
                [
                    
                    'username'         => $request->username,
                    'email'            => $response->EmailAddress,
                    'first_name'       => $response->FirstName,
                    'last_name'        => $response->Surname,
                    'dob'              => $response->DOB,
                    'officer_entities' => json_decode(json_encode($response->OfficerEntities), true),
                    'password'         => bcrypt($request->password),
                    'remote_last_updated_at' => Carbon::now()->subCentury()
                ]
            );
            $user->last_login_at      = Carbon::now();
            if (is_null($user->api_token)) {
                $user->api_token = Str::random(60);
            }
            $user->save();

            Auth::login($user, true);

            return $this->sendLoginResponse($request);
        }


        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {

        $this->clearLoginAttempts($request);

        $user = Auth::user();
        // check permissions here

        $out = [];

        $out['user'] =  $user->makeVisible(['api_token','officer_entities','acl']);

        return response()->json($out);
    }



      /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }
}
