<?php

namespace App\Http\Controllers\API;

use Auth;
use Validator;
use App\Recommendation;
use Carbon\Carbon;
use App\AuditObject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecommendationController extends Controller
{

    public function create(AuditObject $auditObject, Request $request)
    {

        $rules     = [
            'text' => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }

        $this->authorize('create', [Recommendation::class, $auditObject]);

        $recommendation = Recommendation::make([
            'text' => $request->text
        ]);
        $recommendation->assessment_id = $auditObject->assessment_id;
        $recommendation->beach_id = $auditObject->beach_id;
        $recommendation->audit_id = $auditObject->audit_id;
        $recommendation->audit_object_id = $auditObject->id;
        $recommendation->object_type_id = $auditObject->type_id;
        $recommendation->user_id = Auth::user()->id;
        $recommendation->recommendable_type = 'auditObject';
        $recommendation->recommendable_id = $auditObject->id;

        $recommendation->save();

        return response()->json(['status' => 'ok', 'recommendation' => $recommendation]);
    }

    public function update(AuditObject $auditObject, Recommendation $recommendation, Request $request)
    {
        $this->authorize('update', $recommendation);

        $rules     = [
            'text' => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }
        $recommendation->text = $request->text;
        $recommendation->save();

        return response()->json(['status' => 'ok', 'recommendation' => $recommendation]);
    }

    public function delete(AuditObject $auditObject, Recommendation $recommendation, Request $request)
    {
        $this->authorize('delete', $recommendation);

        $recommendation->delete();

        return response()->json(['status' => 'ok', 'recommendationId' => $recommendation->id]);
    }


    // public function fetch(Assessment $assessment, Request $request)
    // {
    //     $hiddenBeaches = $assessment->beaches()->select('id')->get();

    //     $assessment->beaches = $hiddenBeaches;
    //     return response()->json(['status' => 'ok', 'assessment' => $assessment]);
    // }

    public function index(Request $request)
    {
        $this->authorize('index', Recommendation::class);

        $recommendations = Recommendation::whereIn('assessment_id', Auth::user()->assessments->pluck('id'))->with(['beach','user','assessment','audit','auditObject'])->get();
        // foreach ($assessments as $assessment) {
        //     $assessment->beaches->makeHidden([
        //      'updated_at',
        //      'created_at',
        //      'last_updated'
        //     ]);
        //     $assessment->users->makeHidden([
        //      'updated_at',
        //      'created_at'
        //     ]);
        //     // $assessment->audits->makeHidden([
        //     //  'updated_at',
        //     //  'created_at'
        //     // ]);
        // }
        return response()->json(['status' => 'ok', 'recommendation' => $recommendations]);
    }
}
