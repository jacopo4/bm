<?php

namespace App\Http\Controllers\API;

use App\Entity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EntityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
            return response()->json(['status' => 'ok', 'entity' => Entity::all()]);
    }
}
