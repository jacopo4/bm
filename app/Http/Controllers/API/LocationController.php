<?php
namespace App\Http\Controllers\API;

use App\Beach;
use Illuminate\Http\Request;
use App\Services\SLS\Beachsafe;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{

    public function myLocation(Request $request)
    {
        $user = Auth::guard('api')->user();

        $request->validate([
            'latitude'  => 'required',
            'longitude' => 'required'
        ]);

        $user->latitude  = $request->latitude;
        $user->longitude = $request->longitude;
        $user->save();

        $place = [];
        $places      = app('geocoder')->reverse($user->latitude, $user->longitude)->get();
        if ($places->count()) {
            $place = $places->first()->toArray();
        }

        $beaches = Beach::select("beaches.*")
                        ->distance($user->latitude, $user->longitude)
                        ->closest()
                        ->nearby(100)
                        ->limit(20)
                        ->get();


        $weather = [];
        if ($beaches->first()) {
            $beachsafe = new Beachsafe();
            $beach     = $beachsafe->getBeach($beaches->first()->key);
            if (isset($beach) && isset($beach->weather)) {
                $weather = $beachsafe->formatLocationWeather($beach);
            }
        }


        return response()->json([
            'beaches' => $beaches,
            'place'   => $place,
            'weather' => $weather
        ]);
    }
}
