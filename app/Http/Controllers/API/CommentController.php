<?php

namespace App\Http\Controllers\API;

use Auth;
use Validator;
use App\Comment;
use Carbon\Carbon;
use App\AuditObject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{

    public function create(AuditObject $auditObject, Request $request)
    {

        $rules     = [
            'text' => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }

        $this->authorize('create', [Comment::class, $auditObject]);

        $comment = Comment::make([
            'text' => $request->text
        ]);
        $comment->assessment_id = $auditObject->assessment_id;
        $comment->beach_id = $auditObject->beach_id;
        $comment->audit_id = $auditObject->audit_id;
        $comment->audit_object_id = $auditObject->id;
        $comment->object_type_id = $auditObject->type_id;
        $comment->user_id = Auth::user()->id;
        $comment->commentable_type = 'auditObject';
        $comment->commentable_id = $auditObject->id;

        $comment->save();

        return response()->json(['status' => 'ok', 'comment' => $comment]);
    }

    public function update(AuditObject $auditObject, Comment $comment, Request $request)
    {
        $this->authorize('update', $comment);

        $rules     = [
            'text' => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }
        $comment->text = $request->text;
        $comment->save();

        return response()->json(['status' => 'ok', 'comment' => $comment]);
    }

    public function delete(AuditObject $auditObject, Comment $comment, Request $request)
    {
        $this->authorize('delete', $comment);

        $comment->delete();

        return response()->json(['status' => 'ok', 'commentId' => $comment->id]);
    }


    // public function fetch(Assessment $assessment, Request $request)
    // {
    //     $hiddenBeaches = $assessment->beaches()->select('id')->get();

    //     $assessment->beaches = $hiddenBeaches;
    //     return response()->json(['status' => 'ok', 'assessment' => $assessment]);
    // }

    public function index(Request $request)
    {
        $this->authorize('index', Comment::class);

        $comments = Comment::whereIn('assessment_id', Auth::user()->assessments->pluck('id'))->with(['beach','user','assessment','audit','auditObject'])->get();
        // foreach ($assessments as $assessment) {
        //     $assessment->beaches->makeHidden([
        //      'updated_at',
        //      'created_at',
        //      'last_updated'
        //     ]);
        //     $assessment->users->makeHidden([
        //      'updated_at',
        //      'created_at'
        //     ]);
        //     // $assessment->audits->makeHidden([
        //     //  'updated_at',
        //     //  'created_at'
        //     // ]);
        // }
        return response()->json(['status' => 'ok', 'comment' => $comments]);
    }
}
