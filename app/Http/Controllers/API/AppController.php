<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppController extends Controller
{

    public function index(Request $request)
    {
          $out     = [
              'version' => config('app.version'),
              'env' => config('app.env'),
              'name' => config('app.name'),
              'css' => '/css/app.css',
              'js' => '/js/app.js',
              'builded_at' => config('app.builded_at')
           ];

          return response()->json($out);
    }

    public function manifest(Request $request)
    {
        $out = [
            "start_url" =>  "/",
            "name" =>  config('app.name'),
            "short_name" =>  "SLSA BM ".config('app.env'),
            "description" =>  "Beach Management App",
            "icons" => [
                [
                    "src" =>  "/android-chrome-192x192.png",
                    "sizes" =>  "192x192",
                    "type" =>  "image/png"
                ],
                [
                "src" =>  "/android-chrome-512x512.png",
                    "sizes" =>  "512x512",
                    "type" =>  "image/png"
                ]
            ],
            "theme_color" =>  "#0060a9",
            "background_color" =>  "#0060a9",
            "display" =>  "standalone",
            "orientation" =>  "portrait",
            "scope" =>  "/",
            /**
             * Caution: If the user clicks a link in your app that navigates outside of the scope,
             * the link will open and render within the existing PWA window. If you want the link
             * to open in a browser tab, you must add target="_blank" to the <a> tag. On Android,
             * links with target="_blank" will open in a Chrome Custom Tab.
             */

        ];

        return response()->json($out);
    }
}
