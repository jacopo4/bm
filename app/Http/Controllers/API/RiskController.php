<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Risk;
use Validator;
use Carbon\Carbon;
use App\AuditObject;
use AuditObjectRiskSeeder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RiskController extends Controller
{

    public function create(Request $request)
    {
        $rules     = [
            'audit_object_id' => ['required'],
            'title' => ['required', 'string'],
            'likehood' => ['required'],
            'consequence' => ['required'],
            'exposure' => ['required'],
            'score' => ['required']
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }


        $auditObject = AuditObject::where('id', $request->audit_object_id)->firstOrFail();

        // if audit object not hazard then return error
        if ($auditObject->type_id !== 16) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = ['audit_object_id' => ['Audit Object Must be Hazard']];
            return response()->json($out);
        }

        $user = Auth::user();

        $this->authorize('create', [Risk::class, $auditObject]);

        $risk = Risk::create([
            'assessment_id'    => $auditObject->assessment_id,
            'beach_id'         => $auditObject->beach_id,
            'audit_id'         => $auditObject->audit_id,
            'user_id'          => $user->id,
            'audit_object_id'  => $auditObject->id,
            'object_type_id'   => $auditObject->type_id,
            'relatable_type'   => 'auditObject',
            'relatable_id'     => $auditObject->id,

            'title'            => $request->title,
            'likehood'         => $request->likehood,
            'consequence'      => $request->consequence,
            'exposure'         => $request->exposure,
            'score'            => $request->score
        ]);

        $risk->save();
        return response()->json(['status' => 'ok', 'risk' => $risk]);
    }

    public function update(Risk $risk, Request $request)
    {
        $this->authorize('update', $risk);

        $rules     = [
            'title' => ['required', 'string'],
            'likehood' => ['required'],
            'consequence' => ['required'],
            'exposure' => ['required'],
            'score' => ['required']
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }

        $risk->title = $request->title;
        $risk->likehood = $request->likehood;
        $risk->consequence = $request->consequence;
        $risk->exposure = $request->exposure;
        $risk->score = $request->score;
        $risk->save();

        return response()->json(['status' => 'ok', 'risk' => $risk]);
    }

    public function delete(Risk $risk, Request $request)
    {
        $risk->delete();

        return response()->json(['status' => 'ok', 'riskId' => $risk->id]);
    }

    public function index(Request $request)
    {
        $this->authorize('index', Risk::class);

        $risks = Risk::whereIn('assessment_id', Auth::user()->assessments->pluck('id'))->get();
        // foreach ($assessments as $assessment) {
        //     $assessment->beaches->makeHidden([
        //      'updated_at',
        //      'created_at',
        //      'last_updated'
        //     ]);
        //     $assessment->users->makeHidden([
        //      'updated_at',
        //      'created_at'
        //     ]);
        //     // $assessment->audits->makeHidden([
        //     //  'updated_at',
        //     //  'created_at'
        //     // ]);
        // }
        return response()->json(['status' => 'ok', 'risk' => $risks]);
    }
}
