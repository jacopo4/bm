<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Audit;
use App\Assessment;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuditController extends Controller
{

    /**
     * Createa new Audit
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $rules     = [
            'title' => ['required', 'string'],
            'beach_id' => ['required', 'string'],
            'assessment_id' => ['required'],
            'date_visit' => ['required', 'date']
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }

        $this->authorize('create', [Audit::class, Assessment::findOrFail($request->assessment_id)]);

        $date_visit = ($request->date_visit ? Carbon::createFromFormat("Y-m-d", $request->date_visit) : null);
        $audit = Audit::create([
            'title'      => $request->title,
            'date_visit' => $date_visit,
            'summary'    => $request->summary,
            'beach_id'    => $request->beach_id,
            'status'     => $request->status,
            'assessment_id'    => $request->assessment_id,
            'beach_hazard_rating'    => $request->beach_hazard_rating,
            'beach_type'    => $request->beach_type,
            'beach_fvr_development'    => $request->beach_fvr_development,
            'beach_fvr_population'    => $request->beach_fvr_population,
            'beach_fvr_frequency'    => $request->beach_fvr_frequency,
            'beach_local_population'    => $request->beach_local_population,
            'beach_human_water'    => $request->beach_human_water,
            'beach_human_water_conflict'    => null,
            'beach_human_beach'    => $request->beach_human_beach,
            'beach_human_beach_conflict'    => null,
            'user_id' => Auth::user()->id
        ]);

        $audit->save();

        // $audit->beaches()->attach($request->beach_id);
        // $hiddenBeaches = $audit->beaches()->select('id, title')->get();
        // $audit->beaches = $hiddenBeaches;

        return response()->json(['status' => 'ok', 'audit' => $audit]);
    }
    /**
     * Update Audit
     *
     * @param Audit $audit
     * @param Request $request
     * @return void
     */
    public function update(Audit $audit, Request $request)
    {
        $this->authorize('update', $audit);

        $rules     = [
            'title' => ['required', 'string'],
            'date_visit' => ['required', 'date']
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }

        $date_visit = ($request->date_visit ? Carbon::createFromFormat("Y-m-d", $request->date_visit) : null);
        $audit->title = $request->title;
        $audit->date_visit = $date_visit;
        $audit->summary = $request->summary;
        $audit->status = $request->status;
        $audit->beach_hazard_rating = $request->beach_hazard_rating;
        $audit->beach_type = $request->beach_type;
        $audit->beach_fvr_development = $request->beach_fvr_development;
        $audit->beach_fvr_population = $request->beach_fvr_population;
        $audit->beach_fvr_frequency = $request->beach_fvr_frequency;
        $audit->beach_local_population = $request->beach_local_population;
        $audit->beach_human_water = $request->beach_human_water;
        $audit->beach_human_water_conflict = null;
        $audit->beach_human_beach = $request->beach_human_beach;
        $audit->beach_human_beach_conflict = null;

        $audit->save();

        return response()->json(['status' => 'ok', 'audit' => $audit]);
    }

    public function index(Request $request)
    {
        $this->authorize('index', Audit::class);

        $audits = Audit::whereIn('assessment_id', Auth::user()->assessments->pluck('id'))->with('beach')->get();
        // foreach ($assessments as $assessment) {
        //     $assessment->beaches->makeHidden([
        //      'updated_at',
        //      'created_at',
        //      'last_updated'
        //     ]);
        //     $assessment->users->makeHidden([
        //      'updated_at',
        //      'created_at'
        //     ]);
        //     // $assessment->audits->makeHidden([
        //     //  'updated_at',
        //     //  'created_at'
        //     // ]);
        // }
        return response()->json(['status' => 'ok', 'audit' => $audits]);
    }
}
