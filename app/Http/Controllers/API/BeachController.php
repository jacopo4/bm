<?php

namespace App\Http\Controllers\API;

use Auth;
use Storage;
use App\Beach;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BeachController extends Controller
{
    public function fetch(Beach $beach, Request $request)
    {
        $beach = Beach::where('id', $beach->id)->distance(0, 0)->get()->makeHidden([
          'updated_at',
          'created_at',
          'last_updated'
        ]);

        return response()->json(['status' => 'ok', 'beach' => $beach]);
    }

    public function index(Request $request)
    {
        $beaches = Beach::select('id', 'absamp_id', 'key', 'title', 'state', 'image', 'longitude', 'latitude', 'metadata')->distance(0, 0)->get();
        return response()->json(['status' => 'ok', 'beaches' => $beaches]);
    }
}
