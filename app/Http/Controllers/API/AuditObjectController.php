<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Audit;
use Validator;
use Carbon\Carbon;
use App\AuditObject;
use App\ObjectSubtype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class AuditObjectController extends Controller
{

    public function create(Request $request)
    {

        $rules     = [
            'title' => ['required', 'string'],
            'type_id' => ['required', 'integer'],
            'subtype_id' => ['required', 'integer', 'exists:object_subtypes,id'],
            'audit_id' => ['required', 'integer']
        ];

        // send audit object id with the request and check if this is not null. If not null, the will need to attach. What if sent in wrong order?
        // maybe create the audit objects, and then sync the hazard attachments
        if ($request->metadata) {
            $rules = array_merge($rules, ObjectSubtype::find($request->subtype_id)->metadata_rules);
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }

        $audit = Audit::where('id', $request->audit_id)->firstOrFail();
        $user = Auth::user();

        $this->authorize('create', [AuditObject::class, $audit]);

        $metadata = $request->metadata ? $request->metadata : ObjectSubtype::find($request->subtype_id)->metadata; // if we have metadata from offline audit then use that, else default to metadata from ObjectSubtype

        if ($request->title == 'New') {
            $request->title = ObjectSubtype::find($request->subtype_id)->title;
        }

        $object = AuditObject::make([
            'title'         => $request->title,
            'type_id'       => $request->type_id,
            'subtype_id'    => $request->subtype_id,
            'audit_id'      => $request->audit_id,
            'assessment_id' => $audit->assessment_id,
            'beach_id'      => $audit->beach_id,
            'user_id'       => $user->id,
            'latitude'      => $request->latitude,
            'longitude'     => $request->longitude,
            'metadata'      => $metadata
        ]);

        $object->save();

        return response()->json(['status' => 'ok', 'object' => $object]);
    }

    public function update(AuditObject $object, Request $request)
    {
        $this->authorize('update', $object);

        $rules     = [
            'title' => ['sometimes', 'min:1', 'string'],
            'longitude' => ['sometimes'],
            'latitude' => ['sometimes'],
            // 'longitude' => ['sometimes', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            // 'latitude' => ['sometimes', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/']
        ];

        $rules = array_merge($rules, ObjectSubtype::find($object->subtype_id)->metadata_rules);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
              $out = [];
              $out['error'] = true;
              $out['errors'] = $validator->errors();
              return response()->json($out);
        }

        $object->update($validator->validated());

        return response()->json(['status' => 'ok', 'object' => $object]);
    }

    /**
     * Fetch all object
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $this->authorize('index', AuditObject::class);

        $auditObjects = AuditObject::whereIn('assessment_id', Auth::user()->assessments->pluck('id'))
                ->with(['hazards' => function ($query) {
                    $query->select('id');
                }])
                ->with(['signs' => function ($query) {
                    $query->select('id');
                }])->get();
        return response()->json(['status' => 'ok', 'audit_object' => $auditObjects]);
    }

    public function fetchTypes(Request $request)
    {
        $subtypes = ObjectSubtype::with(['type'])->get();
        return response()->json(['status' => 'ok', 'subtypes' => $subtypes]);
    }
}
