<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Audit;
use App\Media;
use Validator;
use Carbon\Carbon;
use App\AuditObject;
use App\ObjectSubtype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class MediaController extends Controller
{
    /**
     * Save Media file
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $rules     = [
            // 'id' => ['string'],
            'audit_object_id' => ['required', 'integer', 'exists:audit_object,id'],
            'name' => ['required', 'string'],
            'url' => ['required', 'string']
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }

        $object = AuditObject::where('id', $request->audit_object_id)->firstOrFail();
        $user = Auth::user();

        $this->authorize('create', [Media::class, $object]);

        // single
        $media = $object
                ->addMediaFromBase64($request->url)
                ->usingName($request->name)
                ->usingFileName($request->name)
                ->withCustomProperties([
                    "local_created_at" => $request->created_at,
                    "local_updated_at" => $request->updated_at,
                ]) //middle method
                ->toMediaCollection('photos');

        //$media->audit_object_id =

        $out = $media->toArray();
        $out['model_id'] = $request->model_id;
        // // multiple ple
        // $media = $object
        // ->addMultipleMediaFromRequest(['files'])
        // ->each(function ($fileAdder) {
        //     try {
        //         $media = $fileAdder->toMediaCollection('photos'); // 's3private'
        //         \Log::info('File addeded: '. $media->name);
        //     } catch (\Exception $e) {
        //         \Log::error('Could not import file:'. json_encode($fileAdder).' '.$e->getMessage());
        //     }
        // });


        return response()->json(['status' => 'ok', 'media' => $out]);
    }

    /**
     * Delete media
     *
     * @param Media $media
     * @return void
     */
    public function destroy(Media $media)
    {
        $this->authorize('delete', $media);

        $media->delete();

        return response()->json(['status'=> 'ok']);
    }

     /**
      * All media
      *
      * @return void
      */
    public function index()
    {
        $this->authorize('index', Media::class);

        $media = Media::whereIn('assessment_id', Auth::user()->assessments->pluck('id'))->get();

        return response()->json(['status' => 'ok', 'media' => $media]);
    }
}
