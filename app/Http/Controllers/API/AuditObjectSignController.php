<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Audit;
use Validator;
use Carbon\Carbon;
use App\AuditObject;
use App\ObjectSubtype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuditObjectSignController extends Controller
{
    public function create(AuditObject $object, Request $request)
    {
        $rules     = [
            'subtype_id' => ['required', 'integer', 'exists:object_subtypes,id'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = $validator->errors();
            return response()->json($out);
        }

        // Make sure our object is not sign type
        if ($object->type_id === 28) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = ['object' => ['Audit Object is a Sign']];
            return response()->json($out);
        }

        $subtype = ObjectSubtype::find($request->subtype_id);

        if ($subtype->type_id !== 28) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = ['subtype_id' => ['Subtype is not Hazard']];
            return response()->json($out);
        }

        $audit = $object->audit;
        $user = Auth::user();

        $this->authorize('create', [AuditObject::class, $audit]);

        $sign = AuditObject::make([
            'title'         => $object->title . ' ' . $subtype->title . ' sign',
            'type_id'       => 28, // make hazard type
            'subtype_id'    => $request->subtype_id,
            'audit_id'      => $object->audit_id,
            'assessment_id' => $audit->assessment_id,
            'beach_id'      => $audit->beach_id,
            'user_id'       => $user->id,
            'latitude'      => $object->latitude,
            'longitude'     => $object->longitude,
            'metadata'      => $subtype->metadata
        ]);

        $sign->save();

        $sign->signChildren()->attach($object);

        return response()->json(['status' => 'ok', 'object' => $object, 'sign' => $sign]);
    }

    public function update(AuditObject $object, AuditObject $sign, Request $request)
    {
        $this->authorize('update', $object);

        // check if in same audit
        if ($object->audit_id !== $sign->audit_id) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = ['audit_id' => ['Audit IDs do not match']];
            return response()->json($out);
        }

        // Make sure our object is sign type
        if ($sign->type_id !== 28) {
            $out = [];
            $out['error'] = true;
            $out['errors'] = ['sign_object_id' => ['Audit Object not a Sign']];
            return response()->json($out);
        }

        $object->signs()->attach($sign);

        return response()->json(['status' => 'ok', 'object' => $object, 'sign' => $sign]);
    }

    public function delete(AuditObject $object, AuditObject $sign, Request $request)
    {
        $this->authorize('update', $object);

        $object->signs()->detach($sign);

        return response()->json(['status' => 'ok', 'object' => $object, 'sign' => $sign]);
    }
}
