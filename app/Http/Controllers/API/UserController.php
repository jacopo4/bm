<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
  /**
   * Show the application users.
   *
   * @param Request $request
   *
   * @param null $role
   *
   * @return \Illuminate\Contracts\Support\Renderable
   * @throws \Illuminate\Auth\Access\AuthorizationException
   */
    public function search(Request $request, $role = null)
    {
        // $this->authorize('index', User::class);

        if ($request->search) {
            $users = User::select('first_name', 'last_name', 'id')->where(function ($query) use ($request) {
                $query->where('first_name', 'like', '%'.$request->search.'%')
                    ->orWhere('last_name', 'like', '%'.$request->search.'%');
            })->get();
        }

        return response()->json($users);
    }
}
