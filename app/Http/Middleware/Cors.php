<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {



        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Max-Age' => '86400',
            'Access-Control-Allow-Headers' => 'Content-Type, Origin, Authorization, X-Requested-With, Accept'
        ];


        if ($request->isMethod('OPTIONS')) {
            $response = response('OK', 200);
        } else {
            // Pass the request to the next middleware
            $response = $next($request);
        }

        $response->withHeaders($headers);
        return $response;
    }
}
