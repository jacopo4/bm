<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Assessment extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'type' => $this->type,
            'area' => $this->area,
            'summary' => $this->summary,
            'status' => $this->status,
            'beaches' => $this->beaches,
            'users' => User::collection($this->users),
            'start_date' => $this->start_date->format('Y-m-d'),
            'end_date' => $this->end_date->format('Y-m-d'),
            'delivery_date' => $this->delivery_date->format('Y-m-d'),
            'audits' => $this->audits,
            'objects' => $this->objects,
            'comments' => $this->comments,
            'media' => $this->media
        ];
    }
}
