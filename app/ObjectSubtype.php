<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;

class ObjectSubtype extends Model
{
    use SoftDeletes, LogsActivity;


    protected static $logName = 'ObjectSubtype';
    protected static $logOnlyDirty = true;
    //protected static $logAttributes = ['name', 'location'];
    protected static $submitEmptyLogs = false;
    protected static $logFillable = true;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Object Subtype  <strong>".$this->title."</strong> has been {$eventName}";
    }

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $fillable = [
        'id',
        'type_id',
        'title',
        'order',
        'metadata_type',
        'created_at',
        'updated_at'
    ];

    public function type()
    {
        return $this->belongsTo(ObjectType::class);
    }

    public function getMetadataAttribute()
    {
        $metadata = [];
        if ($this->metadata_type == 'ReefType') { // Reef
            $metadata = ['width' => null];
        } elseif ($this->metadata_type == 'AccessType') { // Access
            $metadata = ['width' => null,'gradient' => null, 'condition' => null, 'surface' => null];
        } elseif ($this->metadata_type == 'RipType') { // Rips
            $metadata = ['spacing' => null];
        } elseif ($this->metadata_type == 'EmergencyType') { // Emergency services
            $metadata = ['distance' => null, 'response_time' => null ];
        } elseif ($this->metadata_type == 'LifeSavingType') { // Life saving services
            $metadata = ['manager' => null, 'provider' => null ];
        } elseif ($this->metadata_type == 'HazardsType') { // Biological Hazards & Hazards
            $metadata = ['user_groups' => []];
        } elseif ($this->metadata_type == 'SignageType') { // Signage
            $metadata = ['symbols' => []];
        } elseif ($this->metadata_type == 'FacilityType') { // facilities
            $metadata = ['manager' => null, 'manager_phone' => null ];
        } elseif ($this->metadata_type == 'CarParkType') { // Car parks
            $metadata = ['manager' => null, 'manager_phone' => null, 'count' => null ];
        }
        return $metadata;
    }

    public static function metadataTypeFor($subtype)
    {
        if ($subtype == 170) { // Reef
            return 'ReefType';
        } elseif (in_array($subtype, [ 55, 56, 57, 58, 59, 60 ])) { // Access
            return 'AccessType';
        } elseif (in_array($subtype, [ 162, 163, 164, 177, 316, 317, 318, 319, 321, 510 ])) { // Rips
            return 'RipType';
        } elseif (in_array($subtype, [ 112, 113, 114, 115, 116 ])) { // Emergency services
            return 'EmergencyType';
        } elseif (in_array($subtype, [ 241, 242, 243 ])) { // Life saving services
            return 'LifeSavingType';
        } elseif (in_array($subtype, [ 81, 82, 83, 84, 85, 86, 87, 155, 156, 157, 158, 159, 160, 161, 165, 166, 167, 168, 169, 171, 172, 173, 174, 175, 176, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 511, 512, 513, 514 ])) { // Biological Hazards & Hazards
            return 'HazardsType';
        } elseif (in_array($subtype, [ 379, 380, 381, 382, 383, 384, 385 ])) { // Signage
            return 'SignageType';
        } elseif (in_array($subtype, [ 256, 257, 258, 259, 260, 261, 390, 149, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 117 ])) { // facilities
            return 'FacilityType';
        } elseif (in_array($subtype, [ 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 509 ])) { // facilities
            return 'FacilityType';
        } elseif (in_array($subtype, [ 95, 96, 98 ])) { // Car parks
            return 'CarParkType';
        }
    }

    public function getMetadataRulesAttribute()
    {
        $rules = [];
        if ($this->metadata_type == 'ReefType') { // Reef
            $rules = [
                'metadata' => ['sometimes'],
                'metadata.*' => [Rule::in(['width'])],
                'metadata.width' => ['nullable', 'string']
            ];
        } elseif ($this->metadata_type == 'AccessType') { // Access
            $rules = [
                'metadata' => ['sometimes'],
                'metadata.*' => [Rule::in(['width', 'gradient', 'condition', 'surface'])],
                'metadata.width' => ['nullable', 'integer'],
                'metadata.gradient' => ['nullable', 'integer'],
                'metadata.condition' => ['nullable', 'integer'],
                'metadata.surface' => ['nullable', 'integer'],
            ];
        } elseif ($this->metadata_type == 'RipType') { // Rips
            $rules = [
                'metadata' => ['sometimes'],
                'metadata.*' => [Rule::in(['spacing'])],
                'metadata.spacing' => ['nullable', 'string'],
            ];
        } elseif ($this->metadata_type == 'EmergencyType') { // Emergency services
            $rules = [
                'metadata' => ['sometimes'],
                'metadata.*' => [Rule::in(['distance', 'response_time'])],
                'metadata.distance' => ['nullable', 'string'],
                'metadata.response_time' => ['nullable', 'string'],
            ];
        } elseif ($this->metadata_type == 'LifeSavingType') { // Life saving services
            $rules = [
                'metadata' => ['sometimes'],
                'metadata.*' => [Rule::in(['manager', 'provider'])],
                'metadata.manager' => ['nullable', 'string'],
                'metadata.provider' => ['nullable', 'string'],
            ];
        } elseif ($this->metadata_type == 'HazardsType') { // Biological Hazards & Hazards
            $rules = [
                'metadata' => ['sometimes'],
                'metadata.*' => [Rule::in(['user_groups'])],
                'metadata.user_groups' => ['sometimes', 'array'],
                'metadata.user_groups.*' => ['sometimes', 'integer'],
            ];
        } elseif ($this->metadata_type == 'SignageType') { // Signage
            $rules = [
                'metadata' => ['sometimes'],
                'metadata.*' => [Rule::in(['symbols'])],
                'metadata.symbols' => ['sometimes', 'array'],
                'metadata.symbols.*' => ['sometimes', 'array'],
                'metadata.symbols.*.symbol' => ['required', 'string'],
                'metadata.symbols.*.type' => ['required', 'integer'],
                'metadata.symbols.*.classification' => ['required', 'integer'],
            ];
        } elseif ($this->metadata_type == 'FacilityType') { // facilities
            $rules = [
                'metadata' => ['sometimes'],
                'metadata.*' => [Rule::in(['manager', 'manager_phone','count','distance'])],
                'metadata.manager' => ['nullable', 'string'],
                'metadata.manager_phone' => ['nullable', 'string'],
                'metadata.count' => ['nullable', 'integer'],
                'metadata.distance' => ['nullable', 'integer']
            ];
        } elseif ($this->metadata_type == 'CarParkType') { // Car parks
            $rules = [
                'metadata' => ['sometimes'],
                'metadata.*' => [Rule::in(['manager', 'manager_phone', 'count','distance'])],
                'metadata.manager' => ['nullable', 'string'],
                'metadata.manager_phone' => ['nullable', 'string'],
                'metadata.count' => ['nullable', 'integer'],
                'metadata.distance' => ['nullable', 'integer']
            ];
        }
        return $rules;
    }
}
