<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObjectType extends Model
{
    use SoftDeletes, LogsActivity;
    

    protected static $logName = 'ObjectType';
    protected static $logOnlyDirty = true;
    //protected static $logAttributes = ['name', 'location'];
    protected static $submitEmptyLogs = false;
    protected static $logFillable = true;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Object Type  <strong>".$this->title."</strong> has been {$eventName}";
    }

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $fillable = [
        'id',
        'title',
        'is_group',
        'explanation',
        'created_at',
        'updated_at'
    ];

    public function subtypes()
    {
        return $this->hasMany(ObjectSubtype::class, 'type_id');
    }
}
