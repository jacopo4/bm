<?php

namespace App;

use DB;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Beach extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, LogsActivity;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at',
    ];

    protected $fillable = [
        'id',
        'absamp_id',
        'key',
        'title',
        'state',
        'metadata'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
      'metadata' => 'array'
    ];

    protected static $logName = 'Beach';
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;
    //protected static $logAttributes = ['name', 'location'];
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Beach  <strong>".$this->title."</strong> has been {$eventName}";
    }

    // RELATIONS
    public function assessments()
    {
        return $this->belongsToMany(Assessment::class)->withTimestamps();
    }


    public function audits()
    {
        return $this->HasMany(Audit::class);
    }

    public function objects()
    {
        return $this->HasMany(AuditObject::class);
    }

    // MEDIA?
    // how to add file -  $b->addMedia("sunset-small.jpg")->toMediaCollection('photo');
    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('photo')
            ->singleFile();
    }

    public function getPhotoAttribute()
    {
        return $this->getFirstMedia('photo')->getFullUrl();
    }

    public function getPhotoThumbAttribute()
    {
        return $this->getFirstMedia('photo')->getFullUrl('thumb');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CONTAIN, 150, 150)
            ->nonQueued();
    }


    // DISTANCE
    public function scopeDistance($query, $latitude, $longitude)
    {
        return $query->addSelect(DB::raw(static::distanceSql($latitude, $longitude) . ' AS distance'));
    }

    protected static function distanceSql($nearLat, $nearLon)
    {
        $lat = "latitude * PI()/ 180";
        $nearLat = "$nearLat * PI() / 180";
        $lonDiff = "($nearLon - longitude) * PI() / 180";
        $distance = "((ACOS(SIN($nearLat) * SIN($lat) + COS($nearLat) * COS($lat) * COS($lonDiff)) * 180 / PI())"
            . " * 60 * 1.1515 * 1.609344)";

        return "IF(longitude>0, $distance, null)";
    }

    public function scopeClosest($query)
    {
        return $query->orderBy('distance')
            ->having('distance', '>=', 0);
    }

    public function scopeNearby($query, $range = 50)
    {
        if (is_numeric($range)) {
            return $query->having('distance', '<=', $range);
        }

        return $query;
    }
}
