<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Assessment extends Model
{

    use LogsActivity;
    
    protected static $logName = 'Assessment';
    protected static $logOnlyDirty = true;
    protected static $logFillable = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Assessment  <strong>".$this->title."</strong> has been {$eventName}";
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'type', 'area', 'status', 'summary', 'start_date', 'end_date', 'delivery_date'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'datetime:Y-m-d', 'end_date' => 'datetime:Y-m-d', 'delivery_date' => 'datetime:Y-m-d'
    ];



    public function beaches()
    {
        return $this->belongsToMany(Beach::class)->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('role_id')->withTimestamps();
    }

    public function audits()
    {
        return $this->HasMany(Audit::class);
    }

    public function objects()
    {
        return $this->HasMany(AuditObject::class);
    }
}
