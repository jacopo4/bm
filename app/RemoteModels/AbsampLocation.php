<?php

namespace App\RemoteModels;

use App\RemoteModels\AbsampBeach;
use Illuminate\Database\Eloquent\Model;

class AbsampLocation extends Model
{
     #use Actionable;
    protected $connection = 'absamp';
    protected $table = 'beach_location';
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_last_modified';
     #public $incrementing = false;

    protected $dates = [
       'date_created',
       'date_last_modified'
     ];

    protected $guarded = [];


    public function beach()
    {
        return $this->belongsTo(AbsampBeach::class);
    }
}
