<?php

namespace App\RemoteModels;

use Illuminate\Database\Eloquent\Model;

class AbsampBeachHazardRating extends Model
{
     #use Actionable;
    protected $connection = 'absamp';
    protected $table = 'beach_hazard_ratings';
    // const CREATED_AT = 'date_created';
    // const UPDATED_AT = 'date_last_modified';
    protected $primaryKey = 'bhrid';
     #public $incrementing = false;

    protected $dates = [
       'date_created',
       'date_last_modified'
     ];

    protected $guarded = [];
    //

    public function beach()
    {
        return $this->belongsTo(AbsampBeach::class, 'beach_key');
    }
}
