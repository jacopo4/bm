<?php

namespace App\RemoteModels;

use App\RemoteModels\AbsampAttributes;
use Illuminate\Database\Eloquent\Model;

class AbsampAttributeCodes extends Model
{
     #use Actionable;
    protected $connection = 'absamp';
    protected $table = 'attribute_codes';
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_last_modified';
    protected $primaryKey = 'acdid';
     #public $incrementing = false;

    protected $dates = [
       'date_created',
       'date_last_modified'
     ];

    protected $guarded = [];


    
    public function attributes()
    {
        return $this->hasMany(AbsampAttributes::class, 'acdid');
    }
}
