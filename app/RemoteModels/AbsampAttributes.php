<?php

namespace App\RemoteModels;

use App\RemoteModels\AbsampAttributeCodes;
use Illuminate\Database\Eloquent\Model;

class AbsampAttributes extends Model
{
     #use Actionable;
    protected $connection = 'absamp';
    protected $table = 'attributes';
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_last_modified';
    protected $primaryKey = 'attid';
     #public $incrementing = false;

    protected $dates = [
       'date_created',
       'date_last_modified'
     ];

    protected $guarded = [];
    
    public function code()
    {
        return $this->belongsTo(AbsampAttributeCodes::class, 'acdid');
    }
}
