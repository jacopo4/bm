<?php

namespace App\RemoteModels;

use App\RemoteModels\AbsampLocation;
use App\RemoteModels\AbsampCoordinates;
use Illuminate\Database\Eloquent\Model;

class AbsampBeach extends Model
{
     #use Actionable;
    protected $connection = 'absamp';
    protected $table = 'beaches';
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_last_modified';
    protected $primaryKey = 'beachid';
     #public $incrementing = false;

    protected $dates = [
       'date_created',
       'date_last_modified'
     ];

    protected $guarded = [];


    public function locations()
    {
        return $this->hasMany(AbsampLocation::class, 'beachid');
    }

    public function coordinates()
    {
        return $this->hasOne(AbsampCoordinates::class, 'beachid');
    }

    public function rating()
    {
        return $this->belongsTo(AbsampBeachHazardRating::class, 'beachid');
    }
}
