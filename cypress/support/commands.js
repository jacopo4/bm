import '@percy/cypress'
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('store', (getter, fn) => cy.window().its('cypressVue.$store.getters').invoke(getter).then((objects) => objects.filter(fn)))

Cypress.Commands.add("dusk", (selector) => {
    return cy.get(`[dusk='${selector}']`)
})
Cypress.Commands.add("assertPathIs", (path) => {
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq(path)
    })
})
Cypress.Commands.add("confirmSwal", (path) => {
    cy.get('.swal2-confirm').click()
})
Cypress.Commands.add("cancelSwal", (path) => {
    cy.get('.swal2-cancel').click()
})

Cypress.Commands.add("typeToButton", (type_id) => {
    switch (type_id) {
        case 6:
            return cy.dusk("Audit-Access")
        case 1:
            return cy.dusk("Audit-Activities")
        case 19:
            return cy.dusk("Audit-Services")
        case 13:
            return cy.dusk("Audit-Support")
        case 16:
            return cy.dusk("Audit-Hazards")
        case 12:
            return cy.dusk("Audit-Equipment")
        case 14:
            return cy.dusk("Audit-Facilities")
        case 28:
            return cy.dusk("Audit-Signages")
    }
})


const COMMAND_DELAY = 300;


for (const command of ['visit', 'click', 'trigger', 'type', 'clear', 'reload', 'contains', 'get']) {
    Cypress.Commands.overwrite(command, (originalFn, ...args) => {
        const origVal = originalFn(...args);

        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(origVal);
            }, COMMAND_DELAY);
        });
    });
}
