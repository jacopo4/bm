context('Risks', () => {
  beforeEach(() => {
    cy.server()
    cy.route('GET', '/api/v1/**').as('apiCall')
    cy.route('POST', '/api/v1/myLocation').as('locationRequest')
    cy.route('POST', '/api/v1/login').as('loginRequest')
    cy.visit('/')
    cy.location().then((loc) => {
      if (loc.pathname === '/login') {
        cy.get('#username').type('admin')
        cy.get('#password').type('n4n0')
        cy.get('#login-user').click()
        cy.wait('@loginRequest')
        cy.get('.swal2-cancel').click()
        cy.wait(['@apiCall', '@locationRequest'])
      } else {
        cy.log("we are logged in")
        cy.dusk("logout").click()
        cy.confirmSwal()
        cy.get('#username').type('admin')
        cy.get('#password').type('n4n0')
        cy.get('#login-user').click()
        cy.wait('@loginRequest')
        cy.get('.swal2-cancel').click()
        cy.wait(['@apiCall', '@locationRequest'])
      }
    })

    cy.contains('SLSA Beach Management')
    cy.wait(1000)
  })

  afterEach(() => {
    cy.screenshot({capture: 'viewport'});
  })

  it('Creates a Risk', () => {
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let filteredObjects = auditObjects.filter(auditObject => auditObject.audit_id === 1 && auditObject.type_id === 16)
        let random = filteredObjects.sort(() => .5 - Math.random()).slice(0,1)
        let auditObject = random[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk('Audit-1').click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.dusk('New-Risk').click()
        cy.dusk('input-risk-title').type('Test Risk Title')
        cy.dusk('radio-likehood-0.5').click()
        cy.dusk('radio-consequence-1').click()
        // cy.dusk('radio-exposure-1').click()
        cy.dusk('Risk-Save').click()
        cy.confirmSwal()
        cy.contains('Test Risk Title')
        cy.percySnapshot()
      })
  })

  it('Edits a Risk', () => {
    cy.route('POST', '/api/v1/risks/**').as('riskRequest')
    cy.fixture('risks.json')
      .then((risks) => {
        let risk = risks.sort(() => .5 - Math.random()).slice(0,1)[0]
        cy.fixture('audits.json')
          .then((audits) => {
            let audit = audits.filter(audit => audit.id === risk.audit_id)[0]
            cy.fixture('auditObjects.json')
              .then((auditObjects) => {
                let auditObject = auditObjects.filter(auditObject => auditObject.id === risk.audit_object_id && auditObject.type_id === 16)[0]
                cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
                cy.dusk(`Audit-${audit.id}`).click({force: true})
                cy.typeToButton(auditObject.type_id).click()
                cy.dusk(`Object-${auditObject.id}`).click()
                cy.dusk(`Edit-Risk-${risk.id}`).click()
                cy.dusk('input-risk-title').clear().type('Edit Risk Title')
                cy.dusk('radio-likehood-1').click()
                cy.dusk('radio-consequence-5').click()
                // cy.dusk('radio-exposure-10').click()
                cy.dusk('Risk-Save').click()
                cy.wait('@riskRequest')
                cy.confirmSwal()
                cy.contains('Edit Risk Title')
                cy.percySnapshot()
              })
          })
      })
  })

  it('Delete a Risk', () => {
    cy.route('DELETE', '/api/v1/risks/**').as('riskRequest')
    cy.fixture('risks.json')
      .then((risks) => {
        let risk = risks.filter(risk => risk.audit_id === 1)[0]
        cy.fixture('auditObjects.json')
          .then((auditObjects) => {
            let auditObject = auditObjects.filter(auditObject => auditObject.id === risk.audit_object_id && auditObject.type_id === 16)[0]
            cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
            cy.dusk('Audit-1').click({force: true})
            cy.typeToButton(auditObject.type_id).click()
            cy.dusk(`Object-${auditObject.id}`).click()
            cy.dusk(`Delete-Risk-${risk.id}`).click()
            cy.confirmSwal()
            cy.wait('@riskRequest')
            cy.confirmSwal()
            cy.contains(risk.title).should('not.exist')
            cy.percySnapshot()
          })
      })
  })
})
