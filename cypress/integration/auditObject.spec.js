context('Audit Objects', () => {
  beforeEach(() => {
    cy.server()
    cy.route('GET', '/api/v1/**').as('apiCall')
    cy.route('POST', '/api/v1/myLocation').as('locationRequest')
    cy.route('POST', '/api/v1/login').as('loginRequest')
    cy.visit('/')
    cy.location().then((loc) => {
      if (loc.pathname === '/login') {
        cy.get('#username').type('admin')
        cy.get('#password').type('n4n0')
        cy.get('#login-user').click()
        cy.wait('@loginRequest')
        cy.get('.swal2-cancel').click()
        cy.wait(['@apiCall', '@locationRequest'])
      } else {
        cy.log("we are logged in")
        cy.dusk("logout").click()
        cy.confirmSwal()
        cy.get('#username').type('admin')
        cy.get('#password').type('n4n0')
        cy.get('#login-user').click()
        cy.wait('@loginRequest')
        cy.get('.swal2-cancel').click()
        cy.wait(['@apiCall', '@locationRequest'])
      }
    })

    cy.contains('SLSA Beach Management')
    cy.wait(1000)
  })

  afterEach(() => {
    cy.screenshot({capture: 'viewport'});
  })

  it('Can Create a Hazard on Audit Object', () => {
    cy.route('POST', '/api/v1/object/**/create').as('hazardRequest')
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.audit_id === 1 && auditObject.type_id !== 16)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
        cy.contains(auditObject.title)
        cy.dusk('Create-Hazard').click()
        cy.confirmSwal()
        cy.wait('@hazardRequest')
        cy.confirmSwal()
        cy.contains(`${auditObject.title} Accessible Rock Platforms hazard`)
        cy.percySnapshot()
      })
  })


  it('Can Remove a Hazard from Audit Object', () => {
    cy.route('DELETE', '/api/v1/object/**').as('hazardDeleteRequest')
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.audit_id === 1 && auditObject.type_id !== 16)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
        cy.contains(auditObject.title)
        cy.dusk(`Remove-Hazard-0`).click()
        cy.confirmSwal()
        cy.wait('@hazardDeleteRequest')
        cy.confirmSwal()
        cy.contains(`Be the first to assign a hazard.`)
        cy.percySnapshot()
      })
  })

  it('Can Attach a Hazard to Audit Object', () => {
    cy.route('POST', '/api/v1/object/**').as('hazardRequest')
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.audit_id === 1 && auditObject.type_id !== 16)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
        let hazard = auditObjects.filter(hazard => hazard.audit_id === 1 && hazard.type_id === 16)[0]
        cy.contains(auditObject.title)
        cy.dusk('Add-Hazard').click()
        cy.confirmSwal()
        cy.wait('@hazardRequest')
        cy.confirmSwal()
        cy.contains(`${hazard.title}`)
        cy.percySnapshot()
      })
  })

  it('Can Create a Sign on Audit Object', () => {
    cy.route('POST', '/api/v1/object/**/create').as('signRequest')
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.audit_id === 1 && auditObject.type_id !== 28)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
        cy.contains(auditObject.title)
        cy.dusk('Create-Sign').click()
        cy.confirmSwal()
        cy.wait('@signRequest')
        cy.confirmSwal()
        cy.contains(`${auditObject.title} Beach Flags sign`)
        cy.percySnapshot()
      })
  })


  it('Can Remove a Sign from Audit Object', () => {
    cy.route('DELETE', '/api/v1/object/**').as('signDeleteRequest')
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.audit_id === 1 && auditObject.type_id !== 28)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
        cy.contains(auditObject.title)
        cy.dusk(`Remove-Sign-0`).click()
        cy.confirmSwal()
        cy.wait('@signDeleteRequest')
        cy.confirmSwal()
        cy.contains(`Be the first to assign a sign.`)
        cy.percySnapshot()
      })
  })

  it('Can Attach a Sign to Audit Object', () => {
    cy.route('POST', '/api/v1/object/**').as('signRequest')
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.audit_id === 1 && auditObject.type_id !== 28)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
        let sign = auditObjects.filter(sign => sign.audit_id === 1 && sign.type_id === 28)[0]
        cy.contains(auditObject.title)
        cy.dusk('Add-Sign').click()
        cy.confirmSwal()
        cy.wait('@signRequest')
        cy.confirmSwal()
        cy.contains(`${sign.title}`)
        cy.percySnapshot()
      })
  })

  it('Adds a Comment', () => {
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.audit_id === 1)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
        cy.contains(auditObject.title)
        cy.dusk('New-Comment').type('New Test Comment')
        cy.dusk('Save-Comment').click()
        cy.get('.swal2-confirm').click()
        cy.contains('New Test Comment')
        cy.percySnapshot()
      })
  })

  it('Edits a Comment', () => {
    cy.fixture('comments.json')
      .then((comments) => {
        let comment = comments.filter(comment => comment.audit_id === 1)[0]
        cy.fixture('auditObjects.json')
          .then((auditObjects) => {
            let auditObject = auditObjects.filter(auditObject => auditObject.id === comment.audit_object_id)[0]
            cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
            cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
            cy.typeToButton(auditObject.type_id).click()
            cy.dusk(`Object-${auditObject.id}`).click()
            cy.dusk(`Edit-Comment-${comment.id}`).click()
            cy.dusk('Comment-Update-Text').clear().type('New Test Comment')
            cy.dusk('Comment-Cancel-Update').click() // test cancel
            cy.contains(comment.text)
            cy.dusk(`Edit-Comment-${comment.id}`).click()
            cy.dusk('Comment-Update-Text').clear().type('New Edited Comment')
            cy.dusk('Comment-Save-Update').click()
            cy.confirmSwal()
            cy.contains('New Edited Comment')
            cy.percySnapshot()
          })
      })

  })

  it('Delete a Comment', () => {
    cy.route('DELETE', '/api/v1/object/**').as('deleteRequest')
    cy.fixture('comments.json')
      .then((comments) => {
        let comment = comments.filter(comment => comment.audit_id === 1)[0]
        cy.fixture('auditObjects.json')
          .then((auditObjects) => {
            let auditObject = auditObjects.filter(auditObject => auditObject.id === comment.audit_object_id)[0]
            cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
            cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
            cy.typeToButton(auditObject.type_id).click()
            cy.dusk(`Object-${auditObject.id}`).click()
            cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
            cy.dusk(`Delete-Comment-${comment.id}`).click()
            cy.confirmSwal()
            cy.wait('@deleteRequest')
            cy.confirmSwal()
            cy.percySnapshot()
          })
      })
  })

  it('Adds a Recommendation', () => {
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.audit_id === 1)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
        cy.contains(auditObject.title)
        cy.dusk('New-Recommendation').type('New Test Recommendation')
        cy.dusk('Save-Recommendation').click()
        cy.get('.swal2-confirm').click()
        cy.contains('New Test Recommendation')
        cy.percySnapshot()
      })
  })

  it('Edits a Recommendation', () => {
    cy.fixture('recommendations.json')
      .then((recommendations) => {
        let recommendation = recommendations.filter(recommendation => recommendation.audit_id === 1)[0]
        cy.fixture('auditObjects.json')
          .then((auditObjects) => {
            let auditObject = auditObjects.filter(auditObject => auditObject.id === recommendation.audit_object_id)[0]
            cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
            cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
            cy.typeToButton(auditObject.type_id).click()
            cy.dusk(`Object-${auditObject.id}`).click()
            cy.dusk(`Edit-Recommendation-${recommendation.id}`).click()
            cy.dusk('Recommendation-Update-Text').clear().type('New Test Recommendation')
            cy.dusk('Recommendation-Cancel-Update').click() // test cancel
            cy.contains(recommendation.text)
            cy.dusk(`Edit-Recommendation-${recommendation.id}`).click()
            cy.dusk('Recommendation-Update-Text').clear().type('New Edited Recommendation')
            cy.dusk('Recommendation-Save-Update').click()
            cy.confirmSwal()
            cy.contains('New Edited Recommendation')
            cy.percySnapshot()
          })
      })
  })

  it('Delete a Recommendation', () => {
    cy.route('DELETE', '/api/v1/object/**').as('deleteRequest')
    cy.fixture('recommendations.json')
      .then((recommendations) => {
        let recommendation = recommendations.filter(recommendation => recommendation.audit_id === 1)[0]
        cy.fixture('auditObjects.json')
          .then((auditObjects) => {
            let auditObject = auditObjects.filter(auditObject => auditObject.id === recommendation.audit_object_id)[0]
            cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
            cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
            cy.typeToButton(auditObject.type_id).click()
            cy.dusk(`Object-${auditObject.id}`).click()
            cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObject.id}`)
            cy.dusk(`Delete-Recommendation-${recommendation.id}`).click()
            cy.confirmSwal()
            cy.wait('@deleteRequest')
            cy.confirmSwal()
            cy.percySnapshot()
          })
      })
  })

  it('Creates an AuditObject', () => {
    cy.route('POST', '/api/v1/object').as('newObjectRequest')
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.audit_id === 1)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.dusk('Audit-Activities').click()
        cy.dusk('New-Audit-Object').click()
        cy.confirmSwal()
        cy.wait('@newObjectRequest')
        cy.confirmSwal()
        cy.assertPathIs(`/assessments/${auditObject.assessment_id}/audit/${auditObject.audit_id}/object/${auditObjects.length+3}`)
        cy.percySnapshot()
      })
  })

  it('Edits Details', () => {
    cy.route('POST', '/api/v1/object/**').as('newObjectRequest')
    cy.fixture('auditObjects.json')
      .then((auditObjects) => {
        let auditObject = auditObjects.filter(auditObject => auditObject.subtype_id === 194)[0]
        cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
        cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
        cy.typeToButton(auditObject.type_id).click()
        cy.dusk(`Object-${auditObject.id}`).click()
        cy.dusk('AuditObject-Edit').click()
        cy.dusk('input-title').clear().type('Updated AuditObject Title')
        // cy.dusk('select-type').children().then((children) => {
        //     cy.dusk('select-type').select(`${children[2].value}`)
        // })
        cy.dusk('AuditObject-Cancel-Update').click()
        cy.confirmSwal()
        cy.contains(auditObject.title) // test cancel
        cy.dusk('AuditObject-Edit').click()
        cy.dusk('input-title').clear().type('Updated AuditObject Title')
        // cy.dusk('select-type').children().then((children) => {
        //     cy.dusk('select-type').select(`${children[2].value}`)
        // }) don't change type
        cy.dusk('AuditObject-Save-Update').click()
        cy.confirmSwal()
        cy.wait('@newObjectRequest')
        cy.wait(2000)
        cy.contains('Updated AuditObject Title')
        cy.percySnapshot()
      })

  })

  it('Edits a Risk', () => {
    cy.route('POST', '/api/v1/risks/**').as('riskRequest')
    cy.fixture('risks.json')
      .then((risks) => {
        let risk = risks.filter(risk => risk.audit_id === 1)[0]
        cy.fixture('auditObjects.json')
          .then((auditObjects) => {
            let auditObject = auditObjects.filter(auditObject => auditObject.id === risk.audit_object_id)[0]
            cy.dusk(`Assessment-Audit-${auditObject.assessment_id}`).click()
            cy.dusk(`Audit-${auditObject.audit_id}`).click({force: true})
            cy.typeToButton(auditObject.type_id).click()
            cy.dusk(`Object-${auditObject.id}`).click()
            cy.dusk(`Edit-Risk-${risk.id}`).click()
            cy.dusk('input-risk-title').clear().type('Edit Risk Title')
            cy.dusk('radio-likehood-3').click()
            cy.dusk('radio-consequence-5').click()
            // cy.dusk('radio-exposure-5').click()
            cy.dusk('Risk-Save').click()
            cy.confirmSwal()
            cy.wait('@riskRequest')
            cy.percySnapshot()
          })
      })

  })

  it('Creates an AuditObject On Empty Audit', () => {
    cy.route('POST', '/api/v1/object').as('objectRequest')
    cy.window()
      .its('cypressVue')
      .then(cypressVue => {
        let numAudits = cypressVue.$store.getters[`database/audits/all`]().length
        cy.fixture('audits.json')
          .then((audits) => {
            let audit = audits.filter(audit => audit.id === 5)[0]
            cy.dusk(`Assessment-Audit-3`).click()
            cy.dusk(`Audit-${numAudits}`).click({force: true})
            cy.dusk('Audit-Hazards').click()
            cy.dusk('New-Audit-Object').click()
            cy.confirmSwal()
            cy.wait('@objectRequest')
            cy.confirmSwal()
            cy.percySnapshot()
          })
      })
  })

  it('Adds Comment to Empty AuditObject', () => {
    cy.route('POST', '/api/v1/object').as('objectRequest')
    cy.window()
      .its('cypressVue')
      .then(cypressVue => {
        let numAudits = cypressVue.$store.getters[`database/audits/all`]().length
        let numObjects = cypressVue.$store.getters[`database/auditObjects/all`]().length
        cy.dusk(`Assessment-Audit-3`).click()
        cy.dusk(`Audit-${numAudits}`).click({force: true})
        cy.dusk('Audit-Hazards').click()
        // cy.dusk('Object-32').click()
        cy.dusk(`Object-${numObjects}`).click({force: true})
        cy.dusk('New-Comment').type('New Empty Comment')
        cy.dusk('Save-Comment').click()
        cy.get('.swal2-confirm').click()
        cy.contains('New Empty Comment')
        cy.percySnapshot()
      })
  })

  it('Adds Recommendation to Empty AuditObject', () => {
    cy.window()
      .its('cypressVue')
      .then(cypressVue => {
        let numAudits = cypressVue.$store.getters[`database/audits/all`]().length
        let numObjects = cypressVue.$store.getters[`database/auditObjects/all`]().length
        cy.dusk(`Assessment-Audit-3`).click()
        cy.dusk(`Audit-${numAudits}`).click({force: true})
        cy.dusk('Audit-Hazards').click()
        cy.dusk(`Object-${numObjects}`).click({force: true})
        cy.dusk('New-Recommendation').type('New Empty Recommendation')
        cy.dusk('Save-Recommendation').click()
        cy.get('.swal2-confirm').click()
        cy.contains('New Empty Recommendation')
        cy.percySnapshot()
      })
  })

  it('Adds Risk to Empty AuditObject', () => {
    cy.window()
      .its('cypressVue')
      .then(cypressVue => {
        let numAudits = cypressVue.$store.getters[`database/audits/all`]().length
        let numObjects = cypressVue.$store.getters[`database/auditObjects/all`]().length
        cy.dusk(`Assessment-Audit-3`).click()
        cy.dusk(`Audit-${numAudits}`).click({force: true})
        cy.dusk('Audit-Hazards').click()
        cy.dusk(`Object-${numObjects}`).click({force: true})
        cy.dusk('New-Risk').click()
        cy.dusk('input-risk-title').type('Test Empty Risk')
        cy.dusk('radio-likehood-0.5').click()
        cy.dusk('radio-consequence-1').click()
        // cy.dusk('radio-exposure-1').click()
        cy.dusk('Risk-Save').click()
        cy.confirmSwal()
        cy.contains('Test Empty Risk')
        cy.percySnapshot()
      })
  })



})
