context('Auth', () => {

  afterEach(() => {
      cy.screenshot({capture: 'viewport'});
  })

  it('Can Login', () => {
      cy.server()
      cy.route('GET', '/api/v1/**').as('apiCall')
      cy.route('POST', '/api/v1/myLocation').as('locationRequest')
      cy.route('POST', '/api/v1/login').as('loginRequest')
      cy.visit('/')
      cy.location().then((loc) => {
            if (loc.pathname === '/login') {
                cy.get('#username').type('admin')
                cy.get('#password').type('n4n0')
                cy.get('#login-user').click()
                cy.wait('@loginRequest')
                cy.get('.swal2-cancel').click()
                cy.wait(['@apiCall', '@locationRequest'])
            } else {
                cy.log("we are logged in")
                cy.dusk("logout").click()
                cy.confirmSwal()
                cy.get('#username').type('admin')
                cy.get('#password').type('n4n0')
                cy.get('#login-user').click()
                cy.wait('@loginRequest')
                cy.get('.swal2-cancel').click()
                cy.wait(['@apiCall', '@locationRequest'])
            }
      })
      cy.contains('SLSA Beach Management')
  })
})
