context('Offline Audit', () => {
  before(() => {
    cy.clearLocalStorage()
    cy.window().then((win) => {
      win.indexedDB.deleteDatabase("vuex")
    })
  })

  beforeEach(() => {
    cy.server()
    cy.route('GET', '/api/v1/**').as('apiCall')
    cy.route('POST', '/api/v1/myLocation').as('locationRequest')
    cy.route('POST', '/api/v1/login').as('loginRequest')
    cy.visit('/')
    cy.location().then((loc) => {
      if (loc.pathname === '/login') {
        cy.get('#username').type('admin')
        cy.get('#password').type('n4n0')
        cy.get('#login-user').click()
        cy.wait('@loginRequest')
        cy.get('.swal2-cancel').click()
        cy.wait(['@apiCall', '@locationRequest'])
      } else {
        cy.log("we are logged in")
        cy.dusk("logout").click()
        cy.confirmSwal()
        cy.get('#username').type('admin')
        cy.get('#password').type('n4n0')
        cy.get('#login-user').click()
        cy.wait('@loginRequest')
        cy.get('.swal2-cancel').click()
        cy.wait(['@apiCall', '@locationRequest'])
      }
    })

    cy.contains('SLSA Beach Management')
    cy.wait(1000)

    cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
      window.audits = audits
      let audit = audits[0]
      if (audit) {
        cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id).then((auditObjects) => {
          window.auditObjects = auditObjects
        })
        cy.store(`database/comments/all`, comment => comment.audit_id === audit.id).then((comments) => {
          window.comments = comments
        })
        cy.store(`database/recommendations/all`, recommendation => recommendation.audit_id === audit.id).then((recommendations) => {
          window.recommendations = recommendations
        })
        cy.store(`database/risks/all`, risk => risk.audit_id === audit.id).then((risks) => {
          window.risks = risks
        })
      }
        
    })
  })

  afterEach(() => {
    cy.screenshot({capture: 'viewport'});
  })

  it('Create an Audit', () => {
    cy.dusk('Assessment-Audit-1').click()
    cy.dusk('New-Audit').click({force: true})
    cy.dusk('input-title').type('New Audit Test')
    cy.dusk('input-summary').type('Audit Summary')
    cy.dusk('Beach-Selector').select('1')
    cy.dusk('select-hazard-rating').select('1')
    cy.dusk('select-beach-type').select('1')
    cy.dusk('select-fvr-development').select('1')
    cy.dusk('select-fvr-population').select('1')
    cy.dusk('select-fvr-frequency').select('1')
    cy.dusk('select-local-population-rating').select('1')
    cy.dusk('select-human-activity-population-in-water').select('1')
    // cy.dusk('select-human-activity-population-in-water-conflict').select('1')
    cy.dusk('select-human-activity-population-on-beach').select('1')
    // cy.dusk('select-human-activity-population-on-beach-conflict').select('1')
    cy.dusk('Create-Audit').click()
    cy.confirmSwal()
    cy.percySnapshot()
    cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
      let audit = audits[0]
      cy.assertPathIs(`/assessments/1/audit/${audit.id}/type/beach`)
      cy.contains('New Audit Test')
    })
  })

  it('Has Details', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk(`Audit-Beach`).click({force: true})
    cy.contains('Beach')
    cy.dusk(`Audit-Map`).click({force: true})
    cy.percySnapshot()
    // })
  })

  it('Can Edit Details', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits    
    let audit = audits[0]
    cy.log(audit)
    cy.log(audits)
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.wait(2000)
    cy.dusk('Audit-Edit').click({force: true})
    cy.wait(2000)
    cy.dusk('input-title').clear().type('Test Edit Title')
    cy.wait(2000)
    cy.dusk('Save-Audit').click()
    cy.wait(2000)
    cy.confirmSwal()
    cy.wait(5000)
    cy.contains('Test Edit Title')
    cy.percySnapshot()
    // })
  })

  it('Cancel Edit Details Does not Change Details', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.wait(2000)
    cy.dusk('Audit-Edit').click({force: true})
    cy.wait(2000)
    cy.dusk('input-title').clear().type('Cancel Edit Title')
    cy.wait(2000)
    cy.dusk('Cancel-Update').click()
    cy.confirmSwal()
    cy.wait(2000)
    cy.contains('Test Edit Title')
    cy.dusk('Audit-Edit').click({force: true})
    cy.contains('Test Edit Title')
    cy.percySnapshot()
    // })
  })

  it('Can Create an Audit Object', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()
    cy.dusk("New-Audit-Object").click()
    cy.confirmSwal()
    cy.wait(1000)
    cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id).then((auditObjects) => {
      cy.log("logging all auditObjects for this audit:");
      cy.log(JSON.stringify(auditObjects));
    })
    cy.percySnapshot()
    // })
  })

  it('Can Edit Audit Object Details', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

          
    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('AuditObject-Edit').click()
    cy.dusk('input-title').clear().type('Updated AuditObject Title')
    // cy.dusk('select-type').children().then((children) => {
    //     cy.dusk('select-type').select(`${children[2].value}`)
    // })
    cy.dusk('AuditObject-Cancel-Update').click()
    cy.confirmSwal()
    cy.contains(auditObject.title) // test cancel
    cy.dusk('AuditObject-Edit').click()
    cy.dusk('input-title').clear().type('Updated AuditObject Title')
    // cy.dusk('select-type').children().then((children) => {
    //     cy.dusk('select-type').select(`${children[2].value}`)
    // })
    cy.dusk('input-manager').type('Manager')
    cy.dusk('input-provider').type('Provider')
    cy.dusk('AuditObject-Save-Update').click()
    cy.confirmSwal()
    cy.wait(2000)
    cy.contains('Updated AuditObject Title')
    cy.percySnapshot()
    // })
    // })
  })

  it('Can Create a Hazard on Audit Object', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('Create-Hazard').click()
    cy.confirmSwal()
    cy.contains('Hazard Created')
    cy.confirmSwal()
    cy.contains(`${auditObject.title} Accessible Rock Platforms hazard`)
    cy.percySnapshot()
    // })
    // })
  })

  it('Can Remove a Hazard from Audit Object', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id !== 16).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id !== 16);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('Remove-Hazard-0').click()
    cy.confirmSwal()
    cy.contains('Hazard Removed')
    cy.confirmSwal()
    cy.percySnapshot()
    // })
    // })
  })

  it('Can Attach a Hazard to Audit Object', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id !== 16).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id !== 16);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('Add-Hazard').click()
    cy.confirmSwal()
    cy.contains('Hazard')
    cy.confirmSwal()
    cy.percySnapshot()
    // })
    // })
  })

  it('Can Create a Sign on Audit Object', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('Create-Sign').click()
    cy.confirmSwal()
    cy.contains('Sign Created')
    cy.confirmSwal()
    cy.contains(`${auditObject.title} Accessible Rock Platforms hazard`)
    cy.percySnapshot()
    // })
    // })
  })

  it('Can Remove a Sign from Audit Object', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('Remove-Sign-0').click()
    cy.confirmSwal()
    cy.contains('Sign Removed')
    cy.confirmSwal()
    cy.percySnapshot()
    //     })
    // })
  })

  it('Can Attach a Sign to Audit Object', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('Add-Sign').click()
    cy.confirmSwal()
    cy.contains('Sign')
    cy.confirmSwal()
    cy.percySnapshot()
    //     })
    // })
  })

  it('Can Add a Comment', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('New-Comment').type('New Offline Comment')
    cy.dusk('Save-Comment').click()
    cy.wait(5000)
    cy.confirmSwal()
    cy.contains('New Offline Comment')
    cy.percySnapshot()
    //     })
    // })
  })

  it('Can Edit a Comment', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    // cy.store(`database/comments/all`, comment => comment.audit_id === audit.id).then((comments) => {
    let comments = window.comments
    let comment = comments[0]
    cy.dusk(`Edit-Comment-${comment.id}`).click({force: true})
    cy.dusk('Comment-Update-Text').clear().type('Offline Edit Comment')
    cy.dusk('Comment-Cancel-Update').click() // test cancel
    cy.contains('New Offline Comment')
    cy.dusk(`Edit-Comment-${comment.id}`).click({force: true})
    cy.dusk('Comment-Update-Text').clear().type('Offline Edit Comment')
    cy.dusk('Comment-Save-Update').click()
    cy.confirmSwal()
    cy.contains('Offline Edit Comment')
    cy.percySnapshot()
    // })
    //     })
    // })
  })

  it('Delete a Comment', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    // cy.store(`database/comments/all`, comment => comment.audit_id === audit.id).then((comments) => {
    let comments = window.comments
    let comment = comments[0]
    cy.dusk(`Delete-Comment-${comment.id}`).click({force: true})
    cy.confirmSwal()
    cy.wait(1000)
    cy.confirmSwal()
    cy.contains('Offline Edit Comment').should('not.exist')
    cy.percySnapshot()
    //         })
    //     })
    // })
  })

  it('Can Add a Recommendation', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('New-Recommendation').type('New Offline Recommendation')
    cy.dusk('Save-Recommendation').click()
    cy.wait(5000)
    cy.confirmSwal()
    cy.contains('New Offline Recommendation')
    cy.percySnapshot()
    //     })
    // })
  })

  it('Can Edit a Recommendation', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    // cy.store(`database/recommendations/all`, recommendation => recommendation.audit_id === audit.id).then((recommendations) => {
    let recommendations = window.recommendations
    let recommendation = recommendations[0]
    cy.dusk(`Edit-Recommendation-${recommendation.id}`).click({force: true})
    cy.dusk('Recommendation-Update-Text').clear().type('Offline Edit Recommendation')
    cy.dusk('Recommendation-Cancel-Update').click() // test cancel
    cy.contains('New Offline Recommendation')
    cy.dusk(`Edit-Recommendation-${recommendation.id}`).click({force: true})
    cy.dusk('Recommendation-Update-Text').clear().type('Offline Edit Recommendation')
    cy.dusk('Recommendation-Save-Update').click()
    cy.confirmSwal()
    cy.contains('Offline Edit Recommendation')
    cy.percySnapshot()
    //         })
    //     })
    // })
  })

  it('Delete a Recommendation', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk("Audit-Services").click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 19).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 19);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    // cy.store(`database/recommendations/all`, recommendation => recommendation.audit_id === audit.id).then((recommendations) => {
    let recommendations = window.recommendations
    let recommendation = recommendations[0]
    cy.dusk(`Delete-Recommendation-${recommendation.id}`).click({force: true})
    cy.confirmSwal()
    cy.wait(1000)
    cy.confirmSwal()
    cy.contains('Offline Edit Recommendation').should('not.exist')
    cy.percySnapshot()
    //         })
    //     })
    // })
  })

  it('Create a Risk', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk('Audit-Hazards').click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 16).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 16);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    cy.dusk('New-Risk').click()
    cy.dusk('input-risk-title').type('Test Risk Title')
    cy.dusk('radio-likehood-0.5').click()
    cy.dusk('radio-consequence-1').click()
    // cy.dusk('radio-exposure-1').click()
    cy.dusk('Risk-Save').click()
    cy.confirmSwal()
    cy.wait(1000)
    cy.contains('Test Risk Title')
    cy.percySnapshot()
    //     })
    // })
  })

  it('Edits a Risk', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk('Audit-Hazards').click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 16).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 16);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    // cy.store(`database/risks/all`, risk => risk.audit_id === audit.id).then((risks) => {
    let risks = window.risks
    let risk = risks[0]
    cy.wait(2000)
    cy.dusk(`Edit-Risk-${risk.id}`).click()
    cy.wait(2000)
    cy.dusk('input-risk-title').clear().type('Edit Risk Title')
    cy.dusk('radio-likehood-3').click()
    cy.dusk('radio-consequence-5').click()
    // cy.dusk('radio-exposure-5').click()
    cy.wait(2000)
    cy.dusk('Risk-Save').click()
    cy.wait(2000)
    cy.confirmSwal()
    cy.wait(2000)
    cy.contains('Edit Risk Title')
    cy.percySnapshot()
    //         })
    //     })
    // })
  })

  it('Deletes a Risk', () => {
    // cy.store(`database/audits/all`, audit => audit.status === 0).then((audits) => {
    let audits = window.audits
    let audit = audits[0]
    cy.dusk(`Assessment-Audit-${audit.assessment_id}`).click()
    cy.dusk(`Audit-${audit.id}`).click({force: true})
    cy.dusk('Audit-Hazards').click()

    // cy.store(`database/auditObjects/all`, object => object.audit_id === audit.id && object.type_id === 16).then((auditObjects) => {
    let auditObjects = window.auditObjects.filter(object => object.type_id === 16);
    let auditObject = auditObjects[0]
    cy.dusk(`Object-${auditObject.id}`).click({force: true})
    // cy.store(`database/risks/all`, risk => risk.audit_id === audit.id).then((risks) => {
    let risks = window.risks
    let risk = risks[0]
    cy.dusk(`Delete-Risk-${risk.id}`).click()
    cy.confirmSwal()
    cy.wait(1000)
    cy.confirmSwal()
    cy.contains('Edit Risk Title').should('not.exist')
    cy.percySnapshot()
    //         })
    //     })
    // })
  })




  after(() => {
    cy.clearLocalStorage()
    cy.window().then((win) => {
      win.indexedDB.deleteDatabase("vuex")
    })
  })
})
