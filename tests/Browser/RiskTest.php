<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Audit;
use App\Risk;

class RiskTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
    */
    public function testCreateRisk()
    {
        $audit = Audit::first();
        $firstObject = Audit::first()->objects()->get()->random();
        $this->browse(function (Browser $browser) use ($audit, $firstObject) {
            $this->browserLoginAsUser($browser)
                    ->pause(15000) // wait for audit objects to load
                    ->waitFor('@Audit-1')
                    ->press('@Audit-1')
                    ->waitForText('Dashboard')
                    ->waitFor($this->mapTypeToSelector($firstObject->type_id))
                    ->pause(1000)
                    ->press($this->mapTypeToSelector($firstObject->type_id))
                    ->waitFor('@Object-'.$firstObject->id, 30)
                    ->press('@Object-'.$firstObject->id)
                    ->waitFor('@New-Risk')
                    ->press('@New-Risk')
                    ->waitFor('@input-risk-title')
                    ->type('@input-risk-title', 'Test Risk Title')
                    ->press('@radio-likehood-0.5')
                    ->press('@radio-consequence-1')
                    // ->press('@radio-exposure-1')
                    ->press('Create')
                    ->waitForText('3D Risk created', 20)
                    ->waitForText('OK')
                    ->pause(100)
                    ->press('OK')
                    ->assertSee('Test Risk Title')
                    ->screenshot('Risk Created');
        });
    }

    public function testEditRisk()
    {
        $risk = Risk::get()->random();
        $audit = $risk->audit;
        $firstObject = $risk->auditObject;
        $this->browse(function (Browser $browser) use ($audit, $firstObject, $risk) {
            $browser->visit('/')
                    ->waitFor('@Audit-'.$audit->id)
                    ->press('@Audit-'.$audit->id)
                    ->waitForText('Dashboard')
                    ->waitFor($this->mapTypeToSelector($firstObject->type_id))
                    ->pause(1000)
                    ->press($this->mapTypeToSelector($firstObject->type_id))
                    ->waitFor('@Object-'.$firstObject->id, 60)
                    ->press('@Object-'.$firstObject->id)
                    ->waitFor('@Edit-Risk-'.$risk->id)
                    ->press('@Edit-Risk-'.$risk->id)
                    ->waitFor('@input-risk-title')
                    ->type('@input-risk-title', 'Edit Risk Title')
                    ->press('@radio-likehood-1')
                    ->press('@radio-consequence-5')
                    // ->press('@radio-exposure-10')
                    ->press('Update')
                    ->waitForText('3D Risk updated', 20)
                    ->pause(100)
                    ->press('OK')
                    ->assertSee('Edit Risk Title')
                    ->screenshot('Risk Edited');
        });
    }

    // public function testDeleteRisk()
    // {
    //     $audit = Audit::first();
    //     $firstObject = Audit::first()->objects()->get()->random();
    //     $this->browse(function (Browser $browser) use ($audit, $firstObject) {
    //         $browser->visit('/')
    //                 ->pause(15000) // wait for audit objects to load
    //                 ->waitFor('@Audit-1')
    //                 ->press('@Audit-1')
    //                 ->waitForText('Dashboard')
    //                 ->waitUntilVueIsNot('audit.objects', [], '@Audit-Header', 20)
    //                 ->waitFor($this->mapTypeToSelector($firstObject->type_id))
    //                 ->press($this->mapTypeToSelector($firstObject->type_id))
    //                 ->waitFor('@Object-'.$firstObject->id)
    //                 ->press('@Object-'.$firstObject->id)
    //                 ->waitFor('@New-Risk')
    //                 ->press('@New-Risk')
    //                 ->waitFor('@input-risk-title')
    //                 ->type('@input-risk-title', 'Test Risk Title')
    //                 ->press('@radio-likehood-0.5')
    //                 ->press('@radio-consequence-1')
    //                 ->press('@radio-exposure-1')
    //                 ->press('Create')
    //                 ->pause(5000)
    //                 ->waitForText('3D Risk created')
    //                 ->press('OK')
    //                 ->screenshot('Risk Created');
    //     });
    // }

    public function mapTypeToSelector($type_id)
    {
        switch ($type_id) {
            case 6:
                return "@Audit-Access";
            case 1:
                return "@Audit-Activities";
            case 19:
                return "@Audit-Services";
            case 13:
                return "@Audit-Support";
            case 16:
                return "@Audit-Hazards";
            case 12:
                return "@Audit-Equipment";
            case 14:
                return "@Audit-Facilities";
            case 28:
                return "@Audit-Signages";
        }
    }
}
