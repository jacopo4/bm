<?php

namespace Tests\Browser;

use App\User;
use Carbon\Carbon;
use App\Assessment;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AssesementTest extends DuskTestCase
{

    public function testCreateNewAssesment()
    {
        $this->browse(function (Browser $browser) {
            $this->browserLoginAsUser($browser)
                    ->waitForText('Assessments')
                    ->press('@New-Assessment')
                    ->waitFor('@input-title')
                    ->assertPathIs('/assessments/new')
                    ->waitFor('@input-title')
                    ->type('@input-title', 'New Assessment Title')
                    ->type('@input-state/area-name', 'Assessment State')
                    ->typeDate('@input-start-date', Carbon::now())
                    ->typeDate('@input-end-date', Carbon::now())
                    ->screenshot('Filled Assessment')
                    ->press('@Create-Assessment')
                    ->waitForText('Good Job!', 30)
                    ->press('OK')
                    ->waitForText('New Assessment Title')
                    ->screenshot('Create Assessment');
        });
    }

    public function testCannotCreateWithInvalidFields()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->waitForText('Assessments')
                    ->press('@New-Assessment')
                    ->waitForText('New Assessment')
                    ->assertPathIs('/assessments/new')
                    ->waitFor('@input-state/area-name')
                    ->type('@input-state/area-name', 'Assessment State')
                    ->typeDate('@input-start-date', Carbon::now())
                    ->typeDate('@input-end-date', Carbon::now())
                    ->press('@Create-Assessment')
                    ->waitForText('Please fill in all')
                    ->assertPathIs('/assessments/new')
                    ->screenshot('Invalid Assessment');
        });
    }

    public function testAddMembers()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->waitFor('@Assessment-Status-1')
                    ->press('@Assessment-Status-1')
                    ->assertPathIs('/assessments/1/status')
                    ->waitForText('Members')
                    ->press('@Invite-Member')
                    ->type('#name', User::find(3)->first_name)
                    ->waitUntilVue('running', 'true', '@AddMemberForm')
                    ->waitFor('@Autocomplete-0')
                    ->screenshot('Autocomplete Member')
                    ->click('@Autocomplete-0')
                    ->waitUntil('!window.axiosRunning', 10)
                    ->select('@Member-Role')
                    ->screenshot('Member Form filled')
                    ->press('@add-member')
                    ->waitUntil('!window.axiosRunning', 10)
                    ->press('@close-member')
                    ->waitForText(User::find(3)->first_name)
                    ->screenshot('Added Member');
        });
    }

    public function testAssessmentDetails()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->waitFor('@Assessment-Status-1')
                    ->press('@Assessment-Status-1')
                    ->assertPathIs('/assessments/1/status')
                    ->waitForText('Details')
                    ->assertSee(Assessment::first()->title)
                    ->screenshot('Assessment Status')
                    ->visit('/')
                    ->waitFor('@Assessment-Status-1')
                    ->press('@Assessment-Audit-1')
                    ->assertPathIs('/assessments/1/audit')
                    ->waitForText('Audits')
                    ->screenshot('Assessment Audits')
                    ->visit('/')
                    ->waitFor('@Assessment-Status-1')
                    ->press('@Assessment-Locations-1')
                    ->assertPathIs('/assessments/1/locations')
                    ->waitForText('Locations')
                    ->screenshot('Assessment Locations')
                    ->visit('/')
                    ->waitFor('@Assessment-Status-1')
                    ->press('@Assessment-Comments-1')
                    ->assertPathIs('/assessments/1/comments')
                    ->waitForText('Comments')
                    ->screenshot('Assessment Comments');
        });
    }

    public function testEditAssessment()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->waitFor('@Assessment-Status-1')
                    ->press('@Assessment-Status-1')
                    ->assertPathIs('/assessments/1/status')
                    ->waitForText('Details')
                    ->assertSee(Assessment::first()->title)
                    ->press('@Assessment-Edit')
                    ->waitForText('Edit Assessment')
                    ->type('@input-title', 'New Test Title')
                    ->press('Save')
                    ->waitForText('Assessment updated', 10)
                    ->waitForText('OK')
                    ->pause(100)
                    ->press('OK')
                    ->screenshot('Assessment Edit');
        });
    }

    //
    // public function testAddLocations()
    // {
    //     $this->browse(function (Browser $browser) {
    //         $browser->visit('/')
    //                 ->assertSee('Log in')
    //                 ->screenshot('test');
    //     });
    // }
    //
    // public function testComments()
    // {
    //     $this->browse(function (Browser $browser) {
    //         $browser->visit('/')
    //                 ->assertSee('Log in')
    //                 ->screenshot('test');
    //     });
    // }
}
