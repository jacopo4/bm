<?php

namespace Tests\Browser;

use App\Audit;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuditTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testNewAudit()
    {

        

        $params =  [
            "latitude" =>  47.1111,
            "longitude" =>  -122.1111,
            "accuracy" =>  100
        ];
          
        //$response = $this->driver()->execute_cdp_cmd("Page.setGeolocationOverride", $params);
        
      
        $this->browse(function (Browser $browser) {
            $this->browserLoginAsUser($browser)
                    ->script('document.querySelector("body > div.w-100.main-page.page").__vue__.$store.dispatch("location/fetchLocationDetails");'); // get beaches
            $browser->waitFor('@Assessment-Status-1', 10)
                    ->waitFor('@New-Audit')
                    ->press('@New-Audit')
                    ->waitForText('Select Assessment')
                    ->press('Next')
                    ->waitFor('@input-title')

                    ->type('@input-title', 'New Audit Test')
                    ->type('@input-summary', 'Audit Summary')
                    ->waitUntilVueIsNot('options', [], '@Beach-Selector', 30)
                    ->press('@radio-mode-1')
                    ->select('@select-')
                    ->select('@select-hazard-rating')
                    ->select('@select-beach-type')
                    ->select('@select-fvr-development')
                    ->select('@select-fvr-population')
                    ->select('@select-fvr-frequency')
                    ->select('@select-local-population-rating')
                    ->select('@select-human-activity-population-in-water')
                    ->select('@select-human-activity-population-in-water-conflict')
                    ->select('@select-human-activity-population-on-beach')
                ->select('@select-human-activity-population-on-beach-conflict')
                    ->screenshot('Filled Audit')
                    ->press('@Create-Audit')
                    ->waitForText('Good Job!', 60)
                    ->pause(1000)
                    ->press('OK')
                    ->screenshot('Audit Created');
        });
    }

    public function testSeeAuditDetails()
    {
        $audit = Audit::first();
        $this->browse(function (Browser $browser) use ($audit) {
            $browser->visit('/')
                    ->waitFor('@Audit-Beach-'.$audit->id)
                    ->press('@Audit-Beach-'.$audit->id)
                    ->assertSee($audit->beach->title)
                    ->screenshot('Audit Beach Details')
                    ->press('@Audit-Map-'.$audit->id)
                    ->screenshot('Audit Map Details');
        });
    }

    public function testEditAuditDetails()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->waitFor('@Audit-2')
                    ->press('@Audit-2')
                    ->waitFor('@Audit-Edit')
                    ->press('@Audit-Edit')
                    ->waitFor('@input-title')
                    ->screenshot('Audit Edit Modal')
                    ->type('@input-title', 'Test Edit Title')
                    ->press('Save')
                    ->waitForText('Audit updated')
                    ->press('OK')
                    ->assertSee('Test Edit Title')
                    ->screenshot('Audit Edit Details');
        });
    }

    public function testShowAuditObject()
    {
        $audit = Audit::first();
        $objects = Audit::first()->objects()->get()->random(2);
        $firstObject = $objects[0];
        $secondObject = $objects[1];
        $this->browse(function (Browser $browser) use ($audit, $firstObject, $secondObject) {
            $browser->visit('/')
                    ->waitFor('@Audit-1')
                    ->press('@Audit-1')
                    ->waitForText('Dashboard')
                    ->waitFor($this->mapTypeToSelector($firstObject->type_id))
                    ->pause(1000)
                    ->press($this->mapTypeToSelector($firstObject->type_id))
                    ->waitFor('@Object-'.$firstObject->id, 90)
                    ->press('@Object-'.$firstObject->id)
                    ->assertPathIs('/assessments/'.$audit->assessment->id.'/audit/'.$audit->id.'/object/'.$firstObject->id)
                    ->assertSee($firstObject->title)
                    ->screenshot('Show AuditObject')
                    ->press($this->mapTypeToSelector($secondObject->type_id))
                    ->press('@Object-'.$secondObject->id)
                    ->assertPathIs('/assessments/'.$audit->assessment->id.'/audit/'.$audit->id.'/object/'.$secondObject->id)
                    ->assertSee($secondObject->title);
        });
    }

    public function mapTypeToSelector($type_id)
    {
        switch ($type_id) {
            case 6:
                return "@Audit-Access";
            case 1:
                return "@Audit-Activities";
            case 19:
                return "@Audit-Services";
            case 13:
                return "@Audit-Support";
            case 16:
                return "@Audit-Hazards";
            case 12:
                return "@Audit-Equipment";
            case 14:
                return "@Audit-Facilities";
            case 28:
                return "@Audit-Signages";
        }
    }
}
