<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('Log in')
                    ->screenshot('LoginPage')
                    ->pause(1000)
                    ->type('username', 'admin')
                    ->type('password', 'n4n0')
                    ->press('Log in')
                    ->waitForText('Do you have GPS?')
                    ->press('Cancel')
                    ->screenshot('LoggedIn');
        });
    }
}
