<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Audit;
use App\AuditObject;
use App\Comment;

class AuditObjectTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testAddComment()
    {
        $audit = Audit::first();
        $firstObject = Audit::first()->objects()->get()->random();
        $this->browse(function (Browser $browser) use ($audit, $firstObject) {
            $this->browserLoginAsUser($browser)
                    ->waitFor('@Audit-1')
                    ->press('@Audit-1')
                    ->waitForText('Dashboard')
                    ->waitFor($this->mapTypeToSelector($firstObject->type_id))
                    ->pause(1000)
                    ->press($this->mapTypeToSelector($firstObject->type_id))
                    ->waitFor('@Object-'.$firstObject->id, 90)
                    ->press('@Object-'.$firstObject->id)
                    ->assertPathIs('/assessments/'.$audit->assessment->id.'/audit/'.$audit->id.'/object/'.$firstObject->id)
                    ->assertSee($firstObject->title)
                    ->screenshot('Show AuditObject')
                    ->waitFor('@Save-Comment')
                    ->type('@New-Comment', 'New Test Comment')
                    ->press('@Save-Comment')
                    ->waitForText('Comment Saved', 15)
                    ->press('OK')
                    ->assertSee('New Test Comment')
                    ->screenshot('Comment Created');
        });
    }

    public function testEditComment()
    {
        $audit = Audit::first();
        $comment = Comment::where('audit_id', 1)->get()->random();
        $auditObject = $comment->auditObject;
        $this->browse(function (Browser $browser) use ($audit, $auditObject, $comment) {
            $browser->visit('/')
                    ->waitFor('@Audit-1')
                    ->press('@Audit-1')
                    ->waitForText('Dashboard')
                    ->waitFor($this->mapTypeToSelector($auditObject->type_id))
                    ->pause(1000)
                    ->press($this->mapTypeToSelector($auditObject->type_id))
                    ->waitFor('@Object-'.$auditObject->id, 90)
                    ->press('@Object-'.$auditObject->id)
                    ->waitFor('@Edit-Comment-'.$comment->id)
                    ->press('@Edit-Comment-'.$comment->id)
                    ->type('@Comment-Update-Text', 'New Test Comment')
                    ->press('@Comment-Cancel-Update') // test cancel
                    ->assertSee($comment->text)
                    ->press('@Edit-Comment-'.$comment->id)
                    ->type('@Comment-Update-Text', 'New Test Comment')
                    ->press('@Comment-Save-Update')
                    ->waitForText('Comment updated', 10)
                    ->press('OK')
                    ->assertSee('New Test Comment')
                    ->screenshot('AuditObject Update Comment');
        });
    }

    public function testDeleteComment()
    {
        $audit = Audit::first();
        $comment = Comment::where('audit_id', 1)->get()->random();
        $auditObject = $comment->auditObject;
        $this->browse(function (Browser $browser) use ($audit, $auditObject, $comment) {
            $browser->visit('/')
                    ->waitFor('@Audit-1')
                    ->press('@Audit-1')
                    ->waitForText('Dashboard')
                    ->waitFor($this->mapTypeToSelector($auditObject->type_id))
                    ->pause(1000)
                    ->press($this->mapTypeToSelector($auditObject->type_id))
                    ->waitFor('@Object-'.$auditObject->id, 90)
                    ->press('@Object-'.$auditObject->id)
                    ->assertPathIs('/assessments/'.$audit->assessment->id.'/audit/'.$audit->id.'/object/'.$auditObject->id)
                    ->waitFor('@Delete-Comment-'.$comment->id)
                    ->press('@Delete-Comment-'.$comment->id)
                    ->press('Yes!')
                    ->waitForText('Comment Deleted')
                    ->press('OK')
                    ->screenshot('AuditObject Comment Delete');
        });
    }

    public function testCreateAuditObject()
    {
        $audit = Audit::first();
        $this->browse(function (Browser $browser) use ($audit) {
            $browser->visit('/')
                    ->waitFor('@Audit-1')
                    ->press('@Audit-1')
                    ->waitForText('Dashboard')
                    ->waitFor('@Audit-Activities')
                    ->pause(1000)
                    ->press('@Audit-Activities')
                    ->waitFor('@New-Audit-Object')
                    ->press('@New-Audit-Object')
                    ->press('Next')
                    ->type('@New-Audit-Title', 'Test Title')
                    ->press('Next')
                    ->screenshot('Create Audit Test')
                    ->waitForText('Good Job!')
                    ->press('OK')
                    ->assertPathIs('/assessments/'.$audit->assessment->id.'/audit/'.$audit->id.'/object/'.AuditObject::count())
                    ->assertSee('Test Title')
                    ->screenshot('Create Audit Object');
        });
    }

    public function mapTypeToSelector($type_id)
    {
        switch ($type_id) {
            case 6:
                return "@Audit-Access";
            case 1:
                return "@Audit-Activities";
            case 19:
                return "@Audit-Services";
            case 13:
                return "@Audit-Support";
            case 16:
                return "@Audit-Hazards";
            case 12:
                return "@Audit-Equipment";
            case 14:
                return "@Audit-Facilities";
            case 28:
                return "@Audit-Signages";
        }
    }
}
