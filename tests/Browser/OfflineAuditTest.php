<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Audit;

class OfflineAuditTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testNewAudit()
    {
        $this->browse(function (Browser $browser) {
            $this->browserLoginAsUser($browser)
                    ->visit('/')
                    ->script('document.querySelector("body > div.w-100.main-page.page").__vue__.$store.dispatch("location/fetchLocationDetails");'); // get beaches
            $browser->waitFor('@Assessment-Status-1', 30)
                    ->waitFor('@New-Audit')
                    ->press('@New-Audit')
                    ->waitForText('Select Assessment')
                    ->press('Next')
                    ->waitForText('Title')
                    ->type('@input-title', 'New Audit Test')
                    ->type('@input-summary', 'Audit Summary')
                    ->waitUntilVueIsNot('options', [], '@Beach-Selector', 60)
                    ->select('@select-')
                    ->select('@select-hazard-rating')
                    ->select('@select-beach-type')
                    ->select('@select-fvr-development')
                    ->select('@select-fvr-population')
                    ->select('@select-fvr-frequency')
                    ->select('@select-local-population-rating')
                    ->select('@select-human-activity-population-in-water')
                    ->select('@select-human-activity-population-in-water-conflict')
                    ->select('@select-human-activity-population-on-beach')
                    ->select('@select-human-activity-population-on-beach-conflict')
                    ->screenshot('Filled Offline Audit')
                    ->press('@Create-Audit')
                    ->waitForText('Good Job!')
                    ->press('OK')
                    ->screenshot('Audit Created');
        });
    }

    public function testSeeAuditDetails()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertPathIs('/')
                    ->waitFor('@Audit-Beach-$uid1')
                    ->press('@Audit-Beach-$uid1')
                    ->screenshot('Offline Audit Beach Details')
                    ->press('@Audit-Map-$uid1')
                    ->screenshot('Offline Audit Map Details');
        });
    }

    public function testEditAuditDetails()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->waitFor('@Audit-$uid1')
                    ->press('@Audit-$uid1')
                    ->waitFor('@Audit-Edit')
                    ->press('@Audit-Edit')
                    ->waitFor('@input-title')
                    ->screenshot('Audit Edit Modal')
                    ->type('@input-title', 'Test Edit Title')
                    ->press('Save')
                    ->waitForText('Audit updated')
                    ->press('OK')
                    ->assertSee('Test Edit Title')
                    ->screenshot('Audit Edit Details');
        });
    }

    public function testMakeAuditObject()
    {
        $audit = Audit::first();
        $objects = Audit::first()->objects()->get()->random(2);
        $firstObject = $objects[0];
        $secondObject = $objects[1];
        $this->browse(function (Browser $browser) use ($audit, $firstObject, $secondObject) {
            $browser->visit('/')
                    ->waitFor('@Audit-$uid1')
                    ->press('@Audit-$uid1')
                    ->waitForText('Dashboard')
                    ->waitFor("@Audit-Services")
                    ->pause(1000)
                    ->press("@Audit-Services")
                    ->waitUntilVueIsNot('subtypes', [], '@Audit-Header', 120)
                    ->press("@New-Audit-Object")
                    ->press('Next')
                    ->type('@New-Audit-Title', 'Test Title')
                    ->press('Next')
                    ->waitForText('Good Job!')
                    ->press('OK')
                    ->assertPathIs('/assessments/1/audit/$uid1/object/$uid1')
                    ->screenshot('Offline Audit Object');
        });
    }

    public function testAddCommentOffline()
    {
        $audit = Audit::first();
        $objects = Audit::first()->objects()->get()->random(2);
        $firstObject = $objects[0];
        $secondObject = $objects[1];
        $this->browse(function (Browser $browser) use ($audit, $firstObject, $secondObject) {
            $browser->visit('/')
                    ->waitFor('@Audit-$uid1')
                    ->press('@Audit-$uid1')
                    ->waitForText('Dashboard')
                    ->waitFor("@Audit-Services")
                    ->pause(1000)
                    ->press("@Audit-Services")
                    ->waitFor('@Object-$uid1', 60)
                    ->press('@Object-$uid1')
                    ->assertPathIs('/assessments/1/audit/$uid1/object/$uid1')
                    ->assertSee('Test Title')
                    ->waitFor('@Save-Comment')
                    ->type('@New-Comment', 'New Test Comment')
                    ->press('@Save-Comment')
                    ->waitForText('Comment Saved', 15)
                    ->press('OK')
                    ->assertSee('New Test Comment')
                    ->screenshot('Offline Comment Created');
        });
    }

    public function testEditCommentOffline()
    {
        $audit = Audit::first();
        $objects = Audit::first()->objects()->get()->random(2);
        $firstObject = $objects[0];
        $secondObject = $objects[1];
        $this->browse(function (Browser $browser) use ($audit, $firstObject, $secondObject) {
            $browser->visit('/')
                    ->waitFor('@Audit-$uid1')
                    ->press('@Audit-$uid1')
                    ->waitForText('Dashboard')
                    ->waitFor("@Audit-Services")
                    ->pause(1000)
                    ->press("@Audit-Services")
                    ->waitFor('@Object-$uid1', 60)
                    ->press('@Object-$uid1')
                    ->waitFor('@Edit-Comment-$uid1')
                    ->press('@Edit-Comment-$uid1')
                    ->type('@Comment-Update-Text', 'Offline Comment')
                    ->press('@Comment-Cancel-Update') // test cancel
                    ->assertSee('New Test Comment')
                    ->press('@Edit-Comment-$uid1')
                    ->type('@Comment-Update-Text', 'Offline Comment')
                    ->press('@Comment-Save-Update')
                    ->waitForText('Comment updated', 10)
                    ->press('OK')
                    ->assertSee('Offline Comment')
                    ->screenshot('Offline Comment Update');
        });
    }

    public function testDeleteCommentOffline()
    {
        $audit = Audit::first();
        $objects = Audit::first()->objects()->get()->random(2);
        $firstObject = $objects[0];
        $secondObject = $objects[1];
        $this->browse(function (Browser $browser) use ($audit, $firstObject, $secondObject) {
            $browser->visit('/')
                    ->waitFor('@Audit-$uid1')
                    ->press('@Audit-$uid1')
                    ->waitForText('Dashboard')
                    ->waitFor("@Audit-Services")
                    ->pause(1000)
                    ->press("@Audit-Services")
                    ->waitFor('@Object-$uid1', 60)
                    ->press('@Object-$uid1')
                    ->waitFor('@Delete-Comment-$uid1')
                    ->press('@Delete-Comment-$uid1')
                    ->press('Yes!')
                    ->waitForText('Comment Deleted')
                    ->press('OK')
                    ->assertDontSee('Offline Comment')
                    ->screenshot('Offline Comment Delete');
        });
    }

    public function testCreateRisk()
    {
        $audit = Audit::first();
        $firstObject = Audit::first()->objects()->get()->random();
        $this->browse(function (Browser $browser) use ($audit, $firstObject) {
            $browser->visit('/')
                    ->waitFor('@Audit-$uid1')
                    ->press('@Audit-$uid1')
                    ->waitForText('Dashboard')
                    ->waitFor("@Audit-Services")
                    ->pause(1000)
                    ->press("@Audit-Services")
                    ->waitFor('@Object-$uid1', 60)
                    ->press('@Object-$uid1')
                    ->waitFor('@New-Risk')
                    ->press('@New-Risk')
                    ->waitFor('@input-risk-title')
                    ->type('@input-risk-title', 'Test Risk Title')
                    ->press('@radio-likehood-0.5')
                    ->press('@radio-consequence-1')
                    // ->press('@radio-exposure-1')
                    ->press('Create')
                    ->waitForText('3D Risk created', 20)
                    ->waitForText('OK')
                    ->pause(100)
                    ->press('OK')
                    ->assertSee('Test Risk Title')
                    ->screenshot('Offline Risk Created');
        });
    }

    public function mapTypeToSelector($type_id)
    {
        switch ($type_id) {
            case 6:
                return "@Audit-Access";
            case 1:
                return "@Audit-Activities";
            case 19:
                return "@Audit-Services";
            case 13:
                return "@Audit-Support";
            case 16:
                return "@Audit-Hazards";
            case 12:
                return "@Audit-Equipment";
            case 14:
                return "@Audit-Facilities";
            case 28:
                return "@Audit-Signages";
        }
    }
}
