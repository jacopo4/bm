<?php

namespace Tests\Feature;

use App\User;
use App\Beach;
use Tests\TestCase;

class APITest extends TestCase
{
    public $user;

    /**
     * A basic test example.
     *
     * @return void
     */

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::first();
    }


    //
    public function testCanSeeAppApi()
    {

        $response = $this->actingAs($this->user, 'api')
                         ->json(
                             'GET',
                             route('api.v1.app')
                         );


        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                'version',
                'env',
                'name',
                'css',
                'js',
                'builded_at'
                ]
            );
    }



    public function testCanSeeAssessments()
    {

        $response = $this->actingAs($this->user, 'api')
                     ->json(
                         'GET',
                         route('assessments.api.index')
                     );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => "ok",
            'assessment' => []
        ]);
    }

    public function testCanSeeAudits()
    {

        $response = $this->actingAs($this->user, 'api')
                     ->json(
                         'GET',
                         route('audits.api.index')
                     );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => "ok",
            'audit' => []
        ]);
    }

    public function testCanSeeAuditsObjects()
    {

        $response = $this->actingAs($this->user, 'api')
                     ->json(
                         'GET',
                         route('auditObject.api.index')
                     );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => "ok",
            'audit_object' => []
        ]);
    }

    public function testCanSeeAuditsObjectsTypes()
    {

        $response = $this->actingAs($this->user, 'api')
                     ->json(
                         'GET',
                         route('audits.api.fetchTypes')
                     );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => "ok",
            'subtypes' => []
        ]);
    }

    public function testCanSeeMedia()
    {

        $response = $this->actingAs($this->user, 'api')
                     ->json(
                         'GET',
                         route('media.api.index')
                     );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => "ok",
            'media' => []
        ]);
    }

    public function testCanSeeComments()
    {

        $response = $this->actingAs($this->user, 'api')
                     ->json(
                         'GET',
                         route('comments.api.index')
                     );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => "ok",
            'comment' => []
        ]);
    }


    public function testCanSeeRisks()
    {

        $response = $this->actingAs($this->user, 'api')
                     ->json(
                         'GET',
                         route('risks.api.index')
                     );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => "ok",
            'risk' => []
        ]);
    }

    // public function testCanSeeBeachs()
    // {
    //
    //     $response = $this->actingAs($this->user, 'api')
    //                  ->json(
    //                      'GET',
    //                      route('beaches.api.index')
    //                  );
    //
    //     $response
    //     ->assertStatus(200)
    //     ->assertJson([
    //         'status' => "ok",
    //         'beaches' => []
    //     ]);
    // }

    public function testCanSeeBeachDistance()
    {

        $beach = Beach::first();
        $response = $this->actingAs($this->user, 'api')
                     ->json(
                         'GET',
                         route('beaches.api.fetch', $beach)
                     );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => "ok",
            'beach' => []
        ]);
    }

    // do we need this?
    public function testCanSeeEntities()
    {

        $response = $this->actingAs($this->user, 'api')
                     ->json(
                         'GET',
                         route('entities.api.index')
                     );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => "ok",
            'entity' => []
        ]);
    }

    // public function testCanSearchAssementUsers()
    // {

    //     $response = $this->actingAs($this->user, 'api')
    //                  ->json(
    //                      'GET',
    //                      route('assessments.api.search.users')
    //                  );

    //     $response
    //     ->assertStatus(200)
    //     ->assertJson([
    //         'status' => "ok",
    //         'entities' => []
    //     ]);
    // }
}
