<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BasicTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCanSeeHomet()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testWeHaveStaticMix()
    {

        $response = $this->get('/');

        $response
            ->assertStatus(200)
            ->assertSee('http://localhost:8081/manifest.json');
    }
}
