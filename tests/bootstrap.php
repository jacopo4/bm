<?php
require_once __DIR__ . '/../vendor/autoload.php';

if (isset($_SERVER['FAST'])) {
    echo "Fast mode no running migrations\n";
} else {
    echo "running migrations\n";
    exec("php artisan migrate:fresh --env=testing");
    echo "running seed\n";
    exec("php artisan db:seed --env=testing --class=TestingSeeder");
}
