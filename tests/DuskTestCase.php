<?php

namespace Tests;

use App\User;
use Carbon\Carbon;
use Laravel\Dusk\Browser;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Artisan;
use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Symfony\Component\Process\Exception\ProcessFailedException;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {

        if (PHP_OS == 'Darwin') {
            // we are on mac
            static::startChromeDriver();
        } else {
            // linux
            // we have one running in Docker
        }
    }

    // protected function newBrowser($driver)
    // {
    //     return new \Nano\Docs\NanoDocsBrowser($driver);
    // }

    /**
     * Default preparation for each test
     */
    protected function setUp(): void
    {
        parent::setUp(); // Don't forget this!

        Browser::macro('typeDate', function ($selector, $date) {
            $this->keys($selector, '{backspace}')
                ->keys($selector, $date->format('d'))
                ->keys($selector, '{backspace}')
                ->keys($selector, $date->format('m'))
                ->keys($selector, '{backspace}')
                ->keys($selector, $date->format('Y'))
                ->value($selector, $date->format('Y-m-d'));

                return $this;
        });


        // Artisan::call('migrate');
        // if (User::count() == 0) {
        //     Artisan::call('db:seed');
        // }
        // Artisan::call('db:seed', [
        //     '--class' => 'PatrolsTableSeeder'
        // ]);

//         latitude: -32.0544852
// longitude: 115.7456736
        # exec('rm -f ' . storage_path('framework/sessions/*'));
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {

        $resolution = env('RESOLUTION', '1024,1366');

        if (env('HEADLESS') === "0") {
            $options = (new ChromeOptions)->addArguments([
                '--window-size='.$resolution
            ]);
        } else {
            $options = (new ChromeOptions)->addArguments([
                '--disable-gpu',
                '--no-sandbox',
                '--headless',
                '--window-size='.$resolution
            ]);
        }

        //static::startBrowserStackLocal();
//        return RemoteWebDriver::create(
//            'https://' . env('BROWSERSTACK_USERNAME') . ':' . env('BROWSERSTACK_ACCESS_KEY') . '@' . env('BROWSERSTACK_SERVER') . '/wd/hub',
//            $this->browserStackCaps(),
//            90 * 1000,
//            90 * 1000
//        );


        return RemoteWebDriver::create(
            'http://localhost:9515',
            DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY,
                $options
            )
        );
    }

    /**
     * Setup the BrowserStack capabilities
     *
     * @see https://www.browserstack.com/automate/capabilities
     * @return array
     */
    private function browserStackCaps()
    {
        $caps = [
            'project'              => env('APP_NAME'),
            'build'                => env('APP_VERSION'),
            "device"               => env('DEVICE'),
            //"device" => "iPhone X",
            "realMobile"           => true,
            //"deviceOrientation" => "landscape",
            'browserstack.local'   => true,
            "browserstack.debug"   => true,
            "browserstack.console" => "info",
            "unicodeKeyboard"      => true,
            "resetKeyboard"        => true
        ];

        return $caps;
    }

    public static function runServer()
    {

        $env = 'testing';

        $webserver = new Process(array(PHP_BINARY, 'artisan', 'serve', '--port=8081', '--env=' . $env));
        $webserver->disableOutput();
        $webserver->start();
        // executes after the command finishes
//        if (! $webserver->isSuccessful()) {
//            echo $webserver->getOutput();
//            throw new ProcessFailedException($webserver);
//        } else {
//            //echo "all good";
//        }
        if (PHP_OS == 'Darwin') {
            sleep(2);
        } else {
            sleep(4);
        }
    }


    public static function setUpBeforeClass(): void
    {
        self::runServer();
    }

    public static function tearDownAfterClass(): void
    {
        //$webserverProcess = new Process("ps ax|grep 8081|grep -v grep|awk '{print $1}'|xargs kill");
        $webserverProcess = new Process(array(PHP_BINARY, 'ps', 'ax', '|', 'grep','8081','|','grep','-v','grep','|','awk','\'{print $1}\'','|','xargs','kill'));
        $webserverProcess->disableOutput();
        $webserverProcess->run();
    }

    public function loginAsUser()
    {
        // see https://github.com/laravel/dusk/issues/100
        $user = User::first();
        $this->actingAs($user, 'api');
        $this->browse(function ($browser) {
            $this->browserLoginAsUser($browser);
            //    $browser->loginAs($user->id, 'api');
        });
    }


    public function browserLoginAsUser($browser)
    {

        $browser->visit('/')
               ->assertPathIs('/login')
               ->waitForText('Log in')
               ->pause(1000)
               ->type('username', 'admin')
               ->type('password', 'n4n0')
               ->press('Log in')
               ->waitForText('Do you have GPS?')
               ->press('Cancel');

        return $browser->waitForLocation('/', 60);
    }

    /**
     * Assert that a given where condition matches a soft deleted record
     *
     * @param  string $table
     * @param  array $data
     * @param  string $connection
     *
     * @return $this
     */
    protected function seeIsSoftDeletedInDatabase($table, array $data, $connection = null)
    {
        $database   = $this->app->make('db');
        $connection = $connection ?: $database->getDefaultConnection();
        $count      = $database->connection($connection)
                               ->table($table)
                               ->where($data)
                               ->whereNotNull('deleted_at')
                               ->count();
        $this->assertGreaterThan(0, $count, sprintf(
            'Found unexpected records in database table [%s] that matched attributes [%s].',
            $table,
            json_encode($data)
        ));

        return $this;
    }
}
