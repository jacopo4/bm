<?php

namespace Tests;

use App\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;


    /**
     * Default preparation for each test
     */
    public function setUp(): void
    {
        parent::setUp(); // Don't forget this!

        // if (User::count() == 0) {
        //     // Artisan::call('db:seed');
        // }
        // Artisan::call('db:seed', [
        //     '--class' => 'PatrolsTableSeeder'
        // ]);
    }
}
