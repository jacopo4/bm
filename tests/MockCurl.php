<?php

namespace Tests;

use Carbon\Carbon;
use Curl\Curl;

class MockCurl extends Curl
{

    /**
     * @param $name
     * @param $status
     * @return bool|mixed|null|string
     */
    protected function getDummyResponses($name, $data)
    {
        $name = str_replace('/', '', $name);
        $name = str_replace(':', '_', $name);
        $bad = false;
        if (isset($data['username']) && $data['username'] === 'saulo') {
            // goood one
            $bad = false;
        } elseif (isset($data['username']) && $data['username'] === 'bad') {
            // bad one
            $bad = true;
        }
        $directory = __DIR__ . "/Mocks/";
        if ($bad) {
            $file = $directory . $name . "-bad.json";
        } else {
            $file = $directory . $name . ".json";
        }


        if (file_exists($file)) {
            $this->response_headers = array();
            $content = file_get_contents($file);
            // find and replace
            $content = str_replace('%TODAY%', Carbon::now()->format('Y-m-d'), $content);
            $this->response = $content;
            $this->curl_error = false;
            $this->http_status_code = 200;
            $this->error = false;
            return true;
        }
        \Log::error("[MockCurl getDummyResponses] Response does not exist (" . $name . ") -> file: " . $file);
        echo("Response does not exist (" . $name . ") -> file: " . $file . "\n");
        return $this;
    }

    /**
     * Make a get request with optional data.
     *
     * The get request has no body data, the data will be correctly added to the $url with the http_build_query() method.
     *
     * @param string $url The url to make the get request for
     * @param array $data Optional arguments who are part of the url
     * @return self
     */
    public function get($url, $data = [])
    {
        return $this->getDummyResponses($url, $data);
    }

    /**
     * Make a post request with optional post data.
     *
     * @param string $url The url to make the post request
     * @param array $data Post data to pass to the url
     * @return self
     */
    public function post($url, $data = [], $asJson = false)
    {
        return $this->getDummyResponses($url, $data);
    }
}
