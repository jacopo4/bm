<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(
    ['namespace' => 'API', 'prefix' => 'v1'],
    function () {
        Route::post('/myLocation', 'LocationController@myLocation')->name("location.api"); #tested


        Route::post('/assessment', 'AssessmentController@create')->name('assessments.api.create');
        Route::post('/assessments/{assessment}', 'AssessmentController@update')->name('assessments.api.update');
        Route::post('/assessments/{assessment}/members/{user}', 'AssessmentController@addMember')->name('assessments.api.add.member');
        Route::delete('/assessments/{assessment}/members/{user}/remove', 'AssessmentController@removeMember')->name('assessments.api.remove.member');
        Route::delete('/assessments/{assessment}', 'AssessmentController@delete')->name('assessments.api.delete');
        Route::get('/assessments/{assessment}', 'AssessmentController@fetch')->name('assessments.api.fetch');
        Route::get('/assessments', 'AssessmentController@index')->name('assessments.api.index');#tested


        Route::get('/audits', 'AuditController@index')->name('audits.api.index'); #tested
        Route::post('/audit', 'AuditController@create')->name('audits.api.create');
        Route::post('/audits/{audit}', 'AuditController@update')->name('audits.api.update');

        Route::get('/audits/objects', 'AuditObjectController@index')->name('auditObject.api.index'); #tested

        Route::post('/object/{auditObject}/comments', 'CommentController@create')->name('comments.api.create');
        Route::post('/comments/{comment}', 'CommentController@update')->name('comments.api.update');
        Route::delete('/object/{auditObject}/comments/{comment}/delete', 'CommentController@delete')->name('comments.api.delete');

        Route::post('/object', 'AuditObjectController@create')->name('auditObject.api.create');
        Route::post('/object/{object}', 'AuditObjectController@update')->name('auditObject.api.update');

        Route::post('/object/{object}/hazard/create', 'AuditObjectHazardController@create')->name('auditObject.api.hazard.create');
        Route::post('/object/{object}/hazard/{hazard}', 'AuditObjectHazardController@update')->name('auditObject.api.hazard.update');
        Route::delete('/object/{object}/hazard/{hazard}', 'AuditObjectHazardController@delete')->name('auditObject.api.hazard.delete');

        Route::post('/object/{object}/sign/create', 'AuditObjectSignController@create')->name('auditObject.api.sign.create');
        Route::post('/object/{object}/sign/{sign}', 'AuditObjectSignController@update')->name('auditObject.api.sign.update');
        Route::delete('/object/{object}/sign/{sign}', 'AuditObjectSignController@delete')->name('auditObject.api.hazard.delete');

        Route::get('/audits/types', 'AuditObjectController@fetchTypes')->name('audits.api.fetchTypes'); #tested


        Route::get('/media', 'MediaController@index')->name('media.api.index'); #tested
        Route::post('/media', 'MediaController@create')->name('media.api.create');
        Route::delete('/media/{media}', 'MediaController@destroy')->name('media.api.delete');

        Route::get('/comments', 'CommentController@index')->name('comments.api.index'); #tested

        Route::get('/risks', 'RiskController@index')->name('risks.api.index'); #tested
        Route::post('/risks', 'RiskController@create')->name('risks.api.create');
        Route::post('/risks/{risk}', 'RiskController@update')->name('risks.api.update');
        Route::delete('/risks/{risk}/delete', 'RiskController@delete')->name('risks.api.delete');

        Route::get('/beaches/{beach}', 'BeachController@fetch')->name('beaches.api.fetch'); #tested
        // Route::get('/beaches', 'BeachController@index')->name('beaches.api.index'); #tested


        Route::get('/entities', 'EntityController@index')->name('entities.api.index'); #tested

        Route::get('/users', 'UserController@search')->name('assessments.api.search.users'); # NOT tested

        Route::post('/object/{auditObject}/recommendations', 'RecommendationController@create')->name('recommendations.api.create');
        Route::post('/recommendations/{recommendation}', 'RecommendationController@update')->name('recommendations.api.update');
        Route::delete('/object/{auditObject}/recommendations/{recommendation}/delete', 'RecommendationController@delete')->name('recommendations.api.delete');
        Route::get('/recommendations', 'RecommendationController@index')->name('recommendations.api.index');
    }
);
