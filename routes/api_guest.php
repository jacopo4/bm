<?php


Route::group(
    ['namespace' => 'API', 'prefix' => 'v1'],
    function () {


        Route::post('/login', 'Auth\LoginController@apiLogin')
            ->name('api.v1.login')
            ->middleware('throttle:60,1'); # note test

        Route::get('/app', 'AppController@index')
        ->name('api.v1.app')
        ->middleware('throttle:60,1'); #tested
    }
);
