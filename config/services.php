<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    
    'sls' => [
        'datawarehouse' => [
            'host' => env('DWH_SITE','https://apidw.qa.slsa.asn.au'),
            'client' => env('DWH_CLIENT'),
            'secret' => env('DWH_SECRET'),
            'username' => env('DWH_USERNAME'),
            'password' => env('DWH_PASSWORD')
        ],
        'members' => [
            'host' => env('MEMBERS_SITE','https://members.qa.slsa.asn.au'),
            'client' => env('MEMBERS_CLIENT'),
            'secret' => env('MEMBERS_SECRET')
        ],
        'beachsafe' => [
            'host' => env('BEACHSAFE_SITE','https://v3.beachsafe.org.au'),
            'key' => env('BEACHSAFE_TOKEN')
        ]
    ],
    'mapbox' => [
        'key' => env('MAPBOX_API_KEY')
    ]


];
