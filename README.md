<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
Master:
<a href="https://git.nano.rocks/sls/bm/-/commits/master"><img src="https://git.nano.rocks/sls/bm/badges/master/pipeline.svg" alt="Build Status Master"></a>
<a href="https://git.nano.rocks/sls/bm/-/commits/master"><img src="https://git.nano.rocks/sls/bm/badges/master/coverage.svg" alt="Build Status develop"></a>
</p>
<p align="center">
Develop
<a href="https://git.nano.rocks/sls/bm/-/commits/develop"><img src="https://git.nano.rocks/sls/bm/badges/develop/pipeline.svg" alt="Build Status Master"></a>
<a href="https://git.nano.rocks/sls/bm/-/commits/develop"><img src="https://git.nano.rocks/sls/bm/badges/develop/coverage.svg" alt="coverage Status develop"></a>
</p>


[![This project is using Percy.io for visual regression testing.](https://percy.io/static/images/percy-badge.svg)](https://percy.io/Nano-Solutions/bm)

## About Laravel


### Docker

```bash

docker build --pull --build-arg BUILD_VERSION="petr-version" -t hub.nano.rocks/sls/bm:latest  .
docker push hub.nano.rocks/sls/bm:latest
```



## Import data

```
./artisan migrate
./artisan import:objects-types
./artisan db:seed
```


## Installing Cypress
Make sure you run `npm install` and Cypress is installed
Verify Cypress installed by checking if exists: `./node_modules/.bin/cypress`

## percy.io
Export the token if you want to capture it

## Writing Tests
Write test using guide at https://docs.cypress.io/guides/core-concepts/writing-and-organizing-tests.html

## Headless Testing
Simply write test and use `./artisan cypress:test --env=testing`

## Using Cypress Program/Non Headless
* Change baseUrl in cypress.json to point to your local site
Example:
```
{
    "baseUrl": "http://bm.test",
    "chromeWebSecurity": false,
    "defaultCommandTimeout": 30000
}
```
* Run `./node_modules/.bin/cypress open` to open Cypress
* Run `./artisan cypress:db` to dump your database to fixtures for cypress to use
* Execute tests in Cypress program
* Rerun `./artisan cypress:db` after testing if needed
