import VuexORM from '@vuex-orm/core'

import VuexORMLocalForage from 'vuex-orm-localforage'
// import localForage from 'localforage';

import Assessment from '@/models/assessment'
import assessment from '@/state/assessment'

import Audit from '@/models/audit'
import audit from '@/state/audit'

import Beach from '@/models/beach'
import beach from '@/state/beach'

import AssessmentBeach from '@/models/assessment_beach'
import assessmentBeach from '@/state/assessment_beach'

import User from '@/models/user'
import user from '@/state/user'

import AssessmentUser from '@/models/assessment_user'
import assessmentUser from '@/state/assessment_user'

import AuditObject from '@/models/audit_object'
import auditObject from '@/state/audit_object'

import Comment from '@/models/comment'
import comment from '@/state/comment'

import Recommendation from '@/models/recommendation'
import recommendation from '@/state/recommendation'

import Risk from '@/models/risk'
import risk from '@/state/risk'

import Media from '@/models/media'
import media from '@/state/media'

import ObjectType from '@/models/object_type'
import objectType from '@/state/object_type'

import ObjectSubtype from '@/models/object_subtype'
import objectSubtype from '@/state/object_subtype'

import AuditObjectHazard from '@/models/audit_object_hazard'
import auditObjectHazard from '@/state/audit_object_hazard'

import AuditObjectSign from '@/models/audit_object_sign'
import auditObjectSign from '@/state/audit_object_sign'


// Create a new database instance.
const database = new VuexORM.Database()

// Register Models to the database.
database.register(Assessment, assessment)
database.register(Beach, beach)
database.register(AssessmentBeach, assessmentBeach)
database.register(User, user)
database.register(AssessmentUser, assessmentUser)
database.register(Audit, audit)
database.register(AuditObject, auditObject)
database.register(Comment, comment)
database.register(Recommendation, recommendation)
database.register(Risk, risk)
database.register(Media, media)
database.register(ObjectType, objectType)
database.register(ObjectSubtype, objectSubtype)
database.register(AuditObjectHazard, auditObjectHazard)
database.register(AuditObjectSign, auditObjectSign)

VuexORM.use(VuexORMLocalForage, {
  database,
  localforage: {
    name: 'vuex', // Name is required
    version: 1.0, // we can use this for backends that support revisions
    // driver: [
    //   localforage.INDEXEDDB,
    //   localforage.WEBSQL,
    //   localforage.LOCALSTORAGE
    // ]
  },
  actions: {
    $get: '$getFromLocal',
    $fetch: '$fetchFromLocal',
    $create: '$createLocally',
    $update: '$updateLocally',
    $delete: '$deleteFromLocal'
  }
})

// Create Vuex database plugin
const ormStore = VuexORM.install(database, { namespace: 'database' })

export default ormStore
