require('@/bootstrap');
// implement vuex
import store from '@/state/store'

// implement VueRouter
import router from '@/routes'

import 'leaflet/dist/leaflet.css'

import bugsnagClient from '@/includes/bugsnag'
import bugsnagVue from '@bugsnag/plugin-vue'


// Yes, we are comparing against a string
if (window.BUGSNAG_API_KEY != '') {
  bugsnagClient.use(bugsnagVue, Vue)
}

// support font awesome
require('@/includes/font-awesome')

// support sw
if (window.Capacitor == undefined) {
  require('@/includes/service-worker')
} else {
  // console.log('In Capacitor, so not supporting service worker')
}

// support filters
import filters from '@/includes/filters.js'
Object.keys(filters).forEach(key => Vue.filter(key, filters[key]))

// implement sweetalert2
import VueSweetalert2 from 'vue-sweetalert2'


// const options = {
//   customClass: {
//     confirmButton: 'btn btn-success',
//     cancelButton: 'btn btn-danger'
//   }
//   // buttonsStyling: false
// }

Vue.use(VueSweetalert2);

// implement timeago
import VueTimeago from 'vue-timeago'
Vue.use(VueTimeago, {
  name: 'timeago', // component name, `timeago` by default
  locale: 'en-AU' // Default locale
})

// implement scrollactive
var VueScrollactive = require('vue-scrollactive')
Vue.use(VueScrollactive)

// implement logging

import VueLogger from 'vuejs-logger'
const isProduction = process.env.NODE_ENV === 'production'

const options = {
  isEnabled: true,
  logLevel : isProduction ? 'error' : 'debug',
  stringifyArguments : false,
  showLogLevel : true,
  showMethodName : true,
  separator: '|',
  showConsoleColors: true
}

Vue.use(VueLogger, options)

// implement auth nano module
require('@/includes/mixins')
import Auth from '@/includes/auth'

import Network from '@/includes/network'

// Leaflet
// import L from 'leaflet'
// fix for double marker issue
// delete L.Icon.Default.prototype._getIconUrl

// L.Icon.Default.mergeOptions({
//   iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
//   iconUrl: require('leaflet/dist/images/marker-icon.png'),
//   shadowUrl: require('leaflet/dist/images/marker-shadow.png')
// });

// INPUT
import SelectPlus from '@/components/inputs/SelectPlus.vue'
Vue.component('select-plus', SelectPlus)

import RadioPlus from '@/components/inputs/RadioPlus.vue'
Vue.component('radio-plus', RadioPlus)

import InputPlus from '@/components/inputs/InputPlus.vue'
Vue.component('input-plus', InputPlus)

import InputMedia from '@/components/inputs/InputMedia.vue'
Vue.component('input-media', InputMedia)


import Media from '@/components/Media.vue'
Vue.component('media', Media)

import LocationMapPicker from '@/components/inputs/LocationMapPicker.vue'
Vue.component('location-map-picker', LocationMapPicker)

import Comment from '@/components/Comment.vue'
Vue.component('comment', Comment)

import Recommendation from '@/components/Recommendation.vue'
Vue.component('recommendation', Recommendation)

import UserInitialsAvatar from '@/components/UserInitialsAvatar'
Vue.component('UserInitialsAvatar', UserInitialsAvatar)

import Modal from '@/components/Modal.vue'
Vue.component('modal', Modal)

import Version from '@/components/Version.vue'
Vue.component('Version', Version)

import AssessmentHeader from "@/components/Assessment/AssessmentHeader.vue";
Vue.component('AssessmentHeader', AssessmentHeader)

import AuditHeader from "@/components/Audit/AuditHeader.vue";
Vue.component('AuditHeader', AuditHeader)

import AuditObjectSummary from "@/components/AuditObject/AuditObjectSummary.vue";
Vue.component('AuditObjectSummary', AuditObjectSummary)

import AuditObjectRisk from "@/components/AuditObject/AuditObjectRisk.vue";
Vue.component('AuditObjectRisk', AuditObjectRisk)

import AuditObjectComments from "@/components/AuditObject/AuditObjectComments.vue";
Vue.component('AuditObjectComments', AuditObjectComments)

import AuditObjectRecommendations from "@/components/AuditObject/AuditObjectRecommendations.vue";
Vue.component('AuditObjectRecommendations', AuditObjectRecommendations)

import Autocomplete from "@/components/Autocomplete/Autocomplete.vue";
Vue.component('autocomplete', Autocomplete)


// Deal with Route change
router.afterEach((to) => {
  if (to.meta.title != null) {
    document.title = to.meta.title + ' | ' + window.APP_NAME
    if (store.state.system.navbar_title !== to.meta.title) {
      store.commit('system/setTitle', to.meta.title, { root: true })
    }
  }
})

router.beforeEach((to, from, next) => {
  if (to.path !== '/login') {
    if (!Auth.guest()) {
      next()
    } else {
      next('login')
    }
  } else {
    next()
  }
})

import LayoutView from '@/pages/Layout.vue'

// run Vue
const app = new Vue({ // eslint-disable-line no-unused-vars
  el: '#app',
  store, // vuex
  router, // vue-router
  beforeCreate () {
    Auth.axios()
  },
  created () {
    if (!Auth.guest()) {
      // console.log('[setToken] Setting headers.')
      window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
      window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.state.auth.token
      // store.dispatch('system/keepDataFresh')
      // setTimeout(() => {
      //   Auth.checkSession()
      // }, 1000 * 60 * 5)
      // Auth.logging()
    }
    Network.watch()
    document.getElementById('main-loading').style.display = 'none'
    window.swal = this.$swal
  },
  render: h => h(LayoutView)
});

if (window.Cypress) {
  // only available during E2E tests
  // console.log("we see cypress");
  window.cypressVue = app
}
