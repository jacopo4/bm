import store from '@/state/store'
import router from '@/routes'

export default {
  ttl: 24, // 12 hours old data re reloaded
  guest() {
    // return false
    return store.state.auth.token === null
  },
  loading(text) {
    document.getElementById('main-loading').style.display = 'flex'
    document.getElementById('main-loading-text').innerHTML = text
  },
  stopLoading() {
    document.getElementById('main-loading').style.display = 'none'
  },
  // checkSession () {
  //   if (process.env.MIX_API_HOST === 'http://localhost:8081') {
  //     // dont run this in test causes issues
  //     return true
  //   }

  //   if (navigator.onLine) {
  //     store.dispatch('system/checkSession', store.state.auth.user.id).then(response => {
  //       console.log('User sessions all good: ', response.status)
  //       if (response.status === 'reload') {
  //         // Get Location first
  //         console.warn('Reload trigger by reload response from sessions')
  //         store.dispatch('system/keepDataFresh')
  //         window.location = '/'
  //       }
  //       // check age of timestamp
  //       store.dispatch('system/keepDataFresh')
  //     }, error => {
  //       console.log('[API] User sessions NOT good', error)
  //       this.loading('Inactive sessions detected')
  //       this.logout()
  //     })
  //   } else {
  //     console.log('User is offline dont do antyhing')
  //   }
  //   setTimeout(() => {
  //     this.checkSession()
  //   }, 1000 * 60 * 5) // 1m
  // },
  axios() {
    window.axios.defaults.baseURL = window.API_HOST
    axios.interceptors.request.use(function (config) {
      // Do something before request is sent
      // console.log('request ok', config)
      window.axiosRunning = true
      return config
    }, function (error) {
      // Do something with request error
      // console.log('request error', error)
      window.axiosRunning = false
      return Promise.reject(error)
    })
    // Add a response interceptor
    axios.interceptors.response.use(function (response) {
      // Do something with response data
      window.axiosRunning = false
      return response
    }, function (error) {
      window.axiosRunning = false
      // 406 is custom reply when VPN is down or missing member_Id
      // console.log(error)
      // if (error.response !== undefined &&
      //   error.response.status !== undefined &&
      //   error.response.status !== 406 &&
      //   error.response.status !== 404 &&
      //   error.response.status !== 500 &&
      //   error.response.status !== 502 && // proxy too long
      //   error.response.status !== 504) {
      //   // all other errors go back to logout
      //   router.push({ name: 'Logout' })
      // }
      // IF 401 log them out
      if (error.response !== undefined && error.response.status === 401) {
        // all other errors go back to logout
        router.push({ name: 'Logout' })
      }

      return Promise.reject(error)
    })
  },
  // pushLog (severity, ...message) {
  //   let payload = { message: message, severity: severity }
  //   store.dispatch('system/log', payload).then(() => {

  //   }, error => {
  //     console.error(error)
  //   })
  // },
  // logging () {
  //   if (process.env.MIX_API_HOST === 'http://localhost:8081' || process.env.MIX_REMOTE_LOG === 'false') {
  //     // dont run this in test causes issues
  //     return true
  //   }
  //   // define a new console
  //   var self = this
  //   var console = (function (oldCons) {
  //     return {
  //       log: function (...text) {
  //         oldCons.log(...text)
  //         self.pushLog('notice', ...text)
  //       },
  //       info: function (...text) {
  //         oldCons.info(...text)
  //         self.pushLog('info', ...text)
  //       },
  //       warn: function (...text) {
  //         oldCons.warn(...text)
  //         self.pushLog('warning', ...text)
  //       },
  //       error: function (...text) {
  //         oldCons.error(...text)
  //         // self.pushLog('error', ...text)
  //       }
  //     }
  //   }(window.console))
  //   // Then redefine the old console
  //   window.console = console
  // },
  login(username, password) {
    let errorMessageDefault = 'These credentials do not match our records'
    let errorMessage = ''
    return new Promise((resolve, reject) => {
      if (!this.guest()) {
        reject(new Error('User is logged in '))
      } else {
        let payload = { username: username, password: password }
        store.dispatch('auth/login', payload).then(response => {
          resolve(response)
        }, error => {
          if (error.response !== undefined && error.response.status === 406) {
            errorMessage = error.response.data.error
          } else {
            errorMessage = errorMessageDefault
          }
          reject(errorMessage)
        }, { root: true })
      }
    })
  },
  logout() {
    return new Promise((resolve, reject) => {
      store.dispatch('auth/logout').then(() => {
        store.commit('auth/removeToken')
        // window.vm.$destroy()
        //window.vm = null
        window.localStorage.removeItem('vuex')
        window.location.reload()
        resolve()
      }, error => {
        reject(new Error(error))
      }, { root: true })
    })
  }

}
