import moment from 'moment'
moment.locale('en-AU');
export default {
  
  formatFullDate: function (value) {
    if (value !== undefined) {
      return moment(value).format('h:mm a, D/MMM')
    } else {
      return false
    }
  },

  formatTime: function (value) {
    if (value !== undefined) {
      return moment(value, ['H:m']).format('h:mm a')
    } else {
      return false
    }
  },
  formatTimeFromDate: function (value) {
    if (value !== undefined) {
      return moment(value).format('h:mm a')
    } else {
      return false
    }
  },

  formatDate: function (value) {
    if (value == '') return ''
    if (value !== undefined) {
      return moment(value).format('ddd, D/MMM/YY')
    } else {
      return false
    }
  },
  formatDateCalendar: function (value) {
    if (value == '') return ''
    if (value !== undefined && value != null) {
      return moment(value).format('DD/MM/YYYY')
    } else {
      return false
    }
  },

  round: function (value, decimals = 1) {
    if (!value) {
      value = 0
    }
    if (!decimals) {
      decimals = 0
    }
    value = Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals)
    return value
  }
}
