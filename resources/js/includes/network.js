import store from '@/state/store'

export default {
  update () {
    store.state.system.network_online = navigator.onLine
    store.state.system.network_offline = !navigator.onLine
  },
  watch () {
    this.update()
    window.addEventListener('online', this.update)
    window.addEventListener('offline', this.update)
  }
}
