import store from '@/state/store'
// import Vue from 'vue'

export default {
  _isAvailable () {
    return 'geolocation' in window.navigator
  },
  getNavigatorLocation () {
    return new Promise((resolve, reject) => {
      if (!this._isAvailable()) {
        reject(new Error('no browser support'))
      } else {
        window.navigator.geolocation.getCurrentPosition(
          position => {
            resolve({
              lat: position.coords.latitude,
              lng: position.coords.longitude,
              error: null
            })
          },
          (error) => {
            reject(error)
          },
          {
            enableHighAccuracy: false,
            timeout: 40000, // 40s
            maximumAge: 30 * 60 * 1000 // 30m
          }
        )
      }
    })
  },
  async getCapacitorLocation () {
    const position = await window.Capacitor.Plugins.Geolocation.getCurrentPosition({
      enableHighAccuracy: false,
      maximumAge: 30 * 60 * 1000, // 30m
      timeout: 40000
    })
    return({
      lat: position.coords.latitude,
      lng: position.coords.longitude,
    })
  },

  async getLocationAndDetails () {
    store.commit('loading/setLoading', { 'collection': 'location', 'status': 'loading' }, { root: true })
    // Capacitor
    if (window.Capacitor !== undefined) {
      let location = await this.getCapacitorLocation()
      try {
        store.commit('loading/setLoading', { 'collection': 'location', 'status': 'loaded' }, { root: true })
        store.commit('location/setLocation', location, { root: true })
        store.dispatch('location/fetchLocationDetails', { root: true })
      } catch(error) {
        let position = []
        position.error = error.message + ' (' + error.code + ')'
        position.lat = null
        position.lng = null
        store.commit('location/setLocation', position, { root: true })
        store.commit('loading/setLoading', {
          'collection': 'location',
          'status': 'error',
          'error': position.error,
          'timestamp': null
        }, { root: true })
      }
    // Web
    } else {
      this.getNavigatorLocation()
        .then(location => {
          store.commit('loading/setLoading', { 'collection': 'location', 'status': 'loaded' }, { root: true })
          store.commit('location/setLocation', location, { root: true })
          store.dispatch('location/fetchLocationDetails', { root: true })
        })
        .catch(error => {
          let position = []
          position.error = error.message + ' (' + error.code + ')'
          position.lat = null
          position.lng = null
          store.commit('location/setLocation', position, { root: true })
          store.commit('loading/setLoading', {
            'collection': 'location',
            'status': 'error',
            'error': position.error,
            'timestamp': null
          }, { root: true })
        })
    }
  }
}
