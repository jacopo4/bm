import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'

import {
  faHome, faSignOut, faSync, faMapMarker, faPlus, faArrowLeft,faArrowRight,faComments, faEye,
  faClipboardListCheck, faBook, faUmbrellaBeach,
  faRoad, faVolleyballBall, faFlag,faSirenOn, faExclamationTriangle, faLifeRing, faRestroom, faMapSigns,
  faInfoCircle, faSignal, faSignalSlash, faUsers, faEnvelope,
  faMap, faSun, faTint, faCaretUp, faCaretDown,
  faClock, faCamera, faEdit, faTimes,  faSpinner, faCrosshairs, 
  faTrashAlt, faTimesCircle, faThumbsUp, faAngleRight, faPhotoVideo, faUpload, faDownload
} from '@fortawesome/pro-light-svg-icons'
  
library.add(
  faHome, faSignOut,faPlus, faArrowLeft, faArrowRight, faEnvelope, faComments,faEye,
  faClipboardListCheck, faBook, faUmbrellaBeach,
  faRoad, faVolleyballBall,  faFlag, faSirenOn,faExclamationTriangle, faLifeRing, faRestroom, faMapSigns,
  faInfoCircle, faSync, faSignal,faSignalSlash, faUsers,
  faMap, faSun, faTint, faMapMarker, faCaretUp, faCaretDown,
  faClock,  faCamera, faEdit, faTimes, faSpinner, faCrosshairs, 
  faTrashAlt, faTimesCircle, faThumbsUp, faAngleRight, faPhotoVideo, faUpload, faDownload
)
  
dom.watch()

Vue.component('icon', FontAwesomeIcon)
Vue.component('icon-layers', FontAwesomeLayers)
Vue.component('icon-layers-text', FontAwesomeLayersText)