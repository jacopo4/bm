import bugsnag from '@bugsnag/js'

const bugsnagClient = bugsnag(

  { apiKey: (window.BUGSNAG_API_KEY ? window.BUGSNAG_API_KEY : 'no-key'),
    releaseStage: window.APP_ENV,
    appVersion: window.APP_VERSION,
    notifyReleaseStages: window.BUGSNAG_NOTIFY_RELEASE_STAGES.split(','),
    appType: 'vue-client',
    consoleBreadcrumbsEnabled: true,
    autoCaptureSessions: true,
    logger: null
  }
)
export default bugsnagClient