// Audit Model
import { Model } from '@vuex-orm/core'
import Beach from '@/models/beach'
import User from '@/models/user'
import Assessment from '@/models/assessment'
import Audit from '@/models/audit'
import Comment from '@/models/comment'
import Recommendation from '@/models/recommendation'
import Risk from '@/models/risk'
import Media from '@/models/media'
import ObjectType from '@/models/object_type'
import ObjectSubtype from '@/models/object_subtype'
import AuditObjectHazard from '@/models/audit_object_hazard'
import AuditObjectSign from '@/models/audit_object_sign'

import { v1 as uuid } from 'uuid'
const random = () => uuid()
export default class AuditObject extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'auditObjects'

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.uid(random),
      assessment_id: this.attr(''),
      beach_id: this.attr(''),
      user_id: this.attr(''),
      audit_id: this.attr(''),
      title: this.attr(''),
      type_id: this.attr(''),
      subtype_id: this.attr(''),
      latitude: this.attr(''),
      longitude: this.attr(''),
      metadata: this.attr(null),
      online_id: this.attr(''),
      assessment: this.belongsTo(Assessment, 'assessment_id', 'id'),
      beach: this.belongsTo(Beach, 'beach_id', 'id'),
      user: this.belongsTo(User, 'user_id', 'id'),
      audit: this.belongsTo(Audit, 'audit_id', 'id'),
      type: this.belongsTo(ObjectType, 'type_id', 'id'),
      subtype: this.belongsTo(ObjectSubtype, 'subtype_id', 'id'),
      comments: this.hasMany(Comment, 'audit_object_id', 'id'),
      recommendations: this.hasMany(Recommendation, 'audit_object_id', 'id'),
      risks: this.hasMany(Risk, 'audit_object_id', 'id'),
      media: this.hasMany(Media, 'audit_object_id', 'id'),
      hazards: this.belongsToMany(
        AuditObject,
        AuditObjectHazard,
        'audit_object_id',
        'hazard_object_id',
      ),
      signs: this.belongsToMany(
        AuditObject,
        AuditObjectSign,
        'audit_object_id',
        'sign_object_id',
      ),
      hazardChildren: this.belongsToMany(
        AuditObject,
        AuditObjectHazard,
        'hazard_object_id',
        'audit_object_id',
      ),
      signChildren: this.belongsToMany(
        AuditObject,
        AuditObjectSign,
        'sign_object_id',
        'audit_object_id',
      )
    }
  }
}
