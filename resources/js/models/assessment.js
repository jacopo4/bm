// Audit Model
import { Model } from '@vuex-orm/core'
import Beach from '@/models/beach'
import Media from '@/models/media'
import User from '@/models/user'
import Audit from '@/models/audit'
import AuditObject from '@/models/audit_object'
import AssessmentBeach from '@/models/assessment_beach'
import AssessmentUser from '@/models/assessment_user'
import Comment from '@/models/comment'
import Recommendation from '@/models/recommendation'


export default class Assessment extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'assessments'

  static primaryKey = 'id'

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.attr(''),
      title: this.attr(''),
      type: this.number(1),
      area: this.attr(''),
      summary: this.attr(''),
      status: this.number(1),
      beaches: this.belongsToMany(Beach, AssessmentBeach, 'assessment_id', 'beach_id'),
      users: this.belongsToMany(User, AssessmentUser, 'assessment_id', 'user_id'),
      start_date: this.attr(''),
      end_date: this.attr(''),
      delivery_date: this.attr(''),
      audits: this.hasMany(Audit, 'assessment_id', 'id'),
      objects: this.hasMany(AuditObject, 'assessment_id', 'id'),
      comments: this.hasMany(Comment, 'assessment_id', 'id'),
      recommendations: this.hasMany(Recommendation, 'assessment_id', 'id'),
      media: this.hasMany(Media, 'assessment_id', 'id')
    }
  }
}
