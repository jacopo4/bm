// Audit Model
import { Model } from '@vuex-orm/core'

export default class AuditObjectHazard extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'auditObjectHazard'

  static primaryKey = ['audit_object_id', 'hazard_object_id']

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      audit_object_id: this.attr(null),
      hazard_object_id: this.attr(null),
    }
  }
}
