// Audit Model
import { Model } from '@vuex-orm/core'
import AuditObject from '@/models/audit_object'
import Assessment from '@/models/assessment'
import Beach from '@/models/beach'
import User from '@/models/user'
import Audit from '@/models/audit'
import ObjectType from '@/models/object_type'
import { v1 as uuid } from 'uuid'
const random = () => uuid()

export default class Media extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'media'

  static primaryKey = 'id'
  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.uid(random),
      created_at: this.attr(''),
      updated_at: this.attr(''),
      size: this.attr(''),
      mime_type: this.attr(''),
      name: this.attr(''),
      progress: this.attr(null),
      url: this.attr(''),
      assessment_id: this.attr(''),
      beach_id: this.attr(''),
      audit_id: this.attr(''),
      audit_object_id: this.attr(''),
      object_type_id: this.attr(''),
      user_id: this.attr(''),
      online_id: this.attr(''),
      // reletionship
      auditObject: this.belongsTo(AuditObject, 'audit_object_id', 'id'),
      assessment: this.belongsTo(Assessment, 'assessment_id', 'id'),
      beach: this.belongsTo(Beach, 'beach_id', 'id'),
      user: this.belongsTo(User, 'user_id', 'id'),
      audit: this.belongsTo(Audit, 'audit_id', 'id'),
      objectType: this.belongsTo(ObjectType, 'object_type_id', 'id')
      
    }
  }

  /**
   * Get full name of the user.
   */
  get is_offline () {
    return this.url.startsWith("data:")
  }
}
