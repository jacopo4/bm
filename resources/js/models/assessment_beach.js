// Audit Model
import { Model } from '@vuex-orm/core'

export default class AssessmentBeach extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'assessmentBeach'

  static primaryKey = ['assessment_id', 'beach_id']

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      assessment_id: this.attr(null),
      beach_id: this.attr(null)
    }
  }
}
