// Audit Model
import { Model } from '@vuex-orm/core'

export default class AuditObjectSign extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'auditObjectSign'

  static primaryKey = ['audit_object_id', 'sign_object_id']

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      audit_object_id: this.attr(null),
      sign_object_id: this.attr(null),
    }
  }
}
