// Audit Model
import { Model } from '@vuex-orm/core'
import ObjectSubtype from '@/models/object_subtype'

export default class ObjectType extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'object_types'

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.attr(''),
      title: this.attr(''),
      explanation: this.attr(''),
      is_group: this.attr(''),
      subtypes: this.hasMany(ObjectSubtype, 'type_id', 'id')
    }
  }
}
