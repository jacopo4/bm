// Audit Model
import Audit from '@/models/audit'
import AuditObject from '@/models/audit_object'
import Media from '@/models/media'
import { Model } from '@vuex-orm/core'

export default class User extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'users'

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.attr(''),
      member_id: this.attr(''),
      first_name: this.attr(''),
      last_name: this.attr(''),
      audits: this.hasMany(Audit, 'user_id', 'id'),
      objects: this.hasMany(AuditObject, 'user_id', 'id'),
      media: this.hasMany(Media, 'user_id', 'id')
    }
  }
}
