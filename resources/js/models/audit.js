// Audit Model
import { Model } from '@vuex-orm/core'
import Beach from '@/models/beach'
import User from '@/models/user'
import Comment from '@/models/comment'
import Recommendation from '@/models/recommendation'
import Assessment from '@/models/assessment'
import AuditObject from '@/models/audit_object'
import Media from '@/models/media'
import Risk from '@/models//risk'
import { v1 as uuid } from 'uuid'

const random = () => uuid()

export default class Audit extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'audits'

  static primaryKey = 'id'


  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.uid(random),
      assessment_id: this.attr(''),
      beach_id: this.attr(''),
      user_id: this.attr(''),
      title: this.attr(''),
      summary: this.attr(''),
      date_visit: this.attr(''),
      status: this.attr(''),
      online_id: this.attr(''),
      uploading: this.attr(false),

      beach_hazard_rating: this.attr(''),
      beach_type: this.attr(''),
      beach_fvr_development: this.attr(''),
      beach_fvr_population: this.attr(''),
      beach_fvr_frequency: this.attr(''),
      beach_local_population: this.attr(''),
      beach_human_water: this.attr(''),
      beach_human_water_conflict: this.attr(''),
      beach_human_beach: this.attr(''),
      beach_human_beach_conflict: this.attr(''),

      created_at: this.attr(''),
      updated_at: this.attr(''),

      assessment: this.belongsTo(Assessment, 'assessment_id', 'id'),
      beach: this.belongsTo(Beach, 'beach_id', 'id'),
      user: this.belongsTo(User, 'user_id', 'id'),
      objects: this.hasMany(AuditObject, 'audit_id', 'id'),
      media: this.hasMany(Media, 'audit_id', 'id'),
      risks: this.hasMany(Risk, 'audit_id', 'id'),
      comments: this.hasMany(Comment, 'audit_id', 'id'),
      recommendations: this.hasMany(Recommendation, 'audit_id', 'id')
    }
  }


}
