// Audit Model
import { Model } from '@vuex-orm/core'
import Audit from '@/models/audit'
import AuditObject from '@/models/audit_object'
import Media from '@/models/media'


export default class Beach extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'beaches'

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.attr(''),
      absamp_id: this.attr(''),
      key: this.attr(''),
      title: this.attr(''),
      state: this.attr(''),
      image: this.attr(''),
      metadata: this.attr([]),
      longitude: this.number(0),
      latitude: this.number(0),
      distance: this.attr(''),
      audits: this.hasMany(Audit, 'beach_id', 'id'),
      objects: this.hasMany(AuditObject, 'beach_id', 'id'),
      media: this.hasMany(Media, 'beach_id', 'id')
    }
  }
}
