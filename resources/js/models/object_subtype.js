// Audit Model
import { Model } from '@vuex-orm/core'
import ObjectType from '@/models/object_type'

export default class ObjectSubtype extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'object_subtypes'

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.attr(''),
      type_id: this.attr(''),
      title: this.attr(''),
      order: this.attr(''),
      type: this.belongsTo(ObjectType, 'type_id', 'id'),
      metadata_type: this.attr(null),
    }
  }
}
