// Audit Model
import { Model } from '@vuex-orm/core'
import Beach from '@/models/beach'
import User from '@/models/user'
import Assessment from '@/models/assessment'
import Audit from '@/models/audit'
import AuditObject from '@/models/audit_object'
import { v1 as uuid } from 'uuid'
const random = () => uuid()

export default class Comment extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'comments'

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.uid(random),
      assessment_id: this.attr(''),
      beach_id: this.attr(''),
      user_id: this.attr(''),
      audit_id: this.attr(''),
      audit_object_id: this.attr(''),
      object_type_id: this.attr(''),
      text: this.attr(''),
      online_id: this.attr(''),
      created_at: this.attr(''),
      updated_at: this.attr(''),
      assessment: this.belongsTo(Assessment, 'assessment_id', 'id'),
      beach: this.belongsTo(Beach, 'beach_id', 'id'),
      user: this.belongsTo(User, 'user_id', 'id'),
      audit: this.belongsTo(Audit, 'audit_id', 'id'),
      auditObject: this.belongsTo(AuditObject, 'audit_object_id', 'id')
    }
  }
}
