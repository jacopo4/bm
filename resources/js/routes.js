import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: require('@/pages/Home.vue').default,
      meta: {
        title: 'SLSA Beach Management', displayTopBar: true
      }
    },

    {
      path: '/login',
      name: 'Login',
      component: require('@/pages/Login.vue').default,
      meta: {
        title: 'Login', displayTopBar: false
      }
    },
    {
      path: '/logout',
      name: 'Logout',
      component: require('@/pages/Logout.vue').default,
      meta: {
        title: 'Logout', displayTopBar: false
      }

    },
    {
      path: '/welcome',
      name: 'Welcome',
      props: true,
      component: require('@/pages/Welcome.vue').default,
      meta: {
        title: 'Welcome', displayTopBar: false
      }
    },
    {
      path: '/assessments/new',
      name: 'AssessmentNew',
      props: true,
      component: require('@/pages/Assessment/AssessmentNew.vue').default,
      meta: {
        title: 'Assessment', displayTopBar: false, displayBackTopBar: true
      }
    },
    {
      path: '/assessments/:assessmentId',
      name: 'Assessment',
      props: true,
      component: require('@/pages/Assessment/AssessmentLayout.vue').default,
      meta: {
        title: 'Assessment', displayTopBar: false, displayBackTopBar: true
      },
      children: [
        {
          path: 'status', name: 'AssessmentStatus', props: true, component: require('@/pages/Assessment/AssessmentStatus.vue').default,
          meta: { displayTopBar: false, displayBackTopBar: true },
        },

        // Assessment Audit
        {
          path: 'audit', name: 'AssessmentAudit', props: true, component: require('@/pages/Assessment/AssessmentAudit.vue').default,
          meta: { displayTopBar: false, displayBackTopBar: true }
        },
        {
          path: 'audit/new', name: 'AuditNew', props: true, component: require('@/pages/Audit/AuditNew.vue').default,
          meta: { displayTopBar: false, displayBackTopBar: true  },
        },
        {
          path: 'audit/:auditId', name: 'AuditLayout', props: true, component: require('@/pages/Audit/AuditLayout.vue').default,
          meta: { displayTopBar: false, displayBackTopBar: true },
          children: [
            {
              path: 'type/:objectTypeId', name: 'ObjectGroupList', props: true, component: require('@/pages/Object/ObjectGroupList.vue').default,
              meta: { displayTopBar: false, displayBackTopBar: true },
            },
            {
              path: 'object/:objectId', name: 'AuditObjectEdit', props: true, component: require('@/pages/Object/ObjectEdit.vue').default,
              meta: { displayTopBar: false, displayBackTopBar: true },
            }
          ]
        },


        // Assessment Locations
        {
          path: 'locations', name: 'AssessmentLocations', props: true, component: require('@/pages/Assessment/AssessmentLocations.vue').default,
          meta: { displayTopBar: false, displayBackTopBar: true  },
        },
        {
          path: 'comments', name: 'AssessmentComments', props: true, component: require('@/pages/Assessment/AssessmentComments.vue').default,
          meta: { displayTopBar: false, displayBackTopBar: true },
        },
        {
          path: 'recommendations', name: 'AssessmentRecommendations', props: true, component: require('@/pages/Assessment/AssessmentRecommendations.vue').default,
          meta: { displayTopBar: false, displayBackTopBar: true },
        },
      ],
    },
    {
      // not found handler
      path: '*',
      component: require('@/pages/404.vue').default
    }
  ],
  base: '/',
  mode: (window.Capacitor === undefined) ? 'history' : 'hash',

  // saveScrollPosition: true,
  // scrollBehavior(to) {
  //   if (to.hash) {
  //     return new Promise((resolve) => {
  //       setTimeout(() => {
  //         resolve({
  //           selector: to.hash
  //         });
  //       }, 1000);
  //     });
  //   }
  // }
})
