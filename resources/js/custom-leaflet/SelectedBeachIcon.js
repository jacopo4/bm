import L from 'leaflet';

// export default L.icon({
//   iconUrl: '/images/patrolled.gif',
//   iconSize: [40, 30],
//   iconAnchor: [20, 15],
//   popupAnchor: [-3, -76],
// });


export default L.divIcon({
  // specify a class name that we can refer to in styles, as we
  // do above.
  className: 'selected-beach-icon',
  // html here defines what goes in the div created for each marker
  html: '<i class="fal fa-umbrella-beach fa-2x text-primary"></i>',
  // and the marker width and height
  iconSize: [30, 30]
});