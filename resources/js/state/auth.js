import axios from 'axios'
import bugsnagClient from '@/includes/bugsnag'
import AssessmentUser from '@/models/assessment_user'
export const state = {
  // User information
  user: [],
  token: null,
  permissions: []
}

const mutations = {
  setUser (state, user) {
    state.user = user
    state.permissions = user.acl
    if (typeof bugsnagClient !== 'undefined') {
      bugsnagClient.user = user
    }
  },
  setToken (state, token) {
    state.token = token
    window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
    window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
  },
  removeToken (state) {
    state.token = null
  }
}

const getters = {
  belongsToMe: (state) => (model) => {
    return state.user.id === model.user_id
  },
  isAdmin: (state) => (model) => {
    let pivot = AssessmentUser.query().where('user_id', state.user.id).where('assessment_id', model.assessment_id ?? model.id).first()
    if (pivot) {
      return pivot.role_id === 1
    }
    return false
  },
  isAuditor: (state) => (model) => {
    let pivot = AssessmentUser.query().where('user_id', state.user.id).where('assessment_id', model.assessment_id ?? model.id).first()
    if (pivot) {
      return pivot.role_id === 2
    }
    return false
  },
  isClient: (state) => (model) => {
    let pivot = AssessmentUser.query().where('user_id', state.user.id).where('assessment_id', model.assessment_id ?? model.id).first()
    if (pivot) {
      return pivot.role_id === 3
    }
    return false
  },
}

const actions = {
  // auth
  login: (context, payload) => {
    return new Promise((resolve, reject) => {
      axios.post('/api/v1/login', { 'username': payload.username, 'password': payload.password })
        .then(function (response) {
          context.commit('setUser', response.data.user)
          context.commit('setToken', response.data.user.api_token)

          if (response.data.user.longitude !== null) {
            context.commit('location/setLocation', {
              'lat': response.data.user.latitude,
              'lng': response.data.user.longitude
            }, { root: true })
          }

          resolve(response)
        })
        .catch(function (error) {
          // console.log(error)
          reject(error)
        })
    })
  },
  logout: (state) => {
    return new Promise((resolve, reject) => {
      state.commit('removeToken')
      // console.log('TODO use ', reject)
      resolve(reject)
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
