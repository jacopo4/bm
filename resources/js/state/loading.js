const state = {
  loading: {
    location: null,
    assessment: null
  }
}

const actions = {
  setLoading(context) {
    context.commit('setLoading')
  }
}

const mutations = {
  setLoading(state, payload) {
    if (state.loading[payload.collection]) {
      payload.timestamp = state.loading[payload.collection]['timestamp']
    } else {
      payload.timestamp = null
    }
    payload.timestamp = null
    if (payload.status === 'loaded') {
      payload.timestamp = Date.now()
    }
    state.loading[payload.collection] = payload
  }
}

const getters = {
  loading: function (state) {
    return state.loading
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
