import Assessment from '@/models/assessment';
import Beach from '@/models/beach';
import AssessmentBeach from '@/models/assessment_beach';
import User from '@/models/user';
import AssessmentUser from '@/models/assessment_user';
import Audit from '@/models/audit';
import AuditObject from '@/models/audit_object';
import AuditObjectHazard from '@/models/audit_object_hazard';
import AuditObjectSign from '@/models/audit_object_sign';
import Comment from '@/models/comment';
import Recommendation from '@/models/recommendation';
import Risk from '@/models/risk';
import Media from '@/models/media';
import ObjectSubtype from '@/models/object_subtype';
import ObjectType from '../models/object_type';


const state = {
  // Title of the page and top nav bar
  navbar_title: null,
  sidebar_open: false,

  // check network status
  network_online: navigator.onLine,
  network_offline: !navigator.onLine,

  // check app mode
  app_online: true,
  app_offline: false,
  app_loading: false,
  app_uploading: false,

  version: null
}

const mutations = {
  setTitle (state, title) {
    state.navbar_title = title
    document.title = title
  },
  setVersion (state, version) {
    state.version = version
  },
  openSideBar (state, value) {
    state.sidebar_open = value
  },
  setAppOnline (state, value) {
    state.app_online = value
    state.app_offline = !value
  },
  setAppOffline (state, value) {
    state.app_online = !value
    state.app_offline = value
  },
  setAppLoading (state, value) {
    state.app_loading = value
  },
  setAppUpoading (state, value) {
    state.app_uploading = value
  }
}

const getters = {
}

const actions = {
  /**
   * Load data from IndexBD into Store
   */
  async loadDataLocaly() {
    // Vue.$log.debug('System - loadDataLocaly Start');

    await Assessment.$fetchFromLocal()
    await Beach.$fetchFromLocal()
    await AssessmentBeach.$fetchFromLocal()
    await User.$fetchFromLocal()
    await AssessmentUser.$fetchFromLocal()
    await Audit.$fetchFromLocal()
    await AuditObjectHazard.$fetchFromLocal()
    await AuditObjectSign.$fetchFromLocal()
    await AuditObject.$fetchFromLocal()
    await Risk.$fetchFromLocal()
    await Media.$fetchFromLocal()
    await ObjectType.$fetchFromLocal()
    await ObjectSubtype.$fetchFromLocal()
    await Comment.$fetchFromLocal()
    await Recommendation.$fetchFromLocal()

    // Vue.$log.debug('System - loadDataLocaly End');
  },
  /**
   * Delete all cloud based data in our Store / IndexDB
   */
  async deleteOnlineLocaly() {
    // Vue.$log.debug('System - deleteOnlineLocaly Start');
    // context.dispatch('database/deleteAll',null,{root:true})

    // delete all online
    Audit.$deleteFromLocal((audit) => {
      return Number.isInteger(audit.id)
    })

    Assessment.$deleteFromLocal((assessment) => {
      return Number.isInteger(assessment.id)
    })

    // Beach.$deleteFromLocal((beach) => {
    //   return Number.isInteger(beach.id)
    // })

    AuditObject.$deleteFromLocal((auditObject) => {
      return Number.isInteger(auditObject.id)
    })

    AuditObjectHazard.$deleteFromLocal((hazard) => {
      return Number.isInteger(hazard.audit_object_id)
    })

    AuditObjectSign.$deleteFromLocal((sign) => {
      return Number.isInteger(sign.audit_object_id)
    })

    Comment.$deleteFromLocal((comment) => {
      return Number.isInteger(comment.id)
    })

    Recommendation.$deleteFromLocal((recommendation) => {
      return Number.isInteger(recommendation.id)
    })

    Media.$deleteFromLocal((media) => {
      return Number.isInteger(media.id)
    })

    Risk.$deleteFromLocal((risk) => {
      return Number.isInteger(risk.id)
    })

    ObjectType.$deleteFromLocal((objectType) => {
      return Number.isInteger(objectType.id)
    })

    ObjectSubtype.$deleteFromLocal((objectSubType) => {
      return Number.isInteger(objectSubType.id)
    })

    // Vue.$log.debug('System - deleteOnlineLocaly End');
  },
  /**
   * Refresh all data (online) from cloud API
   */
  async refresh(context) {

    context.dispatch('setLoading', true)
    // Vue.$log.debug('System - Refresh Start');
    await context.dispatch('deleteOnlineLocaly')

    let promises = []
    promises.push(Assessment.dispatch('fetchAssessments').then(
      response => {
        if (!response.data.errors) {
          // Assessment.deleteAll()
          response.data.assessment.forEach((assessment) => {
            let pivots = []
            assessment.users.forEach((user) => {
              pivots.push(user.pivot)
            })
            Assessment.$createLocally({ data: assessment })
            AssessmentUser.$createLocally({data: pivots})
          })
        }
      }
    ));

    // promises.push(Beach.dispatch('fetchBeaches').then(
    //   response => {
    //     if (!response.data.errors) {
    //       // Beach.deleteAll()
    //       Beach.$createLocally({ data: response.data.beaches })
    //     }
    //   }
    // ))

    promises.push(Audit.dispatch('fetchAudits').then(
      response => {
        if (!response.data.errors) {
          // Audit.deleteAll()
          Audit.$createLocally({ data: response.data.audit })
        }
      }
    ))


    promises.push(AuditObject.dispatch('fetchAuditsObjects').then(
      response => {
        if (!response.data.errors) {
          // AuditObject.deleteAll()
          response.data.audit_object.forEach((auditObject) => {
            let hazardPivots = []
            auditObject.hazards.forEach((hazard) => {
              hazardPivots.push({
                audit_object_id: auditObject.id,
                hazard_object_id: hazard.id,
              })
            })
            let signPivots = []
            auditObject.signs.forEach((sign) => {
              signPivots.push({
                audit_object_id: auditObject.id,
                sign_object_id: sign.id,
              })
            })
            AuditObject.$createLocally({ data: auditObject })
            AuditObjectHazard.$createLocally({data: hazardPivots})
            AuditObjectSign.$createLocally({data: signPivots})
          })
        }
      }
    ))

    promises.push(Comment.dispatch('fetchComments').then(
      response => {
        if (!response.data.errors) {
          // Comment.deleteAll()
          Comment.$createLocally({ data: response.data.comment })
        }
      }
    ))

    promises.push(Recommendation.dispatch('fetchRecommendations').then(
      response => {
        if (!response.data.errors) {
          // Comment.deleteAll()
          Recommendation.$createLocally({ data: response.data.recommendation })
        }
      }
    ))

    promises.push(Media.dispatch('fetchMedia').then(
      response => {
        if (!response.data.errors) {
          // Media.deleteAll()
          Media.$createLocally({ data: response.data.media })
        }
      }
    ))

    promises.push(Risk.dispatch('fetchRisks').then(
      response => {
        if (!response.data.errors) {
          // Risk.deleteAll()
          Risk.$createLocally({ data: response.data.risk })

        }
      }
    ))

    promises.push(ObjectType.dispatch('fetchTypes').then(
      response => {
        if (!response.data.errors) {
          // ObjectSubtype.deleteAll()
          // ObjectType.deleteAll()
          ObjectSubtype.$createLocally({ data: response.data.subtypes })
        }
      }
    ))

    Promise.all(promises)
      .then(() => {
        // console.log('all promises are done')
        context.dispatch('loadDataLocaly')
        context.dispatch('setLoading', false)
      })
      // .catch((error) => {
      //   //     console.log(`Error in executing all ${error}`)
      // })

    //  Vue.$log.debug('System - Fetch from API  finish');

  },
  /**
   *
   * Upload offline audits and their media
   * @param {*} context
   */
  async uploadData(context) {

    context.dispatch('setUploading', true)
    // Vue.$log.debug('System - uploadData Start');

    // list them
    let offlineAudits = Audit.query()
      .with('assessment')
      .with('beach')
      .with('user')
      .with('objects')
      .with('objects.comments')
      .with('objects.recommendations')
      .with('objects.risks')
      .with('objects.media')
      .where((audit) => {
        return !Number.isInteger(audit.id)
      }).get()


    for (const audit of offlineAudits) {
      // count kids and compere later
      let counts = {
        media: 0,
        risks: 0,
        comments: 0,
        recommendations: 0,
        objects: 0
      }


      // upload Audit
      var params = {
        assessment_id: audit.assessment_id,
        beach_id: audit.beach_id,
        date_visit: audit.date_visit,
        status: 1, // 0 offline , 1 online
        summary: audit.summary,
        title: audit.title,
        updated_at: audit.updated_at,
        created_at: audit.created_at,

        beach_fvr_development: audit.beach_fvr_development,
        beach_fvr_frequency: audit.beach_fvr_frequency,
        beach_fvr_population: audit.beach_fvr_population,
        beach_hazard_rating: audit.beach_hazard_rating,
        beach_human_beach: audit.beach_human_beach,
        beach_human_beach_conflict: audit.beach_human_beach_conflict,
        beach_human_water: audit.beach_human_water,
        beach_human_water_conflict: audit.beach_human_water_conflict,
        beach_local_population: audit.beach_local_population,
        beach_type: audit.beach_type
      }
      // console.log("About to post to API ")

      // use keep track of offline hazard relationship
      let offlineIds = []
      let hazardPivots = []
      let signPivots = []
      try {
        const response = await axios.post('/api/v1/audit', params)
        const data = await response.data
        // update local Offlien Audit with online_id = response.data.id
        // console.log("Audit saved")
        await Audit.$updateLocally({
          where: audit.id,
          data: {
            uploading: true,
            online_id: data.audit.id
          }
        })
        // change new id
        audit.online_id = data.audit.id
        // insert new Onlien AUdit
        await Audit.$createLocally({ data: response.data.audit })
          .then( () => {
            // console.log("Insert new Audit (online)")
          })


        // upload AuditObject
        counts['objects'] += audit.objects.length
        // console.log("About to upload "+audit.objects.length+" Object")
        for (const object of audit.objects) {
          // object
          let objectParams = {
            title: object.title,
            type_id: object.type_id,
            subtype_id: object.subtype_id,
            audit_id: audit.online_id,
            latitude: object.latitude,
            longitude: object.longitude,
            created_at: object.created_at,
            updated_at: object.updated_at,
            metadata: object.metadata
          }
          const objectResponse = await axios.post('/api/v1/object', objectParams)
          // update online_id in offline storage for this object
          await AuditObject.$updateLocally({
            where: object.id,
            data: {
              online_id: objectResponse.data.object.id
            }
          })
          offlineIds.push(object.id)
          // insert new Onlien Object
          await AuditObject.$createLocally({ data: objectResponse.data.object })
            .then( () => {
              // console.log("Insert new Object (online)")
            })


          // Upload AuditObject.Media
          counts['media'] += object.media.length
          // console.log("About to upload "+object.media.length+" Media")
          for (const media of object.media) {
            // console.log("About to upload Media: ")
            let mediaParams = {
              audit_object_id: objectResponse.data.object.id,
              name: media.name,
              url: media.url,
              created_at: media.created_at,
              updated_at: media.updated_at
            }

            const mediaResponse = await axios.post('/api/v1/media', mediaParams)
            // update online_id in offline storage for this object
            await Media.$updateLocally({
              where: media.id,
              data: {
                online_id: mediaResponse.data.media.id
              }
            })
            // insert new Onlien Media
            await Media.$createLocally({ data: mediaResponse.data.media })
              .then( () => {
                // console.log("Insert new Media (online)")
              })

          }

          // Upload AuditObject.Comments
          counts['comments'] += object.comments.length
          // console.log("About to upload "+object.comments.length+" Comment")
          for (const comment of object.comments) {
            // console.log("About to upload Comment: ")
            let commentParams = {
              text: comment.text,
              created_at: comment.created_at,
              updated_at: comment.updated_at
            }
            // TODO shoudl we make this as the other API post?
            const commentResponse = await axios.post('/api/v1/object/'+objectResponse.data.object.id+'/comments', commentParams)
            // update online_id in offline storage for this object
            await Comment.$updateLocally({
              where: comment.id,
              data: {
                online_id: commentResponse.data.comment.id
              }
            })
            // insert new Onlien Media
            Comment.$createLocally({ data: commentResponse.data.comment })
              .then( () => {
                // console.log("Insert new Comment (online)")
              })

          }

          // Upload AuditObject.Recommendations
          counts['recommendations'] += object.recommendations.length
          console.log("About to upload "+object.recommendations.length+" Recommendation")
          for (const recommendation of object.recommendations) {
            console.log("About to upload Recommendation: ")
            let recommendationParams = {
              text: recommendation.text,
              created_at: recommendation.created_at,
              updated_at: recommendation.updated_at
            }
            const recommendationResponse = await axios.post('/api/v1/object/'+objectResponse.data.object.id+'/recommendations', recommendationParams)
            // update online_id in offline storage for this object
            await Recommendation.$updateLocally({
              where: recommendation.id,
              data: {
                online_id: recommendationResponse.data.recommendation.id
              }
            })
            // insert new Onlien Media
            Recommendation.$createLocally({ data: recommendationResponse.data.recommendation })
              .then( () => {
                console.log("Insert new Recommendation (online)")
              })

          }

          // Upload AuditObject.Risk
          counts['risks'] += object.risks.length
          // console.log("About to upload "+object.risks.length+" Risks")
          for (const risk of object.risks) {
            // console.log("About to upload Risk: ")
            let riskParams = {
              audit_object_id: objectResponse.data.object.id,
              title: risk.title, // not required at JS level
              likehood: risk.likehood,
              consequence: risk.consequence,
              exposure: risk.exposure,
              score: risk.score,
              updated_at: risk.updated_at,
              created_at: risk.created_at
            }

            const riskResponse = await axios.post('/api/v1/risks', riskParams)
            // update online_id in offline storage for this object
            await Risk.$updateLocally({
              where: risk.id,
              data: {
                online_id: riskResponse.data.risk.id
              }
            })
            // insert new Onlien Media
            Risk.$createLocally({ data: riskResponse.data.risk })
              .then( () => {
                // console.log("Insert new Risk (online)")
              })

          }
          // console.log("Object done")
        } // end of objects foreach
        // console.log("Finish to post to API ")
        // sync hazard relationship
        for (const id of offlineIds) {
          let parentObject = AuditObject.query().with('hazards').with('signs').find(id)
          for (const hazard of parentObject.hazards) {
            hazardPivots.push(hazard.pivot.$id)
            const hazardResponse = await axios.post('/api/v1/object/' + parentObject.online_id + '/hazard/' + hazard.online_id)
            AuditObjectHazard.$createLocally({ data: [{
              audit_object_id: hazardResponse.data.object.id,
              hazard_object_id: hazardResponse.data.hazard.id,
            }]})
          }
          for (const sign of parentObject.signs) {
            signPivots.push(sign.pivot.$id)
            const signResponse = await axios.post('/api/v1/object/' + parentObject.online_id + '/sign/' + sign.online_id)
            AuditObjectSign.$createLocally({ data: [{
              audit_object_id: signResponse.data.object.id,
              sign_object_id: signResponse.data.sign.id,
            }]})
          }
        }
      } catch (error) {
        console.error(error); // catches both errors
      }

      // console.log("Audit done")
      // mark offlien audit uplaod as done
      await Audit.$updateLocally({
        where: audit.id,
        data: {
          uploading: false,
          status: -1
        }
      })

      // check that everythign has been uplodade fine?
      let onlineAudit = Audit.query()
        .with('assessment')
        .with('beach')
        .with('user')
        .with('objects')
        .with('comments')
        .with('recommendations')
        .with('risks')
        .with('media')
        .find(audit.online_id)

      if(onlineAudit.comments.length == counts.comments &&
        onlineAudit.recommendations.length == counts.recommendations &&
        onlineAudit.media.length == counts.media &&
        onlineAudit.objects.length == counts.objects &&
        onlineAudit.risks.length == counts.risks
      ) {
        // console.log('All matching. deleteing offline', audit.id)
        // delete offline data
        // comment
        await Comment.$deleteFromLocal((comment) => {
          return comment.audit_id == audit.id
        })
        // recommendation
        await Recommendation.$deleteFromLocal((recommendation) => {
          return recommendation.audit_id == audit.id
        })
        //  media
        await Media.$deleteFromLocal((media) => {
          return media.audit_id == audit.id
        })
        // risks
        await Risk.$deleteFromLocal((risk) => {
          return risk.audit_id == audit.id
        })
        // objecct
        await AuditObject.$deleteFromLocal((object) => {
          return object.audit_id == audit.id
        })
        // Hazard relationship
        await AuditObjectHazard.$deleteFromLocal((pivot) => {
          return hazardPivots.includes(pivot.$id)
        })
        // Sign relationship
        await AuditObjectSign.$deleteFromLocal((pivot) => {
          return signPivots.includes(pivot.$id)
        })
        // audit
        await Audit.$deleteFromLocal((item) => {
          return item.id == audit.id
        })

        // console.log('Offline Audit deleted')

      } else {
        console.error('Offline and Online not matching :(', counts)
      }

    } // end of audits forEach

    // Vue.$log.debug('System - uploadData finish');
    context.dispatch('setUploading', false)

  },
  goOnline: (context) => {
    context.commit('setAppOnline', true)
  },
  goOffline: (context) => {
    context.commit('setAppOffline', true)
  },
  setLoading: (context, value) => {
    // Vue.$log.debug('System - setLoading', value);
    context.commit('setAppLoading', value)
  },
  setUploading: (context, value) => {
    // Vue.$log.debug('System - setUploading', value);
    context.commit('setAppUpoading', value)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
