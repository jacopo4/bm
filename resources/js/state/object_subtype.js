const state = {
  signs: [
    { value: 379, text: "Individual Regulation Sign" },
    { value: 380, text: "Defined Access" },
    { value: 381, text: "Open Access" },
    { value: 382, text: "Road Sign" },
    { value: 383, text: "Beach Flags" },
    { value: 384, text: "Car Park Sign" },
    { value: 385, text: "Individual Hazard" }
  ],
  userGroups: [
    { value: 1, text: "Elderly" },
    { value: 2, text: "Youth" },
    { value: 3, text: "Disabled" }
  ],
  accessGradient: [
    { value: 1, text: "Steep" },
    { value: 2, text: "Very steep" },
    { value: 3, text: "None" },
    { value: 4, text: "Other" },
    { value: 5, text: "Not applicable" },
    { value: 6, text: "Low" },
    { value: 7, text: "Moderate" },
    { value: 8, text: "Flat" },
  ],
  accessSurface: [
    { value: 1, text: "Other" },
    { value: 2, text: "Rough" },
    { value: 3, text: "Not applicable" },
    { value: 4, text: "Sealed" },
    { value: 5, text: "Gravel/formed" },
    { value: 6, text: "Dirt/sand" },
    { value: 7, text: "None" },
  ],
  accessWidth: [
    { value: 1, text: "1.5m to 2m" },
    { value: 2, text: "1m to 1.5m" },
    { value: 3, text: "greater than 2m" },
    { value: 4, text: "less than 1m" },
    { value: 5, text: "about 6 m" },
  ],
  accessCondition: [
    { value: 1, text: "Good" },
    { value: 2, text: "Excellent" },
    { value: 3, text: "Public safety concerns" },
    { value: 4, text: "No access" },
    { value: 5, text: "Poor" },
    { value: 6, text: "Very good" },
    { value: 7, text: "Fair" },
  ],
  symbols: [
    { value: 268, text: "Camping Prohibited" },
    { value: 269, text: "No Diving" },
    { value: 270, text: "Bicycles Allowed" },
    { value: 271, text: "Shared Footway" },
    { value: 272, text: "No Running" },
    { value: 273, text: "No Naked Flames" },
    { value: 274, text: "No Model Planes" },
    { value: 275, text: "No Littering" },
    { value: 276, text: "Dogs Allowed off Leash" },
    { value: 277, text: "No Jumping" },
    { value: 278, text: "No Smoking" },
    { value: 279, text: "No Snorkelling" },
    { value: 280, text: "No Spear Fishing" },
    { value: 281, text: "No Swimming" },
    { value: 282, text: "No Trailbikes" },
    { value: 283, text: "No Vehicles" },
    { value: 284, text: "Picking Plants Prohibited" },
    { value: 285, text: "Pushing Prohibited" },
    { value: 286, text: "PWC's Prohibited" },
    { value: 287, text: "Sailboards Prohibited" },
    { value: 288, text: "Scuba Diving Prohibited" },
    { value: 289, text: "No Skateboards" },
    { value: 290, text: "Shooting Prohibited" },
    { value: 291, text: "Surfcraft Prohibited" },
    { value: 292, text: "No Parking" },
    { value: 293, text: "No Horses" },
    { value: 294, text: "Vessels Prohibited" },
    { value: 295, text: "No Removing Shellfish" },
    { value: 296, text: "Bombing Prohibited" },
    { value: 297, text: "No Golf" },
    { value: 298, text: "Fires Prohibited" },
    { value: 299, text: "Dog Litter Must be Picked Up" },
    { value: 300, text: "Dogs Allowed" },
    { value: 301, text: "Dogs Must be on a Leash" },
    { value: 302, text: "Do Not Drink the Water" },
    { value: 303, text: "Other" },
    { value: 304, text: "Water Skiing Prohibited" },
    { value: 305, text: "No Alcohol" },
    { value: 306, text: "No Bike Riding" },
    { value: 307, text: "No Bicycles" },
    { value: 308, text: "No Cats or Dogs" },
    { value: 309, text: "Bodyboards Prohibited" },
    { value: 310, text: "No Dogs Allowed" },
    { value: 311, text: "No Entry" },
    { value: 312, text: "No Fishing" },
    { value: 313, text: "No Firearms" },
    { value: 314, text: "No Glass" },
    { value: 315, text: "No Food" }
  ],
  classifications: [
    { value: 1, text: "Prohibition" },
    { value: 2, text: "Safety" },
    { value: 3, text: "Information" },
  ],
}

const mutations = {
}

const getters = {
}

const actions = {

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
