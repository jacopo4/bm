import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";
import vuexLocalForage from '@/database'

// modules
import auth from '@/state/auth'
import system from '@/state/system'
import loading from '@/state/loading'
import location from '@/state/location'
import assessment from '@/state/assessment'
import beach from '@/state/beach'
import audit from '@/state/audit'
import media from '@/state/media'
import audit_object from '@/state/audit_object'

// extras
// import 'babel-polyfill'
// require('moment/locale/en-au')

Vue.use(Vuex)

const vuexPersisted = new createPersistedState({
  key:'slsa-bm-v1',
  storage: window.localStorage,
  reducer: state => ({
    auth: state.auth,
    system: state.system,
    loading: state.loading,
    location: state.location
  })
})

export default new Vuex.Store ({
  modules: {
    auth,
    system,
    location,
    loading,
    assessment,
    audit,
    audit_object,
    beach,
    media
  },
  plugins: [vuexLocalForage, vuexPersisted],
})
