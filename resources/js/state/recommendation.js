import Recommendation from '@/models/recommendation'
import moment from 'moment';

const state = {
}

const mutations = {
}

const getters = {
}

const actions = {
  submitRecommendation(context, payload) {
    var params = {
      text: payload.text,
      assessment_id: payload.assessment_id,
      beach_id: payload.beach_id,
      audit_id: payload.audit_id,
      audit_object_id: payload.audit_object_id,
      object_type_id: payload.type_id,
      user_id: payload.user_id,
      recommendable_type: payload.recommendable_type,
      recommendable_id: payload.recommendable_id,
      updated_at: moment().format(),
      created_at: moment().format()
    };

    // Offline
    // Audit status == 0 means
    if (payload.status === 0) {

      return new Promise((resolve, reject) => {
        Recommendation.$createLocally({ data: params })
          .then( (response) => {
            resolve({ data: { recommendation: response[0]} })
          })
          .catch( (error) => {
            reject(error)
          })
      })

    } else {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/object/' + payload.audit_object_id + '/recommendations', params)
          .then(function (response) {
            Recommendation.$createLocally({ data: response.data.recommendation })
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }
  },
  updateRecommendation(context, payload) {
    var params = {
      id: payload.id,
      text: payload.text,
      updated_at: moment().format()
    };


    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        Recommendation.$updateLocally({
          where: payload.id,
          data: params
        })
          .then( () => {
            resolve({ data: { recommendation: params} })
          })
          .catch( (error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/recommendations/' + payload.id, params)
          .then(function (response) {
            Recommendation.$updateLocally({
              where: payload.id,
              data: response.data.recommendation
            })
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }
  },
  deleteRecommendation(context, payload) {
    var params = {
      recommendationId: payload.recommendationId
    };

    // Offline
    // Audit status == 0 means
    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        Recommendation.$deleteFromLocal(params.recommendationId)
          .then( () => {
            resolve({ data: { recommendation: params} })
          })
          .catch( (error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.delete('/api/v1/object/' + payload.auditId + '/recommendations/' + payload.recommendationId + '/delete', params)
          .then(function (response) {
            Recommendation.$deleteFromLocal(response.data.recommendationId)
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }

  },
  fetchRecommendations(context, payload) {
    context.commit('loading/setLoading', { 'collection': 'recommendations', 'status': 'loading' }, { root: true })
    var params = { payload };
    return new Promise((resolve, reject) => {
      axios.get('/api/v1/recommendations', params)
        .then(function (response) {
          context.commit('loading/setLoading', { 'collection': 'recommendations', 'status': 'loaded' }, { root: true })
          resolve(response)
        })
        .catch(function (error) {
          context.commit('loading/setLoading', { 'collection': 'recommendations', 'status': 'error', 'error': error }, { root: true })
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
