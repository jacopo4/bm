const state = {
  roles: [
    { value: 1, text: "Admin" },
    { value: 2, text: "Auditor" },
    { value: 3, text: "Client" }
  ]
}

const mutations = {
}

const getters = {
}

const actions = {
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
