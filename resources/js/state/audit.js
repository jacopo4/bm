import Audit from '@/models/audit'
import moment from 'moment'
moment.locale('en-AU')

const state = {
  rating: [
    { value: 1, text: "1" },
    { value: 2, text: "2" },
    { value: 3, text: "3" },
    { value: 4, text: "4" },
    { value: 5, text: "5" },
    { value: 6, text: "6" },
    { value: 7, text: "7" },
    { value: 8, text: "8" },
    { value: 9, text: "9" },
    { value: 10, text: "10" }
  ],
  population_rate: [
    { value: 1, text: "< 50 residents and/or < 200 non residents (domestic or overseas tourists)" },
    { value: 2, text: "50 - 250 residents and/or 21-100 non residents (domestic or overseas tourists)" },
    { value: 3, text: "250 - 1000 residents and/or 100-200 non residents (domestic or overseas tourists)" },
    { value: 4, text: "1000 - 2500 residents and/or 500-1000 non residents (domestic or overseas tourists)" },
    { value: 5, text: "2500+ residents and/or 1000 non residents (domestic or overseas tourists)" }
  ],
  human_activity_beach_conflict: [
    { value: 1, text: "No conflicts reported" },
    { value: 2, text: "Isolated conflicts" },
    { value: 3, text: "Regular" },
    { value: 4, text: "Persistent" },
    { value: 5, text: "Persistent and dangerous" }
  ],
  human_activity_beach: [
    { value: 1, text: "1-250" },
    { value: 2, text: "250-500" },
    { value: 3, text: "500-750" },
    { value: 4, text: "750-1000" },
    { value: 5, text: "1000+" }
  ],
  human_activity_water_conflict: [
    { value: 1, text: "No conflicts reported" },
    { value: 2, text: "Isolated conflicts" },
    { value: 3, text: "Regular" },
    { value: 4, text: "Persistent" },
    { value: 5, text: "Persistent and dangerous" }
  ],
  human_activity_water: [
    { value: 1, text: "1-25" },
    { value: 2, text: "25-50" },
    { value: 3, text: "50-75" },
    { value: 4, text: "75-100" },
    { value: 5, text: "100+" }
  ],
  fvr_development: [
    { value: 1, text: "Beach Rating 1 and 2" },
    { value: 2, text: "Beach Rating 3 and 4" },
    { value: 3, text: "Beach Rating 5 and 6" },
    { value: 4, text: "Beach Rating 7 and 8" },
    { value: 5, text: "Beach Rating 9 and 10" }
  ],
  fvr_frequency: [
    { value: 1, text: "An annual activity or event in held at the facility" },
    { value: 2, text: "An activity event takes place in the facility on a monthly basis" },
    { value: 3, text: "An activity event takes place in the facility on a weekly basis" },
    { value: 4, text: "An activity event takes place in the facility on a monthly basis" },
    { value: 5, text: "The facility is in continuous use for the majority of the day" }
  ],
  fvr_population: [
    { value: 1, text: "Greater than 500 people at a time" },
    { value: 2, text: "100 to 500 people at a time" },
    { value: 3, text: "50 to 100 people at a time" },
    { value: 4, text: "5 to 50 people at a time" },
    { value: 5, text: "Less than 5 people at a time" }
  ],
  status: [
    { value: 0, text: "Offline" },
    { value: 1, text: "Online" },
    { value: 2, text: "Consult" },
    { value: 3, text: "Final" }
  ]
}

const mutations = {
}

const getters = {
}

const actions = {
  submitAudit(context, payload) {
    var params = {
      assessment_id: payload.assessment_id,
      title: payload.title,
      date_visit: payload.date_visit,
      summary: payload.summary,
      status: payload.status,
      beach_id: payload.beach_id,
      beach_hazard_rating: payload.beach_hazard_rating,
      beach_type: payload.beach_type,
      beach_fvr_development: payload.beach_fvr_development,
      beach_fvr_population: payload.beach_fvr_population,
      beach_fvr_frequency: payload.beach_fvr_frequency,
      beach_local_population: payload.beach_local_population,
      beach_human_water: payload.beach_human_water,
      beach_human_water_conflict: payload.beach_human_water_conflict,
      beach_human_beach: payload.beach_human_beach,
      beach_human_beach_conflict: payload.beach_human_beach_conflict,
      created_at: moment().format(),
      updated_at: moment().format(),
    };

    // Offline
    // Audit status == 0 means
    if (params.status === 0) {

      return new Promise((resolve, reject) => {

        Audit.$createLocally({ data: params })
          .then( (response) => {
            resolve({ data: { audit: response[0]} })
          })
          .catch( (error) => {
            reject(error)
          })
      })

    } else {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/audit', params)
          .then(function (response) {
            Audit.$createLocally({ data: response.data.audit })
              .then( () => {
                resolve({ data: { audit: response.data.audit} })
              })
              .catch( (error) => {
                reject(error)
              })
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }
  },
  updateAudit(context, payload) {
    var params = {
      title: payload.title,
      date_visit: payload.date_visit,
      summary: payload.summary,
      status: payload.status,
      beach_hazard_rating: payload.beach_hazard_rating,
      beach_type: payload.beach_type,
      beach_fvr_development: payload.beach_fvr_development,
      beach_fvr_population: payload.beach_fvr_population,
      beach_fvr_frequency: payload.beach_fvr_frequency,
      beach_local_population: payload.beach_local_population,
      beach_human_water: payload.beach_human_water,
      beach_human_water_conflict: payload.beach_human_water_conflict,
      beach_human_beach: payload.beach_human_beach,
      beach_human_beach_conflict: payload.beach_human_beach_conflict,
    };

    if (payload.status === 0) {

      return new Promise((resolve, reject) => {
        Audit.$updateLocally({ where: payload.id, data: params })
          .then( () => {
            resolve({ data: { audit: params} })
          })
          .catch( (error) => {
            reject(error)
          })
      })

    } else {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/audits/' + payload.id, params)
          .then(function (response) {
            Audit.$updateLocally({ where: payload.id, data: response.data.audit })
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }
  },
  fetchAudits(context, payload) {
    context.commit('loading/setLoading', { 'collection': 'audit', 'status': 'loading' }, { root: true })
    var params = { payload };
    return new Promise((resolve, reject) => {
      axios.get('/api/v1/audits', params)
        .then(function (response) {
          context.commit('loading/setLoading', { 'collection': 'audit', 'status': 'loaded' }, { root: true })
          resolve(response)
        })
        .catch(function (error) {
          context.commit('loading/setLoading', { 'collection': 'audit', 'status': 'error', 'error': error }, { root: true })
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
