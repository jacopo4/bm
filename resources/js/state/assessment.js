import User from '@/models/user'
import AssessmentUser from '@/models/assessment_user'

const state = {
  status: [
    { value: 1, text: "Draft" },
    { value: 2, text: "Closed" }
  ],
  types: [
    { value: 1, text: "Standard" }
  ]
}

const mutations = {
}

const getters = {
}

const actions = {
  submitAssessment(context, payload) {
    var params = {
      title: payload.title,
      type: payload.type,
      summary: payload.summary,
      area: payload.area,
      status: payload.status,
      start_date: payload.start_date,
      end_date: payload.end_date,
      delivery_date: payload.delivery_date
    };

    return new Promise((resolve, reject) => {
      axios.post('/api/v1/assessment', params)
        .then(function (response) {
          resolve(response)
        })
        .catch(function (error) {
          reject(error)
        })
    })
  },
  updateAssessment(context, payload) {
    var params = {
      title: payload.title,
      type: payload.type,
      summary: payload.summary,
      area: payload.area,
      status: payload.status,
      start_date: payload.start_date,
      end_date: payload.end_date,
      delivery_date: payload.delivery_date,
    };

    return new Promise((resolve, reject) => {
      axios.post('/api/v1/assessments/' + payload.id, params)
        .then(function (response) {
          resolve(response)
        })
        .catch(function (error) {
          reject(error)
        })
    })
  },
  saveMember(context, payload) {
    var params = {
      user_id: payload.user_id,
      role_id: payload.role_id
    };

    return new Promise((resolve, reject) => {
      axios.post('/api/v1/assessments/' + payload.assessment_id + '/members/' + payload.user_id, params).then((response) => {
        if (!response.data.errors) {
          response.data.assessment.users.forEach((user) => {
            User.insertOrUpdate({ data: user })
            AssessmentUser.insertOrUpdate({ data: [{
              assessment_id: payload.assessment_id,
              role_id: user.pivot.role_id,
              user_id: user.id
            }]})
          })
        }
        resolve(response)
      }).catch((error) => {
        // console.log('error saving user', error)
        reject(error)
      })
    })
  },
  fetchAssessments(context, payload) {
    context.commit('loading/setLoading', { 'collection': 'assessment', 'status': 'loading' }, { root: true })
    var params = { payload };
    return new Promise((resolve, reject) => {
      axios.get('/api/v1/assessments', params)
        .then(function (response) {
          context.commit('loading/setLoading', { 'collection': 'assessment', 'status': 'loaded' }, { root: true })
          resolve(response)
        })
        .catch(function (error) {
          context.commit('loading/setLoading', { 'collection': 'assessment', 'status': 'error', 'error': error }, { root: true })
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
