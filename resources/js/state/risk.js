import Risk from '@/models/risk'
import moment from 'moment'
moment.locale('en-AU')

const state = {
  likehood: [
    { value: 0.5, text: "DRAFT" },
    { value: 1, text: "PRE-SITE" },
    { value: 3, text: "ON-SITE" },
    { value: 6, text: "CONSULT" },
    { value: 10, text: "FINAL" }
  ],
  consequence: [
    { value: 1, text: "DRAFT" },
    { value: 2, text: "PRE-SITE" },
    { value: 5, text: "ON-SITE" },
    { value: 10, text: "CONSULT" },
    { value: 20, text: "FINAL" }
  ],
  exposure: [
    { value: 1, text: "DRAFT" },
    { value: 2, text: "PRE-SITE" },
    { value: 5, text: "ON-SITE" },
    { value: 10, text: "CONSULT" },
    { value: 20, text: "FINAL" }
  ]
}

const mutations = {
}

const getters = {
}

const actions = {

  createRisk(context, payload) {
    var params = {
      assessment_id: payload.assessment_id,
      audit_id: payload.audit_id,
      audit_object_id: payload.audit_object_id,
      object_type_id: payload.object_type_id,

      user_id: payload.user_id,
      beach_id: payload.beach_id,

      title: payload.title,
      likehood: payload.likehood,
      consequence: payload.consequence,
      exposure: payload.exposure,
      score: payload.score,

      created_at: moment().format(),
      updated_at: moment().format()
    };

    if (payload.audit_status === 0) {
      return new Promise((resolve, reject) => {
        Risk.$createLocally({ data: params })
          .then( (response) => {
            resolve({ data: { object: response[0]} })
          })
          .catch( (error) => {
            reject(error)
          })
      })

    } else {

      return new Promise((resolve, reject) => {
        axios.post('/api/v1/risks', params)
          .then(function (response) {
            Risk.$createLocally({ data: response.data.risk })
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }
  },

  updateRisk(context, payload) {
    var params = {
      title: payload.title,
      likehood: payload.likehood,
      consequence: payload.consequence,
      exposure: payload.exposure,
      score: payload.score,
      updated_at: moment().format()
    };


    if (payload.audit_status === 0) {
      return new Promise((resolve, reject) => {
        Risk.$updateLocally({ where: payload.id, data: params })
          .then( () => {
            resolve({ data: { object: params} })
          })
          .catch( (error) => {
            reject(error)
          })
      })

    } else {

      return new Promise((resolve, reject) => {
        axios.post('/api/v1/risks/' + payload.id, params)
          .then(function (response) {
            Risk.$updateLocally({where: payload.id, data: response.data.risk })
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }

  },
  deleteRisk(context, payload) {
    var params = {
      riskId: payload.riskId
    };

    // Offline
    // Audit status == 0 means
    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        Risk.$deleteFromLocal(params.riskId)
          .then( () => {
            resolve({ data: { risk: params} })
          })
          .catch( (error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.delete('/api/v1/risks/' + payload.riskId + '/delete', params)
          .then(function (response) {
            Risk.$deleteFromLocal(response.data.riskId)
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }
  },
  fetchRisks(context, payload) {
    context.commit('loading/setLoading', { 'collection': 'risks', 'status': 'loading' }, { root: true })
    var params = { payload };
    return new Promise((resolve, reject) => {
      axios.get('/api/v1/risks', params)
        .then(function (response) {
          context.commit('loading/setLoading', { 'collection': 'risks', 'status': 'loaded' }, { root: true })
          resolve(response)
        })
        .catch(function (error) {
          context.commit('loading/setLoading', { 'collection': 'risks', 'status': 'error', 'error': error }, { root: true })
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
