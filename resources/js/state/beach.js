const state = {
  types: [
    { value: 1, text: "Marine Beach" },
    { value: 2, text: "Harbour/Bay" },
    { value: 3, text: "Fluvial/River Beach" },
    { value: 4, text: "Artificial Beach" },
    { value: 5, text: "Rock Platform" },
    { value: 6, text: "International" },
    { value: 7, text: "Inland Waterway" }
  ],
}

const mutations = {
}

const getters = {
  beachesOptions: (state) => {
    console.log(state);
    console.log(state.data);
    var formatedOptions = [];
    if (Object.keys(state.data).length > 0) {
      _.each(state.data, (beach) => {
        let option = {};
        option.value = beach.id;
        option.text =
            beach.title +
            " (" +
            beach.state +
            ") - " +
            Math.round(beach.distance * Math.pow(10, 2)) / Math.pow(10, 2) +
            " km";
        formatedOptions.push(option);
      });
    }
    return formatedOptions;
  }
}

const actions = {
  fetchBeaches(context, payload) {
    context.commit('loading/setLoading', { 'collection': 'beaches', 'status': 'loading' }, { root: true })
    var params = {
      payload
    };
    return new Promise((resolve, reject) => {
      axios.get('/api/v1/beaches', params)
        .then(function (response) {
          context.commit('loading/setLoading', { 'collection': 'beaches', 'status': 'loaded' }, { root: true })
          resolve(response)
        })
        .catch(function (error) {
          // console.log('fetch beaches', error)
          context.commit('loading/setLoading', { 'collection': 'beaches', 'status': 'error' }, { root: true })
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
