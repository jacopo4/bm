import AuditObject from '@/models/audit_object'
import AuditObjectSign from '@/models/audit_object_sign'

const state = {
}

const mutations = {
}

const getters = {
}

const actions = {

  createSign(context, payload) {
    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        let params = {
          type_id: 28,
          subtype: payload.subtype,
          title: payload.object.title + ' ' + payload.subtype.title + ' sign',
          assessment_id: payload.object.assessment_id,
          audit_id: payload.object.audit_id,
          audit_status: 0
        }

        context.dispatch('audit_object/submitAuditObject', params, {root:true}).then(async (data) => {

          if (payload.object.latitude) {
            params = {
              latitude: payload.object.latitude,
              longitude: payload.object.longitude,
              audit_status: 0
            }

            await context.dispatch('audit_object/updateLocation', params, {root:true})
          }

          await AuditObjectSign.$createLocally({ data: [{
            audit_object_id: payload.object.id,
            sign_object_id: data.data.object.id,
          }]})


          resolve({ data: { object: payload.object, sign: data.data.object, errors: null} })
        }).catch((error) => {
          reject(error)
        })
      })
    } else {
      let params = {
        id: payload.object.id,
        subtype_id: payload.subtype.id,
      };

      return new Promise((resolve, reject) => {
        axios.post('/api/v1/object/' + payload.object.id + '/sign/create', params)
          .then((response) => {
            AuditObject.$createLocally({ data: response.data.sign })
            AuditObjectSign.$createLocally({ data: [{
              audit_object_id: response.data.object.id,
              sign_object_id: response.data.sign.id,
            }]})
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    }
  },

  removeSign(context, payload) {
    let params = {
      object_id: payload.audit_object.id,
      sign_id: payload.sign_object.id
    }

    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        AuditObjectSign.$deleteFromLocal([
          params.object_id,
          params.sign_id
        ])
          .then(() => {
            resolve({ data: { object: payload.audit_object, sign: payload.sign_object, errors: null} })
          })
          .catch((error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.delete('/api/v1/object/' + params.object_id + '/sign/' + params.sign_id)
          .then((response) => {
            AuditObjectSign.$deleteFromLocal([
              response.data.object.id,
              response.data.sign.id
            ])
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    }
  },

  attachSign(context, payload) {
    let params = {
      object_id: payload.audit_object.id,
      sign_id: payload.sign_object.id
    }

    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        AuditObjectSign.$createLocally({ data: [{
          audit_object_id: params.object_id,
          sign_object_id: params.sign_id,
        }]})
          .then(() => {
            resolve({ data: { object: payload.audit_object, sign: payload.sign_object, errors: null} })
          })
          .catch((error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/object/' + params.object_id + '/sign/' + params.sign_id)
          .then((response) => {
            AuditObjectSign.$createLocally({ data: [{
              audit_object_id: response.data.object.id,
              sign_object_id: response.data.sign.id,
            }]})
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    }
  },

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
