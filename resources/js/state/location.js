//import axios from 'axios'
import Beach from '@/models/beach';

const state = {
  // Related to Geolocation
  location: {
    'lat': null,
    'lng': null,
    'error': null
  },
  place: {},
  weather: {}
}

const actions = {

  // Geolocation
  fetchLocationDetails: (context) => {
    if (context.state && context.state.location && context.state.location.lng) {
      context.commit('loading/setLoading', { 'collection': 'location', 'status': 'loading' }, { root: true })
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/myLocation', {
          latitude: context.state.location.lat,
          longitude: context.state.location.lng
        })
          .then(function (response) {
            context.commit('setPlace', response.data.place)
            Beach.$createLocally({ data: response.data.beaches })
            context.commit('setWeather', response.data.weather)
            context.commit('loading/setLoading', { 'collection': 'location', 'status': 'loaded' }, { root: true })
            resolve(response)
          })
          .catch(function (error) {
            context.commit('loading/setLoading', { 'collection': 'location', 'status': 'error', 'error': error }, { root: true })
            reject(error)
          })
      })
    }
  }
}

const mutations = {
  setLocation (state, location) {
    state.location = {
      'lat': location.lat ? location.lat * 1 : null,
      'lng': location.lng ? location.lng * 1 : null,
      'error': location.error ? location.error : null
    }
  },
  setPlace (state, place) {
    state.place = place
  },
  setWeather (state, weather) {
    state.weather = weather
  }
}

const getters = {
  have_location: (state) => {
    if (state.location.lat == null || state.location.lng == null) {
      return false
    }
    return true
  },
  have_location_error: (state) => {
    if (state.location.error === null) {
      return false
    }
    return true
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
