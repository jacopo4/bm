const state = {
}

const mutations = {
}

const getters = {
}

const actions = {
  fetchTypes(context, payload) {
    context.commit('loading/setLoading', { 'collection': 'object_type', 'status': 'loading' }, { root: true })
    var params = { payload };
    return new Promise((resolve, reject) => {
      axios.get('/api/v1/audits/types', params)
        .then(function (response) {
          context.commit('loading/setLoading', { 'collection': 'object_type', 'status': 'loaded' }, { root: true })
          resolve(response)
        })
        .catch(function (error) {
          context.commit('loading/setLoading', { 'collection': 'object_type', 'status': 'error', 'error': error }, { root: true })
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
