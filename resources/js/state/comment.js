import Comment from '@/models/comment'
import moment from 'moment';

const state = {
}

const mutations = {
}

const getters = {
}

const actions = {
  submitComment(context, payload) {
    var params = {
      text: payload.text,
      assessment_id: payload.assessment_id,
      beach_id: payload.beach_id,
      audit_id: payload.audit_id,
      audit_object_id: payload.audit_object_id,
      object_type_id: payload.type_id,
      user_id: payload.user_id,
      commentable_type: payload.commentable_type,
      commentable_id: payload.commentable_id,
      updated_at: moment().format(),
      created_at: moment().format()
    };

    // Offline
    // Audit status == 0 means
    if (payload.status === 0) {

      return new Promise((resolve, reject) => {
        Comment.$createLocally({ data: params })
          .then( (response) => {
            resolve({ data: { comment: response[0]} })
          })
          .catch( (error) => {
            reject(error)
          })
      })

    } else {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/object/' + payload.audit_object_id + '/comments', params)
          .then(function (response) {
            Comment.$createLocally({ data: response.data.comment })
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }
  },
  updateComment(context, payload) {
    var params = {
      id: payload.id,
      text: payload.text,
      updated_at: moment().format()
    };


    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        Comment.$updateLocally({ data: params })
          .then( () => {
            resolve({ data: { comment: params} })
          })
          .catch( (error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/comments/' + payload.id, params)
          .then(function (response) {
            Comment.$updateLocally({ data: response.data.comment })
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }
  },
  deleteComment(context, payload) {
    var params = {
      commentId: payload.commentId
    };

    // Offline
    // Audit status == 0 means
    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        Comment.$deleteFromLocal(params.commentId)
          .then( () => {
            resolve({ data: { comment: params} })
          })
          .catch( (error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.delete('/api/v1/object/' + payload.auditId + '/comments/' + payload.commentId + '/delete', params)
          .then(function (response) {
            Comment.$deleteFromLocal(response.data.commentId)
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }

  },
  fetchComments(context, payload) {
    context.commit('loading/setLoading', { 'collection': 'comments', 'status': 'loading' }, { root: true })
    var params = { payload };
    return new Promise((resolve, reject) => {
      axios.get('/api/v1/comments', params)
        .then(function (response) {
          context.commit('loading/setLoading', { 'collection': 'comments', 'status': 'loaded' }, { root: true })
          resolve(response)
        })
        .catch(function (error) {
          context.commit('loading/setLoading', { 'collection': 'comments', 'status': 'error', 'error': error }, { root: true })
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
