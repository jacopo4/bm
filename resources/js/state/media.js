import Media from '@/models/media'
import moment from 'moment'
moment.locale('en-AU')

const spinner = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiBzdHlsZT0ibWFyZ2luOiBhdXRvOyBiYWNrZ3JvdW5kOiBub25lOyBkaXNwbGF5OiBibG9jazsgc2hhcGUtcmVuZGVyaW5nOiBhdXRvOyIgd2lkdGg9IjI3NHB4IiBoZWlnaHQ9IjI3NHB4IiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQiPgo8ZyB0cmFuc2Zvcm09InJvdGF0ZSgxMjMuNzk1IDUwIDUwKSI+CiAgPHBhdGggZD0iTTUwIDE1QTM1IDM1IDAgMSAwIDc0Ljc0ODczNzM0MTUyOTE2IDI1LjI1MTI2MjY1ODQ3MDg0MyIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMDA2MGE5IiBzdHJva2Utd2lkdGg9IjEyIj48L3BhdGg+CiAgPHBhdGggZD0iTTQ5IDNMNDkgMjdMNjEgMTVMNDkgMyIgZmlsbD0iIzAwNjBhOSI+PC9wYXRoPgogIDxhbmltYXRlVHJhbnNmb3JtIGF0dHJpYnV0ZU5hbWU9InRyYW5zZm9ybSIgdHlwZT0icm90YXRlIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgZHVyPSIzLjQ0ODI3NTg2MjA2ODk2NTdzIiB2YWx1ZXM9IjAgNTAgNTA7MzYwIDUwIDUwIiBrZXlUaW1lcz0iMDsxIj48L2FuaW1hdGVUcmFuc2Zvcm0+CjwvZz4KPCEtLSBbbGRpb10gZ2VuZXJhdGVkIGJ5IGh0dHBzOi8vbG9hZGluZy5pby8gLS0+PC9zdmc+"

const state = {
  
}

const mutations = {

}


const getters = {

}


export const actions = {
  // upload media via API to Laravel and replace in local DB
  uploadMedia (context, media) {
    return new Promise((resolve, reject) => {
      let config = {
        onUploadProgress: progressEvent => {
          let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total)
          // do whatever you like with the percentage complete
          // maybe dispatch an action that will update a progress bar or something
          if (percentCompleted == 100 ) {
            percentCompleted = "Working.."
          }
          Media.$updateLocally({
            where: media.id,
            data: { 
              progress: percentCompleted
            }
          })
        }
      }
      axios.post('/api/v1/media', media, config)
        .then(function (response) {
          // if 200 and we have media

          // remove from IndexDB
          Media.$deleteFromLocal(media.id).then(() => {
            
          }).catch(function (error) {
            reject(error)
          })

          // insert new one from API -> indexDB
          let stored_media = response.data.media
          Media.$createLocally({
            data: stored_media
          }).then((response) => {
            resolve(response[0])
          }).catch(function (error) {
            console.error('uploadMedia state createLocally  ERROR ',error)
            reject(error)
          })
          // done
        })
        .catch(function (error) {
          console.error('uploadMedia state ERROR',error)
          reject(error)
        })
    })
  },
  // create  media helper 
  createMedia: (context, file) => {
    if(file.audit_status === 1) {
      // send directlty to cloud and wait for URL
      // Vue.$log.debug('createMedia We are ONLINE')
      return new Promise((resolve, reject) => {
        const realData = file.url
        let fakeFile = file
        fakeFile.url = spinner
        //fakeFile.id = "temp-"+Math.random()*10
        context.dispatch('createMediaLocaly',fakeFile )
        // this shoudl return new row, but return LOTS OF row TODO ERROR
          .then((mediaLocal) => {
            // get the local version (via query) and trigeger upload 
            mediaLocal.url  = realData
            context.dispatch('uploadMedia', mediaLocal)
              .then((media) => {
                resolve(media)
              })
              .catch(function (error) {
                console.error('uploadMedia dummy  ERROR '+mediaLocal.name)
                reject(error)
              })
          }).catch(function (error) {
            console.error('createMediaLocaly dummy  ERROR '+fakeFile.name)
            reject(error)
          })
      })
    } else {
      // save localy now ONLY
      // Vue.$log.debug('createMedia We are OFFLINE')
      return new Promise((resolve, reject) => {
        context.dispatch('createMediaLocaly',file )
          .then((media) => {
            resolve(media)
          }).catch(function (error) {
            console.error('createMedia state  ERROR '+file.name)
            reject(error)
          })
      })
      
    }
  },
  // create  media helper 
  createMediaLocaly: (context, file) => {
    let params = file
    return new Promise((resolve, reject) => {
      Media.$createLocally({
        data: params
      }).then((response) => {
        // this shoudl return new row, but return LOTS OF row TODO
        // it somehow modified the object parameter! and has hte right id in it
        resolve(response[0])
      }).catch(function (error) {
        console.error('createMediaLocaly state  ERROR '+file.name)
        reject(error)
      })
    })
  },
  // update  media localy 
  updateMedia: (context, payload) => {
    let file = payload
    
    return new Promise((resolve, reject) => {
      Media.$updateLocally({
        where: file.id,
        data: { 
          updated_at: moment().format(),
          name: file.name,
          size: file.size,
          url: file.url
        }
      }).then((media) => {
        resolve(media)
      }).catch(function (error) {
        console.error('updateMedia state ERROR '+file.name)
        reject(error)
      })
    })
  },
  // delete media local or remotely
  deleteMedia: (context, media) => {
    
    return new Promise((resolve, reject) => {
      
      // check if we have internet here and delete remotly
      if(!media.is_offline && media.audit_status === 1) {
        // Vue.$log.debug('About to delete REMOTE media ');
        axios.delete('/api/v1/media/'+media.id)
          .then(function (response) {
            // if 200 and we have media
            resolve(response)
          // done
          })
          .catch(function (error) {
            console.error('deleteMedia REMOTE ERROR',error)
            reject(error)
          })
      }

      // Vue.$log.debug('About to delete LOCAL media');
      Media.$deleteFromLocal(media.id).then((deleted_media) => {
        resolve(deleted_media)
      }).catch(function (error) {
        console.error('deleteMedia state ERROR',error)
        reject(error)
      })

      
    })
  },
  fetchMedia(context, payload) {
    context.commit('loading/setLoading', { 'collection': 'media', 'status': 'loading' }, { root: true })
    var params = { payload };
    return new Promise((resolve, reject) => {
      axios.get('/api/v1/media', params)
        .then(function (response) {
          context.commit('loading/setLoading', { 'collection': 'media', 'status': 'loaded' }, { root: true })
          resolve(response)
        })
        .catch(function (error) {
          context.commit('loading/setLoading', { 'collection': 'media', 'status': 'error', 'error': error }, { root: true })
          reject(error)
        })
    })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
