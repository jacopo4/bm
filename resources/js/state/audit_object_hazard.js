import AuditObject from '@/models/audit_object'
import AuditObjectHazard from '@/models/audit_object_hazard'

const state = {
}

const mutations = {
}

const getters = {
}

const actions = {

  createHazard(context, payload) {
    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        let params = {
          type_id: 16,
          subtype: payload.subtype,
          title: payload.object.title + ' ' + payload.subtype.title + ' hazard',
          assessment_id: payload.object.assessment_id,
          audit_id: payload.object.audit_id,
          audit_status: 0
        }

        context.dispatch('audit_object/submitAuditObject', params, {root:true}).then(async (data) => {

          if (payload.object.latitude) {
            params = {
              latitude: payload.object.latitude,
              longitude: payload.object.longitude,
              audit_status: 0
            }

            await context.dispatch('audit_object/updateLocation', params, {root:true})
          }

          await AuditObjectHazard.$createLocally({ data: [{
            audit_object_id: payload.object.id,
            hazard_object_id: data.data.object.id,
          }]})


          resolve({ data: { object: payload.object, hazard: data.data.object, errors: null} })
        }).catch((error) => {
          reject(error)
        })
      })
    } else {
      let params = {
        id: payload.object.id,
        subtype_id: payload.subtype.id,
      };

      return new Promise((resolve, reject) => {
        axios.post('/api/v1/object/' + payload.object.id + '/hazard/create', params)
          .then((response) => {
            AuditObject.$createLocally({ data: response.data.hazard })
            AuditObjectHazard.$createLocally({ data: [{
              audit_object_id: response.data.object.id,
              hazard_object_id: response.data.hazard.id,
            }]})
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    }
  },

  removeHazard(context, payload) {
    let params = {
      object_id: payload.audit_object.id,
      hazard_id: payload.hazard_object.id
    }

    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        AuditObjectHazard.$deleteFromLocal([
          params.object_id,
          params.hazard_id
        ])
          .then(() => {
            resolve({ data: { object: payload.audit_object, hazard: payload.hazard_object, errors: null} })
          })
          .catch((error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.delete('/api/v1/object/' + params.object_id + '/hazard/' + params.hazard_id)
          .then((response) => {
            AuditObjectHazard.$deleteFromLocal([
              response.data.object.id,
              response.data.hazard.id
            ])
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    }
  },

  attachHazard(context, payload) {
    let params = {
      object_id: payload.audit_object.id,
      hazard_id: payload.hazard_object.id
    }

    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        AuditObjectHazard.$createLocally({ data: [{
          audit_object_id: params.object_id,
          hazard_object_id: params.hazard_id,
        }]})
          .then(() => {
            resolve({ data: { object: payload.audit_object, hazard: payload.hazard_object, errors: null} })
          })
          .catch((error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/object/' + params.object_id + '/hazard/' + params.hazard_id)
          .then((response) => {
            AuditObjectHazard.$createLocally({ data: [{
              audit_object_id: response.data.object.id,
              hazard_object_id: response.data.hazard.id,
            }]})
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    }
  },

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
