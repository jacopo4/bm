import AuditObject from '@/models/audit_object'
import moment from 'moment'
moment.locale('en-AU')

const state = {

  types: [{
    typeId: 6,
    icon: 'fa-road',
    label: 'Access'
  },{
    typeId: 1,
    icon: 'fa-volleyball-ball',
    label: 'Activities'
  },{
    typeId: 19,
    icon: 'fa-flag',
    label: 'Services'
  },{
    typeId: 13,
    icon: 'fa-siren-on',
    label: 'Support'
  },{
    typeId: 16,
    icon: 'fa-exclamation-triangle',
    label: 'Hazards'
  },{
    typeId: 12,
    icon: 'fa-life-ring',
    label: 'Equipments'
  },{
    typeId: 14,
    icon: 'fa-restroom',
    label: 'Facilities'
  },{
    typeId: 28,
    icon: 'fa-map-signs',
    label: 'Signages'
  }]

}

const mutations = {
}

const getters = {
}

const actions = {
  submitAuditObject(context, payload) {
    var params = {
      title: payload.title,
      type_id: payload.type_id,
      subtype_id: payload.subtype.id,
      audit_id: payload.audit_id,
      assessment_id: payload.assessment_id,
      created_at: moment().format(),
      updated_at: moment().format()
    };

    if (payload.audit_status === 0) {
      let metadata = []
      if (payload.subtype.metadata_type == 'ReefType') { // Reef
        metadata = {'width': null}
      } else if (payload.subtype.metadata_type == 'AccessType') { // Access
        metadata = {'width': null,'gradient': null, 'condition': null, 'surface': null}
      } else if (payload.subtype.metadata_type == 'RipType') { // Rips
        metadata = {'spacing': null}
      } else if (payload.subtype.metadata_type == 'EmergencyType') { // Emergency services
        metadata = {'distance': null, 'response_time': null }
      } else if (payload.subtype.metadata_type == 'LifeSavingType') { // Life saving services
        metadata = {'manager': null, 'provider': null }
      } else if (payload.subtype.metadata_type == 'HazardsType') { // Biological Hazards & Hazards
        metadata = {'user_groups': []}
      } else if (payload.subtype.metadata_type == 'SignageType') { // Signage
        metadata = {'symbols': []}
      } else if (payload.subtype.metadata_type == 'FacilityType') { // facilities
        metadata = {'manager': null, 'manager_phone': null, 'count': null, 'distance': null  }
      } else if (payload.subtype.metadata_type == 'CarParkType') { // Car parks
        metadata = {'manager': null, 'manager_phone': null, 'count': null, 'distance': null }
      }
      params['metadata'] = metadata
      return new Promise((resolve, reject) => {
        AuditObject.$createLocally({ data: params })
          .then( (response) => {
            resolve({ data: { object: response[0]} })
          })
          .catch( (error) => {
            reject(error)
          })
      })

    } else {

      return new Promise((resolve, reject) => {
        axios.post('/api/v1/object', params)
          .then(function (response) {
            AuditObject.$createLocally({ data: response.data.object })
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }
  },

  updateObject(context, payload) {
    var params = {
      id: payload.id,
      title: payload.title,
      metadata: payload.metadata
    };

    if (payload.status === 0) {
      return new Promise((resolve, reject) => {
        AuditObject.$updateLocally({ where: payload.id, data: params })
          .then(() => {
            resolve({ data: { object: params, errors: null} })
          })
          .catch((error) => {
            reject(error)
          })
      })
    } else {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/object/' + payload.id, params)
          .then((response) => {
            // console.log(response.data.object);
            AuditObject.$updateLocally({ where: payload.id, data: response.data.object })
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    }
  },

  updateLocation(context, payload) {
    var params = {
      latitude: payload.latitude,
      longitude: payload.longitude,
    };

    if (payload.audit_status === 0) {
      return new Promise((resolve, reject) => {
        AuditObject.$updateLocally({ where: payload.id, data: params })
          .then( () => {
            resolve({ data: { object: params} })
          })
          .catch( (error) => {
            reject(error)
          })
      })

    } else {

      return new Promise((resolve, reject) => {
        axios.post('/api/v1/object/' + payload.id, params)
          .then(function (response) {
            AuditObject.$updateLocally({where: payload.id, data: response.data.object })
            resolve(response)
          })
          .catch(function (error) {
            reject(error)
          })
      })
    }

  },

  fetchAuditsObjects(context, payload) {
    context.commit('loading/setLoading', { 'collection': 'audit', 'status': 'loading' }, { root: true })
    var params = { payload };
    return new Promise((resolve, reject) => {
      axios.get('/api/v1/audits/objects', params)
        .then(function (response) {
          context.commit('loading/setLoading', { 'collection': 'audit', 'status': 'loaded' }, { root: true })
          resolve(response)
        })
        .catch(function (error) {
          context.commit('loading/setLoading', { 'collection': 'audit', 'status': 'error', 'error': error }, { root: true })
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
