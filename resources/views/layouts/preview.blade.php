<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', '') }} {{ $title ?? '' }}</title>
  <base href="{{ route('assessments.export', $assessment) }}">
  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  {{--<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>--}}
   <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js" integrity="sha256-MAgcygDRahs+F/Nk5Vz387whB4kSK9NXlDN3w58LLq0=" crossorigin="anonymous"></script>
  @yield('style')
</head>

<script>
    // function subst() {
    //     var vars = {};
    //     var query_strings_from_url = document.location.search.substring(1).split('&');
    //     for (var query_string in query_strings_from_url) {
    //         if (query_strings_from_url.hasOwnProperty(query_string)) {
    //             var temp_var = query_strings_from_url[query_string].split('=', 2);
    //             vars[temp_var[0]] = decodeURI(temp_var[1]);
    //         }
    //     }
    //     var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages'];
    //     for (var css_class in css_selector_classes) {
    //         if (css_selector_classes.hasOwnProperty(css_class)) {
    //             var element = document.getElementsByClassName(css_selector_classes[css_class]);
    //             for (var j = 0; j < element.length; ++j) {
    //                 element[j].textContent = vars[css_selector_classes[css_class]];
    //             }
    //         }
    //     }
    // }
</script>
{{--data-spy="scroll" data-target="#myScrollspy" data-offset="20"--}}
<body style="padding-top:0 !important;">
  <div id="preview">
    <main>
      @yield('content')
    </main>
  </div>


  <!-- Scripts -->
  {{--<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
  integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
  crossorigin=""></script>--}}
  <script>
      window.objects = {!! $assessment->objects->toJson() !!}
  </script>
  @stack('scripts')
</body>

</html>
