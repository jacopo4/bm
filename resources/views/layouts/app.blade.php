<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <base href="{{config('app.url') }}"/>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="viewport-fit=cover, width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="format-detection" content="telephone=no"/>
    <!-- icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/apple-touch-icon.png')}}"/>
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/favicon-32x32.png')}}"/>
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/favicon-16x16.png')}}"/>
    <link rel="manifest" href="{{asset('/manifest.json')}}"/>
    <link rel="mask-icon" href="{{asset('/safari-pinned-tab.svg')}}" color="#5bbad5"/>
    <meta name="theme-color" content="#0060a9"/>
    <title>{{ config('app.name', 'SLSA Beach Management App') }}</title>
    <script>
        window.API_HOST = "{{ config('app.url') }}";
        window.APP_NAME = "{{ config('app.name') }}";
        window.APP_VERSION = "{{ config('app.version') }}";
        window.APP_ENV = "{{ config('app.env') }}";
        window.BUILDED_AT = "{{ config('app.builded_at') }}";
        window.BUGSNAG_API_KEY = "{{ config('bugsnag.api_key') }}";
        window.BUGSNAG_NOTIFY_RELEASE_STAGES = "{{ implode(',',config('bugsnag.notify_release_stages')) }}";
        window.MAPBOX_API_KEY = "{{ config('services.mapbox.key', 'pk.eyJ1Ijoic2F1bG8xMSIsImEiOiJjaWVxcWRpMWkwMHp1d2drbWc2b3J4OTB5In0.8WHm7oBKegNi0cF0iy_VfA') }}";

        console.log('%c This is SLSA Beach Management App!', 'font-size: 12px; color: #ed1c2e;')
        console.log('%c '+window.APP_VERSION+' @ '+window.BUILDED_AT, 'font-size: 12px; color: #ed1c2e;')
        console.log('%c Made by Nano Solutions in Fremantle  🇦🇺 ', 'font-size: 14px; color: #ed1c2e')
    </script>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <style>

        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
            font-size: 1rem;
            font-weight: 400;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }

        .main-loading-image {
            text-align: center;
            max-width: 60%;
            animation: heartbeat 8s infinite;
            z-index: 99999;
            -webkit-filter: drop-shadow(1px 1px 30px #222 );
            filter: drop-shadow(1px 1px 30px #222);
        }

        #main-loading-text {
            position: fixed;
            color: #ffffff;
            top: 20px;
            text-align: center;
            max-width: 100%;
            left: 0;
            right: 0;
            z-index: 99999;
        }

        #main-loading {
            position: fixed;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            padding: 0;
            margin: 0;
            background: #0060a9;
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            align-items: center;
            justify-content: center;
            min-height: 24em;
            z-index: 99999;

            background-color: #0060a9 !important;
            /* background: linear-gradient(-45deg, #EE7752, #ed1c2e, #0060a9, #23D5AB);
            background-size: 400% 400%;

            -webkit-animation: Gradient 15s ease infinite;
            -moz-animation: Gradient 15s ease infinite;
            animation: Gradient 15s ease infinite; */
        }

        @keyframes heartbeat {
            0% {
                transform: scale(1);
            }

            20% {
                transform: scale(1.15);
            }
            40% {
                transform: scale(1.35);
            }
            60% {
                transform: scale(1.1);
            }
            80% {
                transform: scale(1.15);
            }
            100% {
                transform: scale(1);
            }
        }


        @keyframes Gradient {
            0% {
                background-position: 0% 50%
            }
            50% {
                background-position: 100% 50%
            }
            100% {
                background-position: 0% 50%
            }
        }

    </style>
</head>

<body>
<div id="main-loading">
    <div id="main-loading-text">Loading App...</div>
    <img src="{{ asset('/images/sls_logo.png') }}" alt="SLS Logo" class="main-loading-image">
</div>

<div id="app" class="container-fluid">
    @yield('content')
</div>


<noscript>It may sound funny, but this application requires JavaScript. Please enable it.</noscript>
<!-- Scripts -->
<script src="{{ mix('js/manifest.js') }}"></script>
<script src="{{ mix('js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
@yield('scripts')
</body>
</html>
