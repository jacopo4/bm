<div id="audit-{{ $audit->id }}-map" style="height: 200px;"></div>

@push('scripts')
<script>
(function() {
    let auditObjects = window.objects.filter(object => object.audit_id === {{ $audit->id }})
    let types = [{
      typeId: 6,
      icon: 'fa-road',
      label: 'Access'
    },{
      typeId: 1,
      icon: 'fa-volleyball-ball',
      label: 'Activities'
    },{
      typeId: 19,
      icon: 'fa-flag',
      label: 'Services'
    },{
      typeId: 13,
      icon: 'fa-siren-on',
      label: 'Support'
    },{
      typeId: 16,
      icon: 'fa-exclamation-triangle',
      label: 'Hazards'
    },{
      typeId: 12,
      icon: 'fa-life-ring',
      label: 'Equipments'
    },{
      typeId: 14,
      icon: 'fa-restroom',
      label: 'Facilities'
    },{
      typeId: 28,
      icon: 'fa-map-signs',
      label: 'Signages'
    }]

    var mymap = L.map('audit-{{ $audit->id }}-map').setView([auditObjects[0].latitude, auditObjects[0].longitude], 14);
    mymap.scrollWheelZoom.disable()
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1Ijoic2F1bG8xMSIsImEiOiJjaWVxcWRpMWkwMHp1d2drbWc2b3J4OTB5In0.8WHm7oBKegNi0cF0iy_VfA',
    }).addTo(mymap);
    //
    // var layerControl = L.control.layers(null, null, {collapsed: false}).addTo(this.map)
    // // add objects markers to map

    let bounds = L.latLngBounds(L.latLng(auditObjects[0].latitude, auditObjects[0].longitude), L.latLng(auditObjects[0].latitude, auditObjects[0].longitude));

    auditObjects.forEach((object) => {

      let icon = types.filter((type) => type.typeId === object.type_id)[0].icon;

      let objectIcon = L.divIcon({ html: '<i class="fal '+icon+' text-primary"></i> <span class="text-primary">'+object.subtype_id+'</span>'})

      if (object.latitude !== null && object.longitude !== null) {
        let objectMarker = L.marker(

          L.latLng([object.latitude , object.longitude])

      ).setIcon(objectIcon).addTo(mymap)
      bounds.extend(L.latLng([object.latitude , object.longitude]))
      }
    })
    mymap.fitBounds(bounds);
})();
</script>
@endpush
