@extends('layouts.preview')

@section('content')
<div class="container-fluid d-flex flex-column">

  <div class="row h-100">
    {{--    Sidebar left--}}
    <div class="fill-page col-3" style="padding: 0; height: 100vh; position: sticky; top: 0;">
      <scrollactive id="manual-nav" class="nav nav-pills bg-white flex-column scroll" active-class="active" :offset="80"
        :duration="800" bezier-easing-value=".5,0,.35,1">
        <ul class="p-0">
          @foreach($assessment->audits as $audit)
          <li class="item p-0 bg-white">
            <div class="p-0 folder">
              <a href="#audit-{{ $audit->id }}" dusk="Audit-{{ $audit->id }}" class="scrollactive-item">
                {{ $audit->title }}
              </a>
            </div>
          </li>
          @foreach($types as $type => $title)
          @if(!$audit->objects()->where('type_id', $type)->get()->isEmpty())
          <ul class="ml-3">
            <li class="item">
              <div class="p-0 folder">
                <a href="#audit-{{ $audit->id }}-type-{{ $type }}" class="scrollactive-item">
                  {{ $title }}
                </a>
                @foreach($audit->objects()->where('type_id', $type)->get() as $auditObject)
                <ul class="ml-3">
                  <li class="item">
                    <div class="p-0 folder">
                      <a href="#object-{{ $auditObject->id }}" class="scrollactive-item">
                        {{ $auditObject->title }}
                      </a>
                    </div>
                  </li>
                </ul>
                @endforeach
              </div>
            </li>
          </ul>
          @endif
          @endforeach
          @endforeach
        </ul>
      </scrollactive>
    </div>

    {{--    COntent right--}}
    <div class="col-9 mb-5 pl-0 pr-3 pt-4">
      <div class="col-2 col-12 pb-0 text-right">
        <a href={{ "/assessments/".$assessment->id."/status" }}>
          Return to Assessment
        </a>
      </div>
      <div class="bg-white m-4 pt-1 rounded">
        <div class="row bg-info pt-2 m-2 rounded">
          <div class="col-12 pb-2">
            <h1>{{ $assessment->title }}</h1>
          </div>
          {{-- <div class="col-xs-12 col-sm-6 pb-2">
            <small>Title</small><br>
            <b>{{ $assessment->title }}</b>
          </div>
          <div class="col-xs-12 col-sm-6 pb-2">
            <small>Type</small><br>
            <b>{{ $assessment_types[$assessment->type] }}</b>
          </div> --}}
          <div class="col-xs-12 col-sm-6 pb-2">
            <small>Summary</small><br>
            {{ $assessment->summary }}
          </div>
          <div class="col-xs-12 col-sm-6 pb-2">
            <small>State/Area Name</small><br>
            <b>{{ $assessment->area }}</b>
          </div>
          <div class="col-xs-12 col-sm-6 pb-2">
            <small>Start Date</small><br>
            <b>{{ $assessment->start_date->format('d/m/Y')}}</b>
          </div>
          <div class="col-xs-12 col-sm-6 pb-2">
            <small>End Date</small><br>
            <b>{{ $assessment->end_date->format('d/m/Y')}}</b>
          </div>
          <div class="col-xs-12 col-sm-6 pb-2">
            <small>Delivery Date</small><br>
            <b>{{ $assessment->delivery_date ? $assessment->delivery_date->format('d/m/Y') : '' }} </b>
          </div>
          <div class="col-xs-12 col-sm-6 pb-2">
            <small>Status</small><br>
            <b>{{ $assessment->status == 2 ? 'Closed' : 'Draft'}}</b>
          </div>

          {{--@if(!$assessment->audits->isEmpty())
            @include('exports.assessmentMap')
          @endif--}}
          @if(!$assessment->users->isEmpty())
          <div class="col-12 pt-2 bg-info">
            <small>Members</small>
          </div>
          <div class="col-12 pb-2">
            <ul class="list-group w-100">
              @foreach($assessment->users as $member)
              <li class="list-group-item flex-column align-items-start p-2">
                <b>{{ $member->first_name }} {{ $member->last_name }}</b><br>
                {{ $roles[$member->pivot->role_id] }}
              </li>
              @endforeach
            </ul>
          </div>
          @else
          <div class="col-12">
          </div>
          @endif
        </div>
        <hr>
        @if($assessment->audits->isEmpty())
        <div class="col-12 pt-2">-</div>
        @endif
        <div class="p-0">
          @foreach($assessment->audits as $audit)
          <div class="bg-primary text-white p-3 mb-0 rounded">
            <a name="audit-{{ $audit->id }}"></a>
            <h3 id="audit-{{ $audit->id }}" class="m-0">
              <b>{{ $audit->title }}</b>
            </h3>
            <div class="clearfix"></div>
          </div>

          <div class="row bg-light pt-2 m-2 rounded">
            <div class="col-xs-12 col-sm-6 pb-2">
              <small>Summary</small><br>
              {{ $audit->summary }}
            </div>
            @if ($audit->beach)
            <div class="col-xs-12 col-sm-6 pb-2">
              <small>Beach</small><br>
              <b>{{ $audit->beach->title }}</b>
            </div>
            @endif
            <div class="col-xs-12 col-sm-6 pb-2">
              <small>Hazard Rating</small><br>
              <b>{{ $audit->beach_hazard_rating }}</b>
            </div>
            <div class="col-xs-12 col-sm-6 pb-2">
              <small>FVR Development</small><br>
              <b>{{ @$beach_fvr_development[$audit->beach_fvr_population] }}</b>
            </div>
            <div class="col-xs-12 col-sm-6 pb-2">
              <small>FVR Frequency</small><br>
              <b>{{ @$beach_fvr_frequency[$audit->beach_fvr_frequency] }}</b>
            </div>
            <div class="col-xs-12 col-sm-6 pb-2">
              <small>FVR Population</small><br>
              <b>{{ @$beach_fvr_population[$audit->beach_fvr_population] }}</b>
            </div>
            <div class="col-xs-12 col-sm-6 pb-2">
              <small>Local Population Rating</small><br>
              <b>{{ @$beach_local_population[$audit->beach_local_population] }}</b>
            </div>
            <div class="col-xs-12 col-sm-6 pb-2">
              <small>Human Activity Population in Water</small><br>
              <b>{{ @$beach_human_water[$audit->beach_human_water] }}</b>
            </div>
            <div class="col-xs-12 col-sm-6 pb-2">
              <small>Human Activity Population on Beach</small><br>
              <b>{{ @$beach_human_beach[$audit->beach_human_beach] }}</b>
            </div>
          </div>

          @if(!empty($audit->objects))

          {{--@include('exports.auditMap', ['audit' => $audit])--}}
          @foreach($types as $type => $title)
          @if(!$audit->objects()->where('type_id', $type)->get()->isEmpty())
          <div class="bg-primary text-white p-3 mb-3 rounded">
            <a name="audit-{{ $audit->id }}-type-{{ $type }}"></a>
            <h4 id="audit-{{ $audit->id }}-type-{{ $type }}">{{$title}}</h4>
            <div class="clearfix"></div>
          </div>
          @endif
          @foreach($audit->objects()->where('type_id', $type)->get() as $auditObject)
          <div class="pagebreak"> </div>
          <div class="bg-light p-3 mb-3 text-left rounded">
            <a name="object-{{ $auditObject->id }}"></a>
            <h4 id="object-{{ $auditObject->id }}">
              {{$title}} >
              {{$auditObject->title}}</h4>
            <div class="clearfix"></div>
          </div>
          {{--@include('exports.auditObjectMap', ['auditObject' => $auditObject])--}}
          @if(!$auditObject->media->isEmpty())
          <div class="col-12 mb-1 p-0 pl-2">
            <h4 class="m-0">
              {{ "Media:" }}
            </h4>
          </div>
          @foreach($auditObject->media as $media)
          <div class="col-12 pt-2">
            @if(isset($media->custom_properties['generated_conversions']['thumbs'])
            &&
            $media->custom_properties['generated_conversions']['thumbs'] === true)

            <div class="p-1">
              <div class="card">
                <div class="card-header">
                  {{$media->file_name}}
                  <small class="float-right">{{$media->human_readable_size}}</small>
                  <a href="{{$media->getFullUrl()}}" target="_blank" class="btn btn-outline-primary btn-sm">Open</a>
                </div>
                <div class="card-body">
                  <img src="{{ $media->getFullUrl() }}" loading="lazy" class="card-img-top">
                </div>
              </div>
            </div>
            @else

            <div class="card">
              <div class="card-header">
                {{$media->file_name}}
                <small class="float-right">{{$media->human_readable_size}}</small>
                <a href="{{$media->getFullUrl()}}" target="_blank" class="btn btn-outline-primary btn-sm">Open</a>
              </div>
            </div>
            @endif

            <br>

          </div>

          @endforeach
          @else
          <div class="col-12">
          </div>
          @endif

          @if(!$auditObject->risks->isEmpty())
          <div class="col-12 mb-1 p-0 pl-2">
            <h4 class="m-0">
              {{ "Risks:" }}
            </h4>
          </div>
          <div class="col-12 mb-1 p-2 bg-white rounded">

            <div class="p-1">
              @foreach($auditObject->risks as $risk)
              <div class="p-1">
                <div class="card p-2">
                  <b>{{ $risk->title }}</b>

                  <div class="row">
                    <div class="col-12">
                      Likehood: {{ $likehood[$risk->likehood] }}
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-12">
                      Consequence: {{ $consequence[$risk->consequence] }}
                    </div>
                  </div>

                  {{--  Exposure: {{ $risk->exposure }}--}}

                  <div class="row">
                    <div class="col-12">
                      Score: {{ $risk->score }}
                    </div>
                  </div>
                </div>
              </div>

              @endforeach
            </div>
          </div>
          @else
          <div class="p-1">
          </div>
          @endif

          @if(!$auditObject->recommendations->isEmpty())
          <div class="col-12 mb-1 p-0 pl-2">
            <h4 class="m-0">
              {{ "Recommendations:" }}
            </h4>
          </div>
          <div class="col-12 mb-1 p-2 bg-white rounded">

            <div class="p-1">
              @foreach($auditObject->recommendations()->orderBy('updated_at')->get() as $recommendation)
              <div class="p-1">
                <div class="card p-2">
                  <b>{{ $recommendation->user->first_name }} {{ $recommendation->user->last_name }} -
                    {{ $recommendation->updated_at > $recommendation->created_at ? $recommendation->updated_at->format('d/m/Y H:i') : $recommendation->created_at->format('d/m/Y H:i')}}
                    {{ $recommendation->updated_at > $recommendation->created_at ? '(edited)' : '' }}</b>
                  <div>
                    <span>{!! $recommendation->text !!}</span>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
          @else
          <div class="p-1"></div>
          @endif

          @endforeach
          @endforeach
          @endif

          @endforeach
        </div>
      </div>

    </div>
  </div>
  @endsection

  @section('style')
  <style>
    @media print {
      .pagebreak {
        page-break-before: always;
      }
    }

    .folder {
      background-color: white;
      line-height: 1rem;
    }

    ul {
      list-style-type: none;
      padding-inline-start: 0;
    }

    li {
      border-top: 1px solid grey;
      padding: 0px;
    }

    li a {
      margin: 0px;
      display: block;
      width: 100%;
      height: 100%;
      padding: .3rem !important;
      text-decoration: none !important;
    }

    .scroll {
      overflow: scroll;
      height: 100%;
    }


    .active {
      color: white;
      background: whitesmoke !important;
    }

    a:active {
      color: white;
      background: #373a3c !important;

    }
  </style>
  @endsection