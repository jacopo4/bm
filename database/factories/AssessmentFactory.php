<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Assessment;
use Faker\Generator as Faker;

$factory->define(Assessment::class, function (Faker $faker) {
    return [
        'title' => $faker->city." Assessment",
        'area' => $faker->state. " ". $faker->country,
        'type' => 1,
        'status' => random_int(1, 2),
        'start_date' => $faker->dateTimeBetween('-1 year', '+0 day'),
        'end_date' => $faker->dateTimeBetween('+1 month', '+1 year'),
        'delivery_date' => $faker->dateTimeBetween('+1 month', '+1 year'),
    ];
});
