<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBeachForeignKeyToAssessments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('assessments', function (Blueprint $table) {
            $table->renameColumn('beaches', 'beach_id');
        });
        Schema::table('assessments', function ($table) {
            $table->integer('beach_id')->unsigned()->change();
            $table->foreign('beach_id')->references('id')->on('beaches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessments', function ($table) {
            $table->dropForeign(['beach_id']);
            $table->integer('beach_id')->change();
        });

        Schema::table('assessments', function (Blueprint $table) {
            $table->renameColumn('beach_id', 'beaches');
        });
    }
}
