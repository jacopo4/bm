<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beaches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('absamp_id');
            $table->char("key", 10)->index();
            $table->string("title");
            $table->string("state")->nullable();
            $table->string("image")->nullable();
            $table->decimal("longitude", 18, 12)->nullable()->index();
            $table->decimal("latitude", 18, 12)->nullable()->index();
            $table->timestamp('last_updated');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beaches');
    }
}
