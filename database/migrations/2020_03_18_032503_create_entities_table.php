<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entities', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->integer("entity_id");
            // $table->integer("entity_type_id");
            // $table->integer("parent_entity_id");
            // $table->integer("grandparent_entity_id");
            // $table->integer("is_sls")->default(0);
            // $table->integer("is_academy")->default(0);
            // $table->integer("is_lifeguards")->default(0);
            // $table->integer("is_pool")->default(0);
            // $table->integer("is_support_oerations")->default(0);
            // $table->integer("is_other_sls")->default(0);
            // $table->integer("is_non_als")->default(0);
            // $table->integer("entity_sub_type_is_external")->default(0);
            // $table->integer("is_patrol_stats_sls")->default(0);
            // $table->integer("is_patrol_stats_lifeguards")->default(0);
            // $table->integer("is_patrol_stats_pool")->default(0);
            // $table->string("office_phone")->nullable();
            // $table->string("cap_logo")->nullable();
            // $table->decimal("latitude", 18, 12)->nullable()->index();
            // $table->decimal("longitude", 18, 12)->nullable()->index();
            // $table->string('physical_address');
            // $table->string('physical_suburb');
            // $table->string('physical_state');
            // $table->string('postal_state');
            // $table->string('physical_postcode');
            // $table->string("website");
            // $table->string("state");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('entity_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('entity_id')->index();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entities');
        Schema::dropIfExists('entity_user');
    }
}
