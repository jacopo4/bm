<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdjustAssessmnentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessments', function (Blueprint $table) {
            $table->dropColumn('members');
            $table->dropColumn('locations');
            $table->dropColumn('comments');
            $table->integer('type')->after('title');
            $table->text('summary')->after('area')->nullable();
            $table->date('delivery_date')->after('end_date')->nullable();
        //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessments', function (Blueprint $table) {
            $table->integer('members');
            $table->integer('locations');
            $table->integer('comments');
            $table->dropColumn('type');
            $table->dropColumn('summary');
            $table->dropColumn('delivery_date');
        });
    }
}
