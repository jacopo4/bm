<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMemberIdToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // add member_id
            $table->integer('member_id')->nullable()->after('id')->unique('member_id');
            // split name
            $table->renameColumn('name', 'first_name')->nullable();
            $table->string('last_name')->after('name')->nullable();
            // add user details
            $table->date('dob')->after('password');
            $table->string('username')->nullable()->after('password');
            $table->string('mobile')->nullable()->after('password');
            // dates
            $table->timestamp('last_login_at')->nullable();
            // meta
            $table->json('officer_entities')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['member_id']);
            $table->renameColumn('first_name', 'name');
            $table->dropColumn(['last_name', 'dob', 'username','mobile','last_login_at', 'officer_entities']);
        });
    }
}
