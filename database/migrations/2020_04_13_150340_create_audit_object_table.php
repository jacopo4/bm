<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditObjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_object', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('assessment_id')->unsigned()->index();
            $table->bigInteger('beach_id')->unsigned()->index();
            $table->bigInteger('audit_id')->unsigned()->index();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->integer('type_id')->unsigned()->index();
            $table->integer('subtype_id')->unsigned()->index();
            $table->text('title');
            $table->text('metadata')->nullable();
            $table->decimal('latitude', 18, 12)->nullable()->index();
            $table->decimal('longitude', 18, 12)->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_object');
    }
}
