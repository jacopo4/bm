<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('assessment_id')->unsigned()->index();
            $table->bigInteger('beach_id')->unsigned()->index();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->string("title");
            $table->text("summary")->nullable();
            $table->date("date_visit")->index();
            $table->integer("beach_hazard_rating")->nullable();
            $table->integer("beach_type")->nullable();
            $table->integer("beach_fvr_development")->nullable();
            $table->integer("beach_fvr_population")->nullable();
            $table->integer("beach_fvr_frequency")->nullable();
            $table->integer("beach_local_population")->nullable();
            $table->integer("beach_human_water")->nullable();
            $table->integer("beach_human_water_conflict")->nullable();
            $table->integer("beach_human_beach")->nullable();
            $table->integer("beach_human_beach_conflict")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit');
    }
}
