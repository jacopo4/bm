<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeAuditObjectSignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_object_sign', function (Blueprint $table) {
            $table->bigInteger('audit_object_id')->unsigned();
            $table->bigInteger('sign_object_id')->unsigned();
            $table->primary(['audit_object_id', 'sign_object_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_object_sign');
    }
}
