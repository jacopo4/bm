<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoveMetadataTypeToObjectSubTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('object_subtypes', function (Blueprint $table) {
            $table->string('metadata_type')->nullable();
        });
        Schema::table('audit_object', function (Blueprint $table) {
            $table->dropColumn('metadata_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('object_subtypes', function (Blueprint $table) {
            $table->dropColumn('metadata_type');
        });
        Schema::table('audit_object', function (Blueprint $table) {
            $table->string('metadata_type')->nullable();
        });
    }
}
