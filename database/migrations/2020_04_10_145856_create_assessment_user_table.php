<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_user', function (Blueprint $table) {
            $table->bigInteger('assessment_id')->unsigned()->index();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->integer('role_id')->unsigned()->index();
            $table->primary(['assessment_id', 'user_id']);
            $table->timestamps();
        });

        Schema::table('assessment_user', function ($table) {
            $table->foreign('assessment_id')->references('id')->on('assessments');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_user');
    }
}
