<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixAuditObjectPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audit_object_pivot', function (Blueprint $table) {
            $table->primary(['audit_object_id', 'hazard_object_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audit_object_pivot', function (Blueprint $table) {
            $table->dropPrimary(['audit_object_id', 'hazard_object_id']);
        });
    }
}
