<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomFieldsToMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->bigInteger('assessment_id')->unsigned()->index()->nullable();
            $table->bigInteger('beach_id')->unsigned()->index()->nullable();
            $table->bigInteger('audit_id')->unsigned()->index()->nullable();
            $table->bigInteger('audit_object_id')->unsigned()->index()->nullable();
            $table->bigInteger('object_type_id')->unsigned()->index()->nullable();
            $table->bigInteger('user_id')->unsigned()->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->dropColumn(['assessment_id', 'beach_id','audit_id','audit_object_id','object_type_id','user_id']);
        });
    }
}
