<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRisksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risks', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('assessment_id')->unsigned()->index();
            $table->bigInteger('beach_id')->unsigned()->index();
            $table->bigInteger('audit_id')->unsigned()->index();
            $table->bigInteger('audit_object_id')->unsigned()->index();
            $table->integer('object_type_id')->unsigned()->index();
            $table->string('title');
            $table->integer('likehood');
            $table->integer('consequence');
            $table->integer('exposure');
            $table->integer('score');
            $table->integer('user_id');
            $table->morphs('relatable');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risks');
    }
}
