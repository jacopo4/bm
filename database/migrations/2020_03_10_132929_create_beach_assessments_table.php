<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeachAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_beach', function (Blueprint $table) {
            $table->bigInteger('assessment_id')->unsigned();
            $table->integer('beach_id')->unsigned();
            $table->primary(['assessment_id', 'beach_id']);
            $table->timestamps();
        });
        Schema::table('assessment_beach', function ($table) {
            $table->foreign('assessment_id')->references('id')->on('assessments');
            $table->foreign('beach_id')->references('id')->on('beaches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_beach');
    }
}
