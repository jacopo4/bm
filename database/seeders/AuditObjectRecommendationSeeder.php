<?php

namespace Database\Seeders;

use App;
use App\Recommendation;
use App\AuditObject;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class AuditObjectRecommendationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {

        $limit = 5;
        if (App::environment('testing')) {
            $limit = 2;
        }
        // Populated assessment with information
        $auditObjects = AuditObject::all();
        foreach ($auditObjects as $auditObject) {
            $auditor = 1;
            $comments_total = rand(1, $limit);
            $ii = 1;
            while ($comments_total > $ii++) {
                Recommendation::create([
                    'assessment_id'    => $auditObject->assessment_id,
                    'beach_id'         => $auditObject->beach_id,
                    'audit_id'         => $auditObject->audit_id,
                    'user_id'          => $auditor,
                    'audit_object_id'  => $auditObject->id,
                    'object_type_id'   => $auditObject->type_id,
                    'recommendable_type' => 'auditObject',
                    'recommendable_id'   => $auditObject->id,
                    'text'             => 'Recommendation: '.$faker->text()
                ]);
            }
        }
    }
}
