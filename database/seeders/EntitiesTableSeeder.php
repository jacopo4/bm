<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class EntitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('entities')->delete();

        DB::table('entities')->insert(array(
            0 =>
            array(
                'id' => 1,
                'title' => 'Surf Life Saving Australia',
                'entity_id' => 1
            ),
            1 =>
            array(
                'id' => 2,
                'title' => 'ILS and Countries State',
                'entity_id' => 1317
            ),
            2 =>
            array(
                'id' => 3,
                'title' => 'Life Saving Victoria',
                'entity_id' => 7
            ),
        ));
    }
}
