<?php

namespace Database\Seeders;

use App\Comment;
use App\AuditObject;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class PercyAuditObjectCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // Populated assessment with information
        $auditObjects = AuditObject::all();
        foreach ($auditObjects as $auditObject) {
            $auditor = 1;
            $comments_total = 2;
            $ii = 1;
            while ($comments_total > $ii++) {
                Comment::create([
                    'assessment_id'    => $auditObject->assessment_id,
                    'beach_id'         => $auditObject->beach_id,
                    'audit_id'         => $auditObject->audit_id,
                    'user_id'          => $auditor,
                    'audit_object_id'  => $auditObject->id,
                    'object_type_id'   => $auditObject->type_id,
                    'commentable_type' => 'auditObject',
                    'commentable_id'   => $auditObject->id,
                    'text'             => 'Original Percy Test Comment'
                ]);
            }
        }
    }
}
