<?php

namespace Database\Seeders;

use App;
use App\Audit;
use App\AuditObject;
use App\ObjectSubtype;
use Illuminate\Database\Seeder;

class AuditObjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $limit = 30;

        if (App::environment('testing')) {
            $limit = 10;
        }
        // Populated audit with objects
        $audits = Audit::with(['beach'])->get();
        foreach ($audits as $audit) {
            $auditor = 1;
            $auditObjects = ObjectSubtype::select('object_subtypes.id', 'object_subtypes.type_id', 'object_subtypes.title')
                                ->join('object_types', 'object_types.id', '=', 'object_subtypes.type_id')
                                ->where('object_types.is_group', '=', 1)
                                ->inRandomOrder()
                                ->take($limit)
                                ->get();
            $random_point = $this->generate_random_point([$audit->beach->latitude, $audit->beach->longitude], mt_rand() / mt_getrandmax());

            AuditObject::create([
                'assessment_id' => $audit->assessment_id,
                'beach_id'      => $audit->beach_id,
                'audit_id'      => $audit->id,
                'user_id'       => $auditor,
                'type_id'       => 16,
                'subtype_id'    => 194,
                'latitude'      => $random_point[0],
                'longitude'     => $random_point[1],
                'title'         => 'AuditObject 194 '.$audit->beach->title,
                'metadata'      => ObjectSubtype::find(194)->metadata
            ]);

            AuditObject::create([
                'assessment_id' => $audit->assessment_id,
                'beach_id'      => $audit->beach_id,
                'audit_id'      => $audit->id,
                'user_id'       => $auditor,
                'type_id'       => 28,
                'subtype_id'    => 379,
                'latitude'      => $random_point[0],
                'longitude'     => $random_point[1],
                'title'         => 'AuditObject 379 '.$audit->beach->title,
                'metadata'      => ObjectSubtype::find(379)->metadata
            ]);
            foreach ($auditObjects as $subtype) {
                $random_point = $this->generate_random_point([$audit->beach->latitude, $audit->beach->longitude], mt_rand() / mt_getrandmax());

                $auditObject = AuditObject::create([
                    'assessment_id' => $audit->assessment_id,
                    'beach_id'      => $audit->beach_id,
                    'audit_id'      => $audit->id,
                    'user_id'       => $auditor,
                    'type_id'       => $subtype->type_id,
                    'subtype_id'    => $subtype->id,
                    'latitude'      => $random_point[0],
                    'longitude'     => $random_point[1],
                    'title'         => $subtype->title.' '.$audit->beach->title,
                    'metadata'      => ObjectSubtype::find($subtype->id)->metadata
                ]);
            }
        }
    }

    private function generate_random_point($centre, $radius)
    {

        $radius_earth = 6371; //kms

        //Pick random distance within $distance;
        $distance = lcg_value()*$radius;

        //Convert degrees to radians.
        $centre_rads = array_map('deg2rad', $centre);

        //First suppose our point is the north pole.
        //Find a random point $distance kms away
        $lat_rads = (pi()/2) -  $distance/$radius_earth;
        $lng_rads = lcg_value()*2*pi();


        //($lat_rads,$lng_rads) is a point on the circle which is
        //$distance kms from the north pole. Convert to Cartesian
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);


        //Rotate that sphere so that the north pole is now at $centre.

        //Rotate in x axis by $rot = (pi()/2) - $centre_rads[0];
        $rot = (pi()/2) - $centre_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);

        //Rotate in z axis by $rot = $centre_rads[1]
        $rot = $centre_rads[1];
        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;


        //Finally convert this point to polar co-ords
        $lng_rads = atan2($x3, $y3);
        $lat_rads = asin($z3);

        return array_map('rad2deg', array( $lat_rads, $lng_rads ));
    }
}
