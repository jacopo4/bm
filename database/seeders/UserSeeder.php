<?php

namespace Database\Seeders;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
              'member_id' => 1,
              'first_name' => "John",
              'last_name' => "Admin",
              'email' => 'admin@nano.rocks',
              'dob' => "1998-04-11",
              'username' => "admin",
              'latitude' => -31.696486399999998,
              'longitude' => 115.8086656,
              'password' => bcrypt('n4n0'),
              'api_token' => Str::random(60)
        ]);
        User::create([
              'member_id' => 2,
              'first_name' => "Celine",
              'last_name' => "Admin",
              'email' => 'admin2@nano.rocks',
              'dob' => "1998-04-11",
              'username' => "admin2",
              'latitude' => -31.696486399999998,
              'longitude' => 115.8086656,
              'password' => bcrypt('n4n0'),
              'api_token' => Str::random(60)
        ]);
        User::create([
            'member_id' => 10,
            'first_name' => "Paul",
            'last_name' => "Auditor",
            'email' => 'auditor@nano.rocks',
            'dob' => "1964-04-11",
            'username' => "auditor",
            'latitude' => -31.696486399999998,
            'longitude' => 115.8086656,
            'password' => bcrypt('n4n0'),
            'api_token' => Str::random(60)
        ]);
        User::create([
            'member_id' => 11,
            'first_name' => "Marie",
            'last_name' => "Auditor",
            'email' => 'auditor2@nano.rocks',
            'dob' => "1984-01-11",
            'username' => "auditor2",
            'latitude' => -31.696486399999998,
            'longitude' => 115.8086656,
            'password' => bcrypt('n4n0'),
            'api_token' => Str::random(60)
        ]);
        User::create([
            'member_id' => 12,
            'first_name' => "Bernie",
            'last_name' => "Auditor",
            'email' => 'auditor3@nano.rocks',
            'dob' => "1984-01-11",
            'username' => "auditor3",
            'latitude' => -31.696486399999998,
            'longitude' => 115.8086656,
            'password' => bcrypt('n4n0'),
            'api_token' => Str::random(60)
        ]);
        User::create([
            'member_id' => 13,
            'first_name' => "Glory",
            'last_name' => "Auditor",
            'email' => 'auditor4@nano.rocks',
            'dob' => "1984-01-11",
            'username' => "auditor4",
            'latitude' => -31.696486399999998,
            'longitude' => 115.8086656,
            'password' => bcrypt('n4n0'),
            'api_token' => Str::random(60)
        ]);
        User::create([
            'member_id' => 20,
            'first_name' => "Brian",
            'last_name' => "CLIENT",
            'dob' => "1994-04-11",
            'email' => 'client@nano.rocks',
            'username' => "client",
            'latitude' => -31.696486399999998,
            'longitude' => 115.8086656,
            'password' => bcrypt('n4n0'),
            'api_token' => Str::random(60)
        ]);
        User::create([
            'member_id' => 21,
            'first_name' => "Pyetra",
            'last_name' => "CLIENT",
            'dob' => "1994-04-11",
            'email' => 'client2@nano.rocks',
            'username' => "client2",
            'latitude' => -31.696486399999998,
            'longitude' => 115.8086656,
            'password' => bcrypt('n4n0'),
            'api_token' => Str::random(60)
        ]);
    }
}
