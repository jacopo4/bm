<?php

namespace Database\Seeders;

use App\AuditObject;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class AuditObjectPhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // Populated assessment with information
        $auditObjects = AuditObject::all();
        foreach ($auditObjects as $auditObject) {
            $photos_total = rand(0, 2);
            $ii = 1;
            while ($photos_total > $ii++) {
                $image = $faker->file('database/files', '/tmp');
                $media = $auditObject->addMedia($image)->toMediaCollection('photos');
                $media->user_id = 1;
                $media->save();
            }
        }
    }
}
