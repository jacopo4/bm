<?php

namespace Database\Seeders;

use App;
use App\Risk;
use App\AuditObject;
use Illuminate\Database\Seeder;

class AuditObjectRiskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $limit = 4;
        if (App::environment('testing')) {
            $limit = 2;
        }
        // Populated assessment with information
        $auditObjects = AuditObject::with(['beach','subtype'])->where('type_id', 16)->get();
        foreach ($auditObjects as $auditObject) {
            $auditor = 1;

            $risks_total = rand(1, $limit);
            $ii = 1;
            while ($risks_total > $ii++) {
                $likehoodOptions = [0.5, 1, 3, 6, 10];
                $consequenceOptions = [1, 2, 5, 10, 20];
                $exposureOptions = [1, 2, 5, 10, 20];

                $likehood = $likehoodOptions[array_rand($likehoodOptions, 1)];
                $consequence = $consequenceOptions[array_rand($consequenceOptions, 1)];
                $exposure = $exposureOptions[array_rand($exposureOptions, 1)];
                $score = $likehood * $consequence * $exposure;

                Risk::create([
                    'assessment_id'    => $auditObject->assessment_id,
                    'beach_id'         => $auditObject->beach_id,
                    'audit_id'         => $auditObject->audit_id,
                    'user_id'          => $auditor,
                    'audit_object_id'  => $auditObject->id,
                    'object_type_id'   => $auditObject->type_id,
                    'relatable_type'   => 'auditObject',
                    'relatable_id'     => $auditObject->id,
                    'title'            => $auditObject->subtype->title.' '.$auditObject->beach->title . ' RISK',
                    'likehood'         => $likehood,
                    'consequence'      => $consequence,
                    'exposure'         => $exposure,
                    'score'            => $score
                ]);
            }
        }
    }
}
