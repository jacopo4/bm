<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Beach;

class BeachSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $beaches = json_decode('[{"id":583,"absamp_id":9090,"key":"wa0833","title":"Bathers Beach","state":"WA","longitude":"115.741220000000","latitude":"-32.057520000000","distance":1.4544489093677264},{"id":10532,"absamp_id":9089,"key":"wa0832","title":"Success Harbour","state":"WA","longitude":"115.750090000000","latitude":"-32.069770000000","distance":2.0530925307164676},{"id":8690,"absamp_id":9092,"key":"wa0835","title":"Port Beach","state":"WA","longitude":"115.745140000000","latitude":"-32.034610000000","distance":2.1294275825317226},{"id":10238,"absamp_id":9088,"key":"wa0831","title":"South Beach (4)","state":"WA","longitude":"115.750740000000","latitude":"-32.072700000000","distance":2.3598998428399107},{"id":10237,"absamp_id":9087,"key":"wa0830","title":"South Beach (3)","state":"WA","longitude":"115.751260000000","latitude":"-32.076240000000","distance":2.7414569946205556},{"id":6249,"absamp_id":9093,"key":"wa0836","title":"Leighton Beach","state":"WA","longitude":"115.749130000000","latitude":"-32.026510000000","distance":2.867731206585295},{"id":10236,"absamp_id":9086,"key":"wa0829","title":"South Beach (2)","state":"WA","longitude":"115.751470000000","latitude":"-32.081240000000","distance":3.2912424270746428},{"id":10235,"absamp_id":9085,"key":"wa0828","title":"South Beach (1)","state":"WA","longitude":"115.755010000000","latitude":"-32.089050000000","distance":4.141820422798151},{"id":1836,"absamp_id":9094,"key":"wa0837","title":"Cable Station","state":"WA","longitude":"115.751920000000","latitude":"-32.012330000000","distance":4.398877348748123},{"id":8866,"absamp_id":9084,"key":"wa0827","title":"Power station","state":"WA","longitude":"115.758570000000","latitude":"-32.095210000000","distance":4.83779939499512},{"id":5748,"absamp_id":9083,"key":"wa0826","title":"James Rocks","state":"WA","longitude":"115.760450000000","latitude":"-32.099720000000","distance":5.351993659729993},{"id":3943,"absamp_id":9095,"key":"wa0838","title":"Dutch Inn","state":"WA","longitude":"115.751460000000","latitude":"-32.003440000000","distance":5.388064691095554},{"id":3281,"absamp_id":9096,"key":"wa0839","title":"Cottesloe Beach","state":"WA","longitude":"115.751390000000","latitude":"-31.991140000000","distance":6.753821764036398},{"id":7704,"absamp_id":12089,"key":"wa0839B","title":"North Cottesloe","state":"WA","longitude":"115.751970000000","latitude":"-31.988910000000","distance":6.998935439091346},{"id":3188,"absamp_id":9082,"key":"wa0825","title":"Coogee Beach","state":"WA","longitude":"115.761020000000","latitude":"-32.120650000000","distance":7.6756895288504845},{"id":11930,"absamp_id":9081,"key":"wa0824","title":"Woodman Point (N)","state":"WA","longitude":"115.744580000000","latitude":"-32.134180000000","distance":9.213246015236413},{"id":11931,"absamp_id":9079,"key":"wa0822","title":"Woodman Point (S)","state":"WA","longitude":"115.754680000000","latitude":"-32.135420000000","distance":9.297756598045178},{"id":11929,"absamp_id":9080,"key":"wa0823","title":"Woodman Point","state":"WA","longitude":"115.743540000000","latitude":"-32.135970000000","distance":9.421963088421192},{"id":5792,"absamp_id":9078,"key":"wa0821","title":"Jervoise Bay (3)","state":"WA","longitude":"115.761800000000","latitude":"-32.138490000000","distance":9.659663386335273},{"id":5791,"absamp_id":9077,"key":"wa0820","title":"Jervoise Bay (2)","state":"WA","longitude":"115.763720000000","latitude":"-32.138920000000","distance":9.72080673728285}]');

        foreach ($beaches as $beach) {
            Beach::create([
                'key'       => $beach->key,
                'absamp_id' => $beach->absamp_id,
                'title'     => $beach->title,
                'state'     => $beach->state,
                'longitude' => $beach->longitude,
                'latitude'  => $beach->latitude,
                'last_updated' => '2020-01-01 01:01:01',
                'metadata' => []
            ]);
        }

        $beach = Beach::find(7);
        $beach->metadata = [
            "hazard_rating" => 6,
            "beach_type" => 1
        ];
        $beach->save();

        $beach = Beach::find(3);
        $beach->metadata = [
            "hazard_rating" => null
        ];
        $beach->save();
    }
}
