<?php

namespace Database\Seeders;

use DB;
use App\Risk;
use App\User;
use App\Audit;
use App\Beach;
use App\Entity;
use App\Comment;
use App\Assessment;
use App\AuditObject;
use App\ObjectSubtype;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        if (!User::count()) {
            // Basic users (admin, auditor, clients)
            $this->call(UserSeeder::class);
        }
        if (!Beach::count()) {
            // Beaches based on Fremantle Location
            $this->call(BeachSeeder::class);
        }
        if (!Assessment::count()) {
            // Create basic assessment
            $this->call(AssessmentsSeeder::class);
        }

        if (!Entity::count()) {
            $this->call(EntitiesTableSeeder::class);
        }

        if (!Audit::count()) {
            // Create audit
            $this->call(AuditSeeder::class);
        }

        // Check for import
        if (!ObjectSubtype::count()) {
            $file = base_path('database/seeders/objects-types.sql.gz');
            if (file_exists($file)) {
                DB::unprepared(file_get_contents('compress.zlib://'.$file));
            } else {
                exit('Import types and seed again! run $ ./artisan import:objects-types');
            }
        }

        if (!AuditObject::count()) {
            // Create audit objects
            $this->call(AuditObjectSeeder::class);
        }

        if (!Risk::count()) {
            // Create objects risks
            $this->call(AuditObjectRiskSeeder::class);
        }

        if (!Comment::count()) {
            // add photos to object
            $this->call(AuditObjectPhotoSeeder::class);
            // create comment
            $this->call(AuditObjectCommentSeeder::class);
            // create Recommendation
            $this->call(AuditObjectRecommendationSeeder::class);
        }
    }
}
