<?php

namespace Database\Seeders;

use DB;
use App\Risk;
use App\User;
use App\Audit;
use App\Beach;
use App\Entity;
use App\Comment;
use App\Assessment;
use App\AuditObject;
use App\ObjectSubtype;
use Illuminate\Database\Seeder;

class TestingSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        if (!User::count()) {
            // Basic users (admin, auditor, clients)
            $this->call(UserSeeder::class);
        }
        if (!Beach::count()) {
            // Beaches based on Fremantle Location
            $this->call(BeachSeeder::class);
        }

        if (!Entity::count()) {
            $this->call(EntitiesTableSeeder::class);
        }
        if (!Assessment::count()) {
            // Create basic assessment

            factory(Assessment::class)->create(['status' => 1]);
            factory(Assessment::class)->create(['status' => 2]);

          // Populated assessment with information
            $assessments = Assessment::all();
            foreach ($assessments as $assessment) {
                if (!$assessment->users()->count()) {
                // Replicate a fresh created Assessment just with admin user. Status 1 = DRAFT
                    $assessment->users()->attach(User::whereUsername('admin')->pluck('id')->all(), ['role_id' => '1']);

                    // Populated the resto of assessments with random users.
                    if ($assessment->status > 1) {
                        // select random auditors and clients
                        $auditors = User::where('username', 'like', 'auditor%')->inRandomOrder()->take(2)->pluck('id')->all();
                        $clients = User::where('username', 'like', 'client%')->inRandomOrder()->take(1)->pluck('id')->all();

                        // Assign the random users to assessment
                        $assessment->users()->attach($auditors, ['role_id' => '2']);
                        $assessment->users()->attach($clients, ['role_id' => '3']);
                    }
                }
            }
        }

        if (!Audit::count()) {
            // Create audit
            $this->call(AuditSeeder::class);
        }

        // Check for import
        if (!ObjectSubtype::count()) {
            $this->command->comment("Seeding via sql file");
            $file = base_path('database/seeders/objects-types.sql.gz');
            if (file_exists($file)) {
                DB::unprepared(file_get_contents('compress.zlib://'.$file));
            } else {
                exit('Import types and seed again! run $ ./artisan import:objects-types');
            }
        }

        if (!AuditObject::count()) {
            // Create audit objects
            $this->call(AuditObjectSeeder::class);
        }

        if (!Risk::count()) {
            // Create objects risks
            $this->call(AuditObjectRiskSeeder::class);
        }

        if (!Comment::count()) {
            // add photos to object
            $this->call(AuditObjectPhotoSeeder::class);
            // create comment
            $this->call(AuditObjectCommentSeeder::class);
            // create recommendations
            $this->call(AuditObjectRecommendationSeeder::class);
        }
    }
}
