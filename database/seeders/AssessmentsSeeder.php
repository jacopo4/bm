<?php

namespace Database\Seeders;

use App\User;
use App\Assessment;
use Illuminate\Database\Seeder;

class AssessmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Assessment::create([
              'title' => "Coastal Risk Assessment",
              'area' => "WA - South Shire Lifeguard",
              'type' => 1,
              'status' => 2,
              'start_date' => "2020-02-11",
              'end_date' => "2020-04-11",
              'delivery_date' => "2020-04-11",
        ]);
        Assessment::create([
            'title' => "Beach Signage Assessment",
            'area' => "NSW - Devon City Council",
            'type' => 1,
            'status' => 2,
            'start_date' => "2020-01-11",
            'end_date' => "2020-03-11",
            'delivery_date' => "2020-04-11",
        ]);
        Assessment::create([
            'title' => "Coastal Risk Assessment",
            'type' => 1,
            'area' => "WA - Albany",
            'status' => 2,
            'start_date' => "2019-09-11",
            'end_date' => "2020-02-11",
            'delivery_date' => "2020-03-11",
        ]);
        Assessment::create([
            'title' => "Assessment Empty",
            'type' => 1,
            'area' => "WA - Fremantle",
            'status' => 1,
            'start_date' => "2020-01-11",
            'end_date' => "2020-04-11",
            'delivery_date' => "2020-05-11",
        ]);


        // Populated assessment with information
        $assessments = Assessment::all();
        foreach ($assessments as $assessment) {
            if (!$assessment->users()->count()) {
            // Replicate a fresh created Assessment just with admin user. Status 1 = DRAFT
                $assessment->users()->attach(User::whereUsername('admin')->pluck('id')->all(), ['role_id' => '1']);

                // Populated the resto of assessments with random users.
                if ($assessment->status > 1) {
                    // select random auditors and clients
                    $auditors = User::where('username', 'like', 'auditor%')->inRandomOrder()->take(2)->pluck('id')->all();
                    $clients = User::where('username', 'like', 'client%')->inRandomOrder()->take(1)->pluck('id')->all();

                    // Assign the random users to assessment
                    $assessment->users()->attach($auditors, ['role_id' => '2']);
                    $assessment->users()->attach($clients, ['role_id' => '3']);
                }
            }
        }
    }
}
