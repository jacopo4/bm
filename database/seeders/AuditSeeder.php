<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Assessment;
use App\Beach;
use App\Audit;

class AuditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Populated assessment with information
        $assessments = Assessment::all();
        foreach ($assessments as $assessment) {
            // NOT EMPTY
            if ($assessment->status > 1) {
                // PICK 3 random Beaches
                $beaches = Beach::inRandomOrder()->take(3)->get();
                foreach ($beaches as $beach) {
                    $assessment->beaches()->attach($beach);
                    // get random auditor
                    $auditor = 1;

                    // Add Audits
                    $audit = Audit::create([
                        'assessment_id' => $assessment->id,
                        'beach_id'      => $beach->id,
                        'user_id'       => $auditor,
                        'title'         => $beach->title . ' Audit',
                        'summary'       => '...',
                        'status'        => 1,
                        'date_visit'    => '2020-01-23',
                        'beach_hazard_rating'           => 6,
                        'beach_type'                    => 1,
                        'beach_fvr_development'         => 1,
                        'beach_fvr_population'          => 2,
                        'beach_fvr_frequency'           => 2,
                        'beach_local_population'        => 4,
                        'beach_human_water'             => 2,
                        'beach_human_beach'             => 2,
                        // 'beach_human_beach_conflict'    => 3
                    ]);
                }
            }
        }
    }
}
