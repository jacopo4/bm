<?php

namespace Database\Seeders;

use App\Risk;
use App\AuditObject;
use Illuminate\Database\Seeder;

class PercyAuditObjectRiskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Populated assessment with information
        $auditObjects = AuditObject::with(['beach','subtype'])->where('type_id', 16)->get();
        foreach ($auditObjects as $auditObject) {
            $auditor = 1;

            $risks_total = 2;
            $ii = 1;
            while ($risks_total > $ii++) {
                $likehoodOptions = [1 => 1, 2 => 6]; // original: [0.5, 1, 3, 6, 10];
                $consequenceOptions = [1 => 1, 2 => 10]; // original: [1, 2, 5, 10, 20];
                $exposureOptions = [1 => 1, 2 => 10]; // original: [1, 2, 5, 10, 20];

                // first risk will be [1, 1, 1], second risk will be [6, 10, 10]
                $likehood = $likehoodOptions[$ii];
                $consequence = $consequenceOptions[$ii];
                $exposure = $exposureOptions[$ii];
                $score = $likehood * $consequence * $exposure;

                Risk::create([
                    'assessment_id'    => $auditObject->assessment_id,
                    'beach_id'         => $auditObject->beach_id,
                    'audit_id'         => $auditObject->audit_id,
                    'user_id'          => $auditor,
                    'audit_object_id'  => $auditObject->id,
                    'object_type_id'   => $auditObject->type_id,
                    'relatable_type'   => 'auditObject',
                    'relatable_id'     => $auditObject->id,
                    'title'            => $auditObject->subtype->title.' '.$auditObject->beach->title . ' RISK',
                    'likehood'         => $likehood,
                    'consequence'      => $consequence,
                    'exposure'         => $exposure,
                    'score'            => $score
                ]);
            }
        }
    }
}
