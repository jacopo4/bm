module.exports = {
    root: true,
    env: {
        browser: true,
        es6: true,
        node: true,
    },
    globals: {
        axios: true
    },
    extends: [
        "eslint:recommended",
        "plugin:vue/essential",
        'plugin:vue/strongly-recommended'
    ],
    globals: {
        'axios': true,
        'google': true,
        'L': true,
        'Camera': true,
        'Vue': true,
        '_': true,
        'Atomics': "readonly",
        'SharedArrayBuffer': "readonly"
    },
    parserOptions: {
        parser: 'babel-eslint',
        ecmaVersion: 2018,
        sourceType: 'module'
    },
    plugins: [
        "vue"
    ],
    rules: {
    'indent': [2, 2],
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
    }
};